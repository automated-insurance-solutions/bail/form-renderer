let mix = require('laravel-mix');

mix.js('build/vendor.js', 'dist/vendor.js')
  .sass('build/form-renderer.all.scss', 'dist/form-renderer/dist')
  .sass('src/styles.scss', 'dist/form-renderer/dist/form-renderer.css')
  .combine([
    './dist/bundle/runtime.js',
    './dist/bundle/polyfills.js',
    './dist/bundle/scripts.js',
    './dist/bundle/main.js',
  ], 'dist/form-renderer/dist/form-renderer.all.js')
  .combine([
    './dist/bundle/runtime.js',
    './dist/bundle/polyfills.js',
    './dist/bundle/main.js',
  ], 'dist/form-renderer/dist/form-renderer.js');
