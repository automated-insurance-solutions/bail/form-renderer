require('popper.js/dist/umd/popper.min.js');

window.moment = require('moment');

require('moment-timezone/builds/moment-timezone.min.js');
require('bootstrap');
require('bootstrap-select/dist/js/bootstrap-select.min.js');
require('tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.js');

window.bootbox = require('bootbox');
