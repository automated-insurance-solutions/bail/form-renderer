import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormWithWitnessesComponent } from './form-with-witnesses.component';

describe('FormWithWitnessesComponent', () => {
  let component: FormWithWitnessesComponent;
  let fixture: ComponentFixture<FormWithWitnessesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormWithWitnessesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormWithWitnessesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
