import { Component, OnInit } from '@angular/core';
import { Form, FormData, FormFactoryService, RenderedForm } from 'form-renderer';
import form from '../../forms/form-with-witnesses.json';

@Component({
  selector: 'app-form-with-witnesses',
  templateUrl: './form-with-witnesses.component.html',
  styleUrls: ['./form-with-witnesses.component.scss']
})
export class FormWithWitnessesComponent implements OnInit {

  form: Form;

  constructor(protected formFactory: FormFactoryService) { }

  ngOnInit(): void {
    this.form = this.formFactory.make(form.form as RenderedForm);

    this.form.setValues(form.data as FormData[]);

    // console.log(this.form.formGroup.value);
  }

}
