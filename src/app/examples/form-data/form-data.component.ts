import { Component, OnInit } from '@angular/core';
import { FormData } from 'form-renderer';
import form from '../../forms/form-with-street-views.json';

@Component({
  selector: 'app-form-data',
  templateUrl: './form-data.component.html',
  styleUrls: ['./form-data.component.scss']
})
export class FormDataComponent implements OnInit {
  data: FormData[];

  ngOnInit(): void {
    this.data = form.data as FormData[];
  }
}
