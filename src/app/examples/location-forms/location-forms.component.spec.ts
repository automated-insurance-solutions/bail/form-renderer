import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationFormsComponent } from './location-forms.component';

describe('LocationFormsComponent', () => {
  let component: LocationFormsComponent;
  let fixture: ComponentFixture<LocationFormsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationFormsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LocationFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
