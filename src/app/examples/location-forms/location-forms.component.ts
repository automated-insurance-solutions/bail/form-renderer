import { Component, OnInit } from '@angular/core';
import * as testForms from '../../../../projects/form-renderer/src/test-forms';
import { Form, FormFactoryService, RenderedForm } from 'form-renderer';

@Component({
  selector: 'app-location-forms',
  templateUrl: './location-forms.component.html',
  styleUrls: ['./location-forms.component.scss']
})
export class LocationFormsComponent implements OnInit {

  form: Form | undefined;

  constructor(protected formFactory: FormFactoryService) { }

  ngOnInit(): void {
    this.form = this.formFactory.make(testForms.locationForm as RenderedForm);
  }

}
