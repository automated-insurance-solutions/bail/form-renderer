import { Component, OnInit } from '@angular/core';
import { Form, FormData, FormFactoryService, RenderedForm } from 'form-renderer';
import form from '../../forms/required-without.json';

@Component({
  selector: 'app-required-without',
  templateUrl: './required-without.component.html',
  styleUrls: ['./required-without.component.scss']
})
export class RequiredWithoutComponent implements OnInit {

  form: Form;

  constructor(protected formFactory: FormFactoryService) { }

  ngOnInit(): void {
    this.form = this.formFactory.make(form.form as RenderedForm);
  }

  validate(): void {
    if (this.form.valid()) {
      alert('Form valid!');
    } else {
      alert('Form invalid!');
    }
  }

}
