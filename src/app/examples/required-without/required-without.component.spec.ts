import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiredWithoutComponent } from './required-without.component';

describe('RequiredWithoutComponent', () => {
  let component: RequiredWithoutComponent;
  let fixture: ComponentFixture<RequiredWithoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequiredWithoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredWithoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
