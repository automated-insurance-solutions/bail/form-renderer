import { Component, OnInit } from '@angular/core';
import form from '../../forms/form-with-street-views.json';
import { Form, FormData, FormFactoryService, RenderedForm } from 'form-renderer';

@Component({
  selector: 'app-form-data-with-street-views',
  templateUrl: './form-data-with-street-views.component.html',
  styleUrls: ['./form-data-with-street-views.component.scss']
})
export class FormDataWithStreetViewsComponent implements OnInit {

  form: Form;

  constructor(protected formFactory: FormFactoryService) { }

  ngOnInit(): void {
    this.form = this.formFactory.make(form.form as RenderedForm);

    this.form.setValues(form.data as FormData[]);
  }

}
