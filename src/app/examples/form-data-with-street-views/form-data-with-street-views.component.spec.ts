import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDataWithStreetViewsComponent } from './form-data-with-street-views.component';

describe('FormDataWithStreetViewsComponent', () => {
  let component: FormDataWithStreetViewsComponent;
  let fixture: ComponentFixture<FormDataWithStreetViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormDataWithStreetViewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDataWithStreetViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
