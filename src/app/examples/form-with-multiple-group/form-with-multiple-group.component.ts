import { Component, OnInit } from '@angular/core';
import * as testForms from '../../../../projects/form-renderer/src/test-forms';
import { Form, FormFactoryService, RenderedForm } from 'form-renderer';

@Component({
  selector: 'app-form-with-multiple-group',
  templateUrl: './form-with-multiple-group.component.html',
  styleUrls: ['./form-with-multiple-group.component.scss']
})
export class FormWithMultipleGroupComponent implements OnInit {

  form: Form | undefined;

  constructor(protected formFactory: FormFactoryService) { }

  ngOnInit(): void {
    this.form = this.formFactory.make(testForms.formWithMultipleGroup as RenderedForm);

    this.form.setValues(testForms.formWithMultipleGroupData);
  }

}
