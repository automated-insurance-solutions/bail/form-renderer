import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormWithMultipleGroupComponent } from './form-with-multiple-group.component';

describe('FormWithMultipleGroupComponent', () => {
  let component: FormWithMultipleGroupComponent;
  let fixture: ComponentFixture<FormWithMultipleGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormWithMultipleGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormWithMultipleGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
