import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormRendererModule } from 'form-renderer';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';
import { NgMapsGoogleModule } from '@ng-maps/google';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormRendererModule,
    NgMapsGoogleModule.forRoot({
      apiKey: 'AIzaSyBKG7ftIqEbcacmJqvuS4u0k4WQv7LrkDo',
      libraries: ['places', 'geometry']
    }),
    FormComponentsModule.forRoot({
      vrnLookup: {
        url: 'https://api.bail.claims/vehicle-lookup'
      },
      addressLookup: {
        url: 'https://api.bail.claims/address-lookup/postcode'
      },
    }),
  ],
  exports: [
    BrowserAnimationsModule,
    FormRendererModule,
    NgMapsGoogleModule,
    FormComponentsModule,
  ]
})
export class SharedModule {

}
