import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormDataWithStreetViewsComponent } from './examples/form-data-with-street-views/form-data-with-street-views.component';
import { FormWithWitnessesComponent } from './examples/form-with-witnesses/form-with-witnesses.component';
import { FormWithMultipleGroupComponent } from './examples/form-with-multiple-group/form-with-multiple-group.component';
import { RequiredWithoutComponent } from './examples/required-without/required-without.component';
import { FormDataComponent } from './examples/form-data/form-data.component';
import { LocationFormsComponent } from './examples/location-forms/location-forms.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent,
  },
  {
    path: 'form-with-multiple-group',
    component: FormWithMultipleGroupComponent,
  },
  {
    path: 'form-with-witnesses',
    component: FormWithWitnessesComponent,
  },
  {
    path: 'form-with-street-view-values',
    component: FormDataWithStreetViewsComponent,
  },
  {
    path: 'form-data',
    component: FormDataComponent,
  },
  {
    path: 'required-without',
    component: RequiredWithoutComponent,
  },
  {
    path: 'location-forms',
    component: LocationFormsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
