import { Component, HostListener, OnInit } from '@angular/core';
import { UtilitiesService } from '@automated-insurance-solutions/bail-form-components';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  items: {url: string, label: string}[] = [
    {
      url: '/form-with-multiple-group',
      label: 'Form With Multiple Group'
    },
    {
      url: '/form-with-witnesses',
      label: 'Form With Witnesses'
    },
    {
      url: '/form-with-street-view-values',
      label: 'Form With Street View Values'
    },
    {
      url: '/form-data',
      label: 'Form Data'
    },
    {
      url: '/required-without',
      label: 'Required Without'
    },
    {
      url: '/location-forms',
      label: 'Location Forms'
    },
  ];

  constructor(protected utilities: UtilitiesService) {
  }

  ngOnInit(): void {
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent): void {
    if (!!event.target) {
      this.utilities.clickedElement.next(event.target);
    }
  }

}
