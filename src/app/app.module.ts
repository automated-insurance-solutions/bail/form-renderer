import { NgModule } from '@angular/core';
import { FormRendererModule } from '../../dist/form-renderer';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormDataWithStreetViewsComponent } from './examples/form-data-with-street-views/form-data-with-street-views.component';
import { FormWithWitnessesComponent } from './examples/form-with-witnesses/form-with-witnesses.component';
import { FormWithMultipleGroupComponent } from './examples/form-with-multiple-group/form-with-multiple-group.component';
import { RequiredWithoutComponent } from './examples/required-without/required-without.component';
import { FormDataComponent } from './examples/form-data/form-data.component';
import { LocationFormsComponent } from './examples/location-forms/location-forms.component';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormDataWithStreetViewsComponent,
    FormWithWitnessesComponent,
    FormWithMultipleGroupComponent,
    RequiredWithoutComponent,
    FormDataComponent,
    LocationFormsComponent,
  ],
  imports: [
    SharedModule,
    AppRoutingModule,
    FormRendererModule,
    SweetAlert2Module.forRoot({})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
