import { ApplicationRef, DoBootstrap, Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FormRendererComponent, FormDataComponent } from '../../../dist/form-renderer';
import { SharedModule } from '../shared/shared.module';

/*
 |---------------------------------------------------------------------
 | Bundle Module
 |---------------------------------------------------------------------
 |
 | We allow third party applications to use our form renderer. To do
 | that we build a JS file that they can import into their system as
 | they won't all be using angular.
 |
 | We use this module to build the form-renderer as a web component
 | rather than an angular component, then this can be used in an JS
 | application.
 |
*/

@NgModule({
  declarations: [
  ],
  imports: [
    SharedModule,
  ],
  providers: []
})
export class BundleModule implements DoBootstrap {
  constructor(protected injector: Injector) {
  }

  ngDoBootstrap(appRef: ApplicationRef): void {
    const formRenderer = createCustomElement(FormRendererComponent, { injector: this.injector });
    const formData = createCustomElement(FormDataComponent, { injector: this.injector });

    customElements.define('form-renderer', formRenderer);
    customElements.define('form-data', formData);
  }
}
