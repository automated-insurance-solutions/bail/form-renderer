export const environment = {
  production: true,
  vehicleLookup: {
    url: 'https://vehicle-lookup.gmlconsulting.co.uk/api'
  },
  maps: {
    key: 'AIzaSyBKG7ftIqEbcacmJqvuS4u0k4WQv7LrkDo'
  }
};
