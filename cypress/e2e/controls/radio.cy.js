describe('Radio', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'radio.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a radio control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-radio').within(() => {
          cy.get('.form-group > label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Radio');
          });

          cy.get('input[type=radio]').should(($radios) => {
            expect($radios).to.have.length(2);
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });
});
