import moment from 'moment';

describe('Date', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'date.json'
    });
  });

  it('should display a date control', () => {
    // Visit the control test page
    cy.visit('control.html');

    cy.get('bail-form-group-renderer').within(() => {
      cy.get('bail-control-group-renderer').within(() => {
        cy.get('h3').should(($h3) => {
          expect($h3).to.have.length(1);

          expect($h3[0].innerText).to.equal('Test Group');
        });

        cy.get('bail-question-renderer').within(() => {
          cy.get('form-datepicker').within(() => {
            cy.get('label').should(($label) => {
              expect($label).to.have.length(1);
              expect($label[0].innerText).to.equal('Date');
            });

            cy.get('input[type=text]').should(($input) => {
              expect($input).to.have.length(1);
            });

            cy.get('small.text-muted').should(($hint) => {
              expect($hint).to.have.length(1);
              expect($hint[0].innerText).to.equal('Test hint');
            });
          });
        });
      });
    });
  });

  it('should select a date', () => {
    // Visit the control test page
    cy.visit('control.html');

    cy.get('input[type=text]').click();

    // Make sure the date picker is displayed
    cy.get('.bootstrap-datetimepicker-widget').should(($datePicker) => {
      expect($datePicker).to.have.length(1);
    });

    // Check that today's date is set when the datepicker is displayed
    cy.get('input[type=text]').should(($input) => {
      expect($input[0].value).to.equal(moment().format('DD/MM/YYYY'));
    });

    // Go to the previous month
    cy.get('.datepicker-days th.prev').click();

    // Select the last day of the previous month
    cy.get('.day:not(.new)').last().click();

    // Check the input changed
    cy.get('input[type=text]').should(($input) => {
      expect($input[0].value).to.equal(moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY'));
    });
  });

  it('should not be able to select a date outside of the valid range', () => {
    // Visit the control test page
    cy.visit('control.html');

    cy.get('input[type=text]').click();

    // Make sure the date picker is displayed
    cy.get('.bootstrap-datetimepicker-widget').should(($datePicker) => {
      expect($datePicker).to.have.length(1);
    });

    // Check that today's date is set when the datepicker is displayed
    cy.get('input[type=text]').should(($input) => {
      expect($input[0].value).to.equal(moment().format('DD/MM/YYYY'));
    });

    // Go to the next month twice
    cy.get('.datepicker-days th.next').click();
    cy.get('.datepicker-days th.next').click();

    // Select the last day of the month
    cy.get('.day:not(.new)').last().click();

    // Make sure the date picker is still displaying
    cy.get('.bootstrap-datetimepicker-widget').should(($datePicker) => {
      expect($datePicker).to.have.length(1);
    });

    // Check that date has not changed
    cy.get('input[type=text]').should(($input) => {
      expect($input[0].value).to.equal(moment().format('DD/MM/YYYY'));
    });
  });

  it('should repopulate the date value', () => {
    cy.intercept('GET', 'http://api.bail.test/data', {
      data: [
        {
          id: "G61ec66bedc44bc3ee379ff44",
          type: "group",
          name: "test_group",
          label: "Test Group",
          multiple: false,
          values: [
            {
              id: "G61ec66bedc44bc3ee379ff44-Q61ec725ddc44bc3ee379ff45",
              type: "date",
              name: "date",
              label: "Date",
              value: moment().subtract(1, 'week').format()
            }
          ]
        }
      ]
    });

    // Visit the control test page
    cy.visit('control.html');

    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-datepicker').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Date');
          });

          cy.get('input[type=text]').should(($input) => {
            expect($input).to.have.length(1);
            expect($input).to.have.value(moment().subtract(1, 'week').format('DD/MM/YYYY'));
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should display a required validation error', () => {
    // Visit the control test page
    cy.visit('control.html');

    cy.wait(500);

    // Submit the form
    cy.get('.btn').contains('Submit').click();

    // Gets around a bug where the validation errors won't display until a event
    // happens. Tried getting around it with the ChangeDetectorRef but to no
    // avail.
    cy.get('label').click();

    // Check the form group has the is-invalid class
    cy.get('form-group .form-group').should('have.class', 'is-invalid');

    // Check the validation messages are displaying
    cy.get('form-error').should(($formError) => {
      expect($formError).to.have.length(1);
      expect($formError[0].innerText).to.equal('Date is required');
    });
  });
});
