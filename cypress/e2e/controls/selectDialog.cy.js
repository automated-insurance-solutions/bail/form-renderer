describe('Select Dialog', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'selectDialog.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a select control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-select-dialog').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Select');
          });

          cy.get('form-select-dialog-control').should(($input) => {
            expect($input).to.have.length(1);
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should select a value', () => {
    cy.get('.select-dialog span').should(($value) => {
      expect($value[0].innerText).to.equal('Select a Value');
    });

    // Open the select dialog
    cy.get('button.select-dialog').click();

    // Check the select dialog is displaying
    cy.get('mat-dialog-container').should(($matDialog) => {
      expect($matDialog).to.have.length(1);
    });

    // Check the options are displaying
    cy.get('.option-dialog-option').should(($options) => {
      expect($options).to.have.length(2);
      expect($options[0].innerText).to.equal('Foo');
      expect($options[1].innerText).to.equal('Bar');
    });

    // Select the option
    cy.get('.option-dialog-option').eq(0).click();

    // Wait for the dialog to close
    cy.wait(500);

    cy.get('.select-dialog span').should(($value) => {
      expect($value[0].innerText).to.equal('Foo');
    });
  });
});
