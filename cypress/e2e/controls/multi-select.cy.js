import moment from "moment/moment";

describe('Multi-Select', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'multi-select.json'
    });
  });

  it('should display a multi-select control', () => {
    // Visit the control test page
    cy.visit('control.html');

    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-select').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Select');
          });

          cy.get('form-bootstrap-select').should(($input) => {
            expect($input).to.have.length(1);
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should select a value', () => {
    // Visit the control test page
    cy.visit('control.html');

    cy.get('.filter-option-inner-inner').should(($label) => {
      expect($label[0].innerText).to.equal('Nothing selected');
    });

    cy.get('.form-control.dropdown-toggle').click();

    cy.get('.dropdown-menu.inner').should(($dropdownMenu) => {
      expect($dropdownMenu).to.have.length(1);
    });

    cy.get('.dropdown-item').should(($items) => {
      expect($items).to.have.length(2);
      expect($items[0].innerText).to.equal('Foo');
      expect($items[1].innerText).to.equal('Bar');
    });

    cy.get('.dropdown-item').eq(1).click();

    cy.get('.filter-option-inner-inner').should(($label) => {
      expect($label[0].innerText).to.equal('Bar');
    });

    cy.get('.dropdown-item').eq(0).click();

    cy.get('.filter-option-inner-inner').should(($label) => {
      expect($label[0].innerText).to.equal('Foo, Bar');
    });
  });
});
