import moment from 'moment';

describe('Time', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'time.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a time control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-timepicker').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Time');
          });

          cy.get('form-timepicker-control input[type=text]').should(($input) => {
            expect($input).to.have.length(1);
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should select a time', () => {
    cy.get('input[type=text]').click();

    // // Make sure the time picker is displayed
    cy.get('.bootstrap-datetimepicker-widget').should(($timePicker) => {
      expect($timePicker).to.have.length(1);
    });

    // Check that the time picker has been set to the current time
    cy.get('input[type=text]').should(($input) => {
      expect($input[0].value).to.equal(moment().format('h:mm A'));
    });

    // Go to the hour selector
    cy.get('.timepicker-hour').click();

    // Select 10
    cy.get('.hour').eq(10).click();

    // Go the minute selector
    cy.get('.timepicker-minute').click();

    // Select 00
    cy.get('.minute').eq(0).click();

    // Check the input changed
    cy.get('input[type=text]').should(($input) => {
      expect($input[0].value).to.match(/^10:00 (AM|PM)/)
    });
  });

  it('should display a required validation error', () => {
    // Submit the form
    cy.get('.btn').contains('Submit').click();

    // Gets around a bug where the validation errors won't display until a event
    // happens. Tried getting around it with the ChangeDetectorRef but to no
    // avail.
    cy.get('label').click();

    // Check the form group has the is-invalid class
    cy.get('form-group .form-group').should('have.class', 'is-invalid');

    // Check the validation messages are displaying
    cy.get('form-error').should(($formError) => {
      expect($formError).to.have.length(1);
      expect($formError[0].innerText).to.equal('Time is required');
    });
  });
});
