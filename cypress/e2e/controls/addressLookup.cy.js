describe('Address Lookup', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'addressLookup.json'
    });

    cy.intercept('POST', 'https://api.bail.claims/address-lookup/postcode', [
      {
        line_1: '22 Cromer Road',
        line_2: '',
        post_town: 'Norwich',
        postcode: 'NR6 6ND',
      },
      {
        line_1: '24 Cromer Road',
        line_2: '',
        post_town: 'Norwich',
        postcode: 'NR6 6ND',
      },
    ]);
  });

  it('should display an address lookup control', () => {
    cy.visit('control.html');

    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-address-lookup').within(() => {
          cy.get('.form-group > label').should(($label) => {
            expect($label).to.have.length(5);
            expect($label[0].innerText).to.equal('Address Lookup');
            expect($label[1].innerText).to.equal('Address Line 1');
            expect($label[2].innerText).to.equal('Address Line 2');
            expect($label[3].innerText).to.equal('City');
            expect($label[4].innerText).to.equal('Postcode');
          });

          cy.get('input[type=text]').should(($input) => {
            expect($input).to.have.length(5);
            expect($input.first().attr('placeholder')).to.equal('SW19 5AE');
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(3);
            expect($hint[0].innerText).to.equal('Enter the full postcode and then click the \'Find Address\' button to search for the address details');
            expect($hint[1].innerText).to.equal('');
            expect($hint[2].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should search for an address', () => {
    cy.visit('control.html');

    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-address-lookup').within(() => {
          cy.get('input[type=text]').first().type('nr66nd');

          cy.get('button').contains('Find Address').click();

          cy.get('.dropdown-item').eq(0).click();

          cy.get('input[type=text]').should(($input) => {
            expect($input).to.have.length(5);
            expect($input[0].value).to.equal('22 Cromer Road, NR6 6ND');
            expect($input[1].value).to.equal('22 Cromer Road');
            expect($input[2].value).to.equal('');
            expect($input[3].value).to.equal('Norwich');
            expect($input[4].value).to.equal('NR6 6ND');
          });
        });
      });
    });
  });

  it('should display a required validation error', () => {
    cy.visit('control.html');

    cy.get('.btn').contains('Submit').click();

    cy.get('form-group .form-group').should('have.class', 'is-invalid');

    cy.get('form-error').should(($formError) => {
      expect($formError).to.have.length(4);
      expect($formError[0].innerText).to.equal(' Address Line 1 is required ');
      expect($formError[1].innerText).to.equal(' City is required ');
      expect($formError[2].innerText).to.equal(' Postcode is required ');
      expect($formError[3].innerText).to.equal('Address Lookup is required');
    });
  });

  it('should repopulate the address lookup control', () => {
    cy.intercept('GET', 'http://api.bail.test/data', {
      body: {
        data: [
          {
            id: "G61ec66bedc44bc3ee379ff44",
            type: "group",
            name: "test_group",
            label: "Test Group",
            multiple: false,
            values: [
              {
                id: "G61ec66bedc44bc3ee379ff44-Q61eed644dc44bc3ee379ff4c",
                type: "addressLookup",
                name: "addressLookup",
                label: "Address Lookup",
                value: '24 Cromer Road, Norwich, NR6 6ND',
                components: {
                  address_line_1: '24 Cromer Road',
                  address_line_2: '',
                  city: 'Norwich',
                  postcode: 'NR6 6ND'
                }
              }
            ]
          }
        ]
      }
    });

    cy.visit('control.html');

    cy.get('input[type=text]').should(($input) => {
      expect($input).to.have.length(5);
      expect($input[0].value).to.equal('');
      expect($input[1].value).to.equal('24 Cromer Road');
      expect($input[2].value).to.equal('');
      expect($input[3].value).to.equal('Norwich');
      expect($input[4].value).to.equal('NR6 6ND');
    });
  });

  it('should show the address form when "enter address manually" is clicked', () => {
    cy.visit('control.html');

    cy.get('form-address-lookup-control').should('not.be.visible');

    cy.get('a').contains('Enter address manually').click();

    cy.get('form-address-lookup-control').should('be.visible');
  });
});
