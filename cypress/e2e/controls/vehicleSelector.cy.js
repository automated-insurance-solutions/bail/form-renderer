describe('Vehicle Selector', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'vehicleSelector.json'
    });
    cy.intercept('GET', 'https://api.bail.claims/vehicle-lookup/makes', [
      {
        "id": "ford",
        "name": "Ford",
        "country": "USA"
      },
      {
        "id": "volkswagen",
        "name": "Volkswagen",
        "country": "Germany"
      }
    ]);
    cy.intercept('GET', 'https://api.bail.claims/vehicle-lookup/makes/Ford/models', [
      {
        "make_id": "ford",
        "name": "Fiesta"
      }
    ]);

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a vehicle selector control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-vehicle-selector').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Vehicle Selector');
          });

          cy.get('.form-control.dropdown-toggle').should(($dropDowns) => {
            expect($dropDowns).to.have.length(3);
            expect($dropDowns[0].innerText).to.equal('Make');
            expect($dropDowns[1].innerText).to.equal('Model');
            expect($dropDowns[2].innerText).to.equal('Colour');
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should select a value', () => {
    cy.wait(500);

    cy.get('.form-control.dropdown-toggle').eq(0).click();

    cy.get('.dropdown-menu.inner.show:visible').within(() => {
      cy.get('.dropdown-item').eq(1).click();
    });

    cy.get('.form-control.dropdown-toggle').eq(1).click();

    cy.get('.dropdown-menu.inner.show:visible').within(() => {
      cy.get('.dropdown-item').eq(1).click();
    });

    cy.get('.form-control.dropdown-toggle').eq(2).click();

    cy.get('.dropdown-menu.inner.show:visible').within(() => {
      cy.get('.dropdown-item').eq(5).click();
    });

    cy.get('.filter-option-inner-inner').eq(0).should(($label) => {
      expect($label[0].innerText).to.equal('Ford');
    });

    cy.get('.filter-option-inner-inner').eq(1).should(($label) => {
      expect($label[0].innerText).to.equal('Fiesta');
    });

    cy.get('.filter-option-inner-inner').eq(2).should(($label) => {
      expect($label[0].innerText).to.equal('Brown');
    });
  });

  it('should display a required validation error', () => {
    cy.get('.btn').contains('Submit').click();

    // Gets around a bug where the validation errors won't display until a event
    // happens. Tried getting around it with the ChangeDetectorRef but to no
    // avail.
    cy.get('label').click();

    cy.get('form-group .form-group').should('have.class', 'is-invalid');

    cy.get('form-error').should(($formError) => {
      expect($formError).to.have.length(1);
      expect($formError[0].innerText).to.equal('Vehicle Selector is required');
    });
  });
});
