describe('VRN Lookup', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'vrnLookup.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a vrn lookup control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-input').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('VRN Lookup');
          });

          cy.get('input[type=text]').should(($input) => {
            expect($input).to.have.length(1);
          });

          cy.get('button.btn.btn-info').contains('Search').should(($button) => {
            expect($button).to.have.length(1);
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Enter the vehicle registration and then click the \'Search\' button to load the vehicle details');
          });
        });
      });
    });
  });

  it('should lookup a vehicle registration', () => {
    cy.intercept('POST', 'https://api.bail.claims/vehicle-lookup/lookup', {
      make: 'VW',
      model: 'Golf',
      colour: 'Silver'
    });

    cy.get('input[type=text]').type('AA20 ABC');

    cy.get('.btn.btn-info').contains('Search').click();

    cy.get('input[type=text]').should(($input) => {
      expect($input).to.have.value('VW - Golf - Silver');
      expect($input).to.have.attr('readonly');
    });
    cy.get('.btn.btn-danger').contains('Change Vehicle').should(($button) => {
      expect($button).to.have.length(1);
    });
  });

  it('should display a required validation error', () => {
    cy.get('input[type=text]').clear();

    cy.get('.btn').contains('Submit').click();

    cy.get('form-group .form-group').should('have.class', 'is-invalid');

    cy.get('form-error').should(($formError) => {
      expect($formError).to.have.length(1);
      expect($formError[0].innerText).to.equal('VRN Lookup is required');
    });
  });
});
