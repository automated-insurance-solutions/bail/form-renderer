describe('Checkbox Multiple', () => {
  beforeEach(() => {
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'checkboxMultiple.json'
    });
    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a multiple checkbox control', () => {
    cy.get('bail-form-group-renderer').within(() => {
      cy.get('bail-control-group-renderer').within(() => {
        cy.get('h3').should(($h3) => {
          expect($h3).to.have.length(1);

          expect($h3[0].innerText).to.equal('Test Group');
        });

        cy.get('bail-question-renderer').within(() => {
          cy.get('form-checkbox').within(() => {
            cy.get('.form-group > label').should(($label) => {
              expect($label).to.have.length(1);
              expect($label[0].innerText).to.equal('Checkbox');
            });

            cy.get('.form-check-label').should(($checkboxes) => {
              expect($checkboxes).to.have.length(2);
              expect($checkboxes[0].innerText).to.equal('Foo');
              expect($checkboxes[1].innerText).to.equal('Bar');
            });

            cy.get('small.text-muted').should(($hint) => {
              expect($hint).to.have.length(1);
              expect($hint[0].innerText).to.equal('Test hint');
            });
          });
        });
      });
    });
  });
});
