describe('Helper Text', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'helperText.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a helper text control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('.control-helper-text').should(($helperText) => {
          expect($helperText[0].innerText).to.equal('This is some test text');
        });
      });
    });
  });
});
