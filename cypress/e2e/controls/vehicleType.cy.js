describe('Vehicle Type', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'vehicleType.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a vehicle type control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-select').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Vehicle Type');
          });

          cy.get('form-bootstrap-select').should(($input) => {
            expect($input).to.have.length(1);
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should select a value', () => {
    cy.get('.filter-option-inner-inner').should(($label) => {
      expect($label[0].innerText).to.equal('Select a Value');
    });

    cy.get('.form-control.dropdown-toggle').click();

    cy.get('.dropdown-menu.inner').should(($dropdownMenu) => {
      expect($dropdownMenu).to.have.length(1);
    });

    cy.get('.dropdown-item').should(($items) => {
      expect($items).to.have.length(6);
      expect($items[0].innerText).to.equal('Select a Value');
      expect($items[1].innerText).to.equal('Car');
      expect($items[2].innerText).to.equal('Truck');
      expect($items[3].innerText).to.equal('Van');
      expect($items[4].innerText).to.equal('Motorcycle');
      expect($items[5].innerText).to.equal('Other');
    });

    cy.get('.dropdown-item').eq(1).click();

    cy.get('.filter-option-inner-inner').should(($label) => {
      expect($label[0].innerText).to.equal('Car');
    });

    cy.get('form-input label').should('not.exist');
  });

  it('should enter an other vehicle type description', () => {
    cy.get('.filter-option-inner-inner').should(($label) => {
      expect($label[0].innerText).to.equal('Select a Value');
    });

    cy.get('.form-control.dropdown-toggle').click();

    cy.get('.dropdown-menu.inner').should(($dropdownMenu) => {
      expect($dropdownMenu).to.have.length(1);
    });

    cy.get('.dropdown-item').should(($items) => {
      expect($items).to.have.length(6);
      expect($items[0].innerText).to.equal('Select a Value');
      expect($items[1].innerText).to.equal('Car');
      expect($items[2].innerText).to.equal('Truck');
      expect($items[3].innerText).to.equal('Van');
      expect($items[4].innerText).to.equal('Motorcycle');
      expect($items[5].innerText).to.equal('Other');
    });

    cy.get('.dropdown-item').eq(5).click();

    cy.get('.filter-option-inner-inner').should(($label) => {
      expect($label[0].innerText).to.equal('Other');
    });

    cy.get('form-input label').should(($label) => {
      expect($label[0].innerText).to.equal('Other Vehicle Type');
    });

    cy.get('form-input small.text-muted').should(($hint) => {
      expect($hint[0].innerText).to.equal('Please provide a description of the vehicle type');
    });

    cy.get('form-input input').type('Forklift');

    cy.get('#data').should(($form) => {
      expect(JSON.stringify($form[0].form.answers()[0].answers['G61ec66bedc44bc3ee379ff44-Q61eedd5836d10d50402a39b6'])).to.equal(
        JSON.stringify({
          question_id: 'G61ec66bedc44bc3ee379ff44-Q61eedd5836d10d50402a39b6',
          answer: {
            value: 'otherId',
            other_vehicle_type: 'Forklift',
          }
        })
      );
    })
  });
});
