describe('Damage Selector', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'damageSelector.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a damage selector control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-damage-selector').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Damage Selector');
          });

          cy.get('svg.damage-selector').should(($svg) => {
            expect($svg).to.have.length(1);
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should select a value', () => {
    cy.get('.damage-polygon').eq(1).click({force: true});

    cy.get('.damage-polygon').eq(1).should('have.class', 'selected');
  });
});
