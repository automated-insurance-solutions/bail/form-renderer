describe('Checkbox', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'checkbox.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a checkbox control', () => {
    cy.get('bail-form-group-renderer').within(() => {
      cy.get('bail-control-group-renderer').within(() => {
        cy.get('h3').should(($h3) => {
          expect($h3).to.have.length(1);

          expect($h3[0].innerText).to.equal('Test Group');
        });

        cy.get('bail-question-renderer').within(() => {
          cy.get('form-checkbox').within(() => {
            cy.get('input[type=checkbox]').should(($checkboxes) => {
              expect($checkboxes).to.have.length(1);
            });

            cy.get('small.text-muted').should(($hint) => {
              expect($hint).to.have.length(1);
              expect($hint[0].innerText).to.equal('Test hint');
            });
          });
        });
      });
    });
  });
});
