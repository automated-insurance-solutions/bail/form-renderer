describe('Range', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'range.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a range control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-range').within(() => {
          cy.get('.form-group > label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Range');
          });

          cy.get('.btn-toolbar-label').should(($labels) => {
            expect($labels).to.have.length(2);
            expect($labels[0].innerText).to.equal('From');
            expect($labels[1].innerText).to.equal('To');
          });

          cy.get('button.btn-outline-primary').should(($buttons) => {
            expect($buttons).to.have.length(6);
            expect($buttons[0].innerText).to.equal('0');
            expect($buttons[1].innerText).to.equal('10');
            expect($buttons[2].innerText).to.equal('20');
            expect($buttons[3].innerText).to.equal('30');
            expect($buttons[4].innerText).to.equal('40');
            expect($buttons[5].innerText).to.equal('50');
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should select a value', () => {
    cy.get('button.btn-outline-primary').eq(3).click();

    // Check the correct element is active
    cy.get('button.btn-outline-primary').eq(0).should('not.have.class', 'active');
    cy.get('button.btn-outline-primary').eq(1).should('not.have.class', 'active');
    cy.get('button.btn-outline-primary').eq(2).should('not.have.class', 'active');
    cy.get('button.btn-outline-primary').eq(3).should('have.class', 'active');
    cy.get('button.btn-outline-primary').eq(4).should('not.have.class', 'active');
    cy.get('button.btn-outline-primary').eq(5).should('not.have.class', 'active');
  });
});
