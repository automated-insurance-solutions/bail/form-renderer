describe('Phone', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'phone.json'
    });

    // Visit the control test page
    cy.visit('control.html');
  });

  it('should display a phone control', () => {
    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-phone').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(1);
            expect($label[0].innerText).to.equal('Phone');
          });

          cy.get('input[type=tel]').should(($input) => {
            expect($input).to.have.length(1);
            expect($input[0].value).to.equal('01234567890');
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });

  it('should display a required validation error', () => {
    cy.get('input[type=tel]').clear();

    cy.get('.btn').contains('Submit').click();

    cy.get('form-group .form-group').should('have.class', 'is-invalid');

    cy.get('form-error').should(($formError) => {
      expect($formError).to.have.length(1);
      expect($formError[0].innerText).to.equal('Phone is required');
    });
  });
});
