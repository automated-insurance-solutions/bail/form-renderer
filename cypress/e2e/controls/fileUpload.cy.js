describe('File Upload', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/form', {
      fixture: 'fileUpload.json'
    });
  });

  it('should display a file upload', () => {
    // Visit the control test page
    cy.visit('control.html');

    cy.get('bail-control-group-renderer').within(() => {
      cy.get('h3').should(($h3) => {
        expect($h3).to.have.length(1);

        expect($h3[0].innerText).to.equal('Test Group');
      });

      cy.get('bail-question-renderer').within(() => {
        cy.get('form-upload').within(() => {
          cy.get('label').should(($label) => {
            expect($label).to.have.length(2);
            expect($label[0].innerText).to.equal('File Upload');
            expect($label[1].innerText).to.equal('Drag & Drop your files or Browse');
          });

          cy.get('form-filepond-control').should(($input) => {
            expect($input).to.have.length(1);
          });

          cy.get('small.text-muted').should(($hint) => {
            expect($hint).to.have.length(1);
            expect($hint[0].innerText).to.equal('Test hint');
          });
        });
      });
    });
  });
});
