describe('Form With Conditions', () => {
  beforeEach(() => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'forms/form-with-conditions.json'});
  });

  it('should render a form with conditions', () => {
    // Visit the form test page
    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        // Assert only the groups and questions without conditions are displaying
        cy.get('bail-form-group-renderer').first().within(() => {
          cy.get('h3').should('have.text', 'Group Without Conditions');

          cy.get('bail-control-group-renderer').within(() => {
            cy.get('.card .card-body bail-question-renderer form-input').within(() => {
              cy.get('label').should('have.text', 'Question Without Conditions');
              cy.get('input[type=text]').should('have.value', '');

              cy.get('input[type=text]').type('Foo');
            });
          });
        });

        // Wait to give the conditions time to run
        cy.wait(200);

        // Assert conditional groups and questions are now displaying
        cy.get('bail-form-group-renderer').eq(0).within(() => {
          cy.get('h3').should('have.text', 'Group Without Conditions');

          cy.get('bail-control-group-renderer').within(() => {
            cy.get('.card .card-body').within(() => {
              cy.get('bail-question-renderer form-input').eq(0).within(() => {
                cy.get('label').should('have.text', 'Question With Conditions');
                cy.get('input[type=text]').should('have.value', '');
              });

              cy.get('bail-question-renderer form-input').eq(1).within(() => {
                cy.get('label').should('have.text', 'Question Without Conditions');
                cy.get('input[type=text]').should('have.value', 'Foo');
              });
            });
          });
        });

        cy.get('bail-form-group-renderer').eq(1).within(() => {
          cy.get('h3').should('have.text', 'Group With Conditions');

          cy.get('bail-control-group-renderer').within(() => {
            cy.get('.card .card-body').within(() => {
              cy.get('bail-question-renderer form-input').eq(0).within(() => {
                cy.get('label').should('have.text', 'Has Conditions');
                cy.get('input[type=text]').should('have.value', '');
              });
            });
          });
        });
      });
    });
  });

  it('should render a form with conditions and populate the data', () => {
    cy.intercept('GET', 'http://api.bail.test/data', {fixture: 'forms/form-with-conditions-data.json'});

    // Visit the form test page
    cy.visit('form.html');

    cy.wait(300);

    cy.get('h3').click();

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        // Assert conditional groups and questions are displaying
        cy.get('bail-form-group-renderer').eq(0).within(() => {
          cy.get('h3').should('have.text', 'Group Without Conditions');

          cy.get('bail-control-group-renderer').within(() => {
            cy.get('.card .card-body').within(() => {
              cy.get('bail-question-renderer form-input').eq(0).within(() => {
                cy.get('label').should('have.text', 'Question With Conditions');
                cy.get('input[type=text]').should('have.value', 'Bar');
              });

              cy.get('bail-question-renderer form-input').eq(1).within(() => {
                cy.get('label').should('have.text', 'Question Without Conditions');
                cy.get('input[type=text]').should('have.value', 'Foo');
              });
            });
          });
        });

        cy.get('bail-form-group-renderer').eq(1).within(() => {
          cy.get('h3').should('have.text', 'Group With Conditions');

          cy.get('bail-control-group-renderer').within(() => {
            cy.get('.card .card-body').within(() => {
              cy.get('bail-question-renderer form-input').eq(0).within(() => {
                cy.get('label').should('have.text', 'Has Conditions');
                cy.get('input[type=text]').should('have.value', 'Baz');
              });
            });
          });
        });
      });
    });
  });
});
