describe('Basic Form Test', () => {
  beforeEach(() => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'basic-form.json'});
  });

  it('should render a basic form', () => {
    // Visit the control test page
    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {
          cy.get('bail-control-group-renderer').within(() => {
            cy.get('h3').should('have.text', 'Test Group');

            cy.get('.card .card-body bail-question-renderer').within(() => {
              cy.get('form-input').within(() => {
                cy.get('label').should('have.text', 'Test Question');
                cy.get('input[type=text]').should('have.value', '');
              });
            });
          });
        });
      });
    });
  });

  it('should render a basic form and populate the data', () => {
    cy.intercept('GET', 'http://api.bail.test/data', {fixture: 'basic-form-data.json'});

    // Visit the control test page
    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {
          cy.get('bail-control-group-renderer').within(() => {
            cy.get('h3').should('have.text', 'Test Group');

            cy.get('.card .card-body bail-question-renderer').within(() => {
              cy.get('form-input').within(() => {
                cy.get('label').should('have.text', 'Test Question');
                cy.get('input[type=text]').should('have.value', 'Foo');
              });
            });
          });
        });
      });
    });
  });
});
