describe('Form With Multiple Control Groups Test', () => {
  beforeEach(() => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'forms/form-with-multiple-group.json'});
  });

  it('should render a form with multiple control groups', () => {
    // Visit the form test page
    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {
          cy.get('h3').should('have.text', 'Test Group');

          cy.get('bail-control-group-renderer').should('not.exist');

          cy.get('button').contains('Add').eq(0).click();

          cy.get('bail-control-group-renderer').should('exist');

          // Assert the multiple group label is displaying
          cy.get('h3').eq(1).should('have.text', ' Group 1 ');

          cy.get('bail-control-group-renderer').within(() => {
            cy.get('.card .card-body bail-question-renderer').eq(0).within(() => {
              cy.get('form-input').within(() => {
                cy.get('label').should('have.text', 'Question 1');

                cy.get('input[type=text]').should('have.value', '');
              });
            });

            cy.get('.card .card-body bail-question-renderer').eq(1).within(() => {
              cy.get('form-input').within(() => {
                cy.get('label').should('have.text', 'Question 2');

                cy.get('input[type=text]').should('have.value', '');
              });
            });
          });
        });
      });
    });
  });

  it('should render a form with multiple control groups and populate the data', () => {
    cy.intercept('GET', 'http://api.bail.test/data', {fixture: 'forms/form-with-multiple-group-data.json'});

    // Visit the form test page
    cy.visit('form.html');

    cy.wait(100);

    cy.get('h3').click();

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {
          cy.get('h3').eq(0).should('have.text', 'Test Group');
          cy.get('h3').eq(1).should('have.text', ' Group 1 ');
          cy.get('h3').eq(2).should('have.text', ' Group 2 ');

          cy.get('bail-control-group-renderer').eq(0).within(() => {
            cy.get('.card .card-body bail-question-renderer').eq(0).within(() => {
              cy.get('form-input').within(() => {
                cy.get('label').eq(0).should('have.text', 'Question 1');
                cy.get('input[type=text]').should('have.value', 'Foo');
              });
            });

            cy.get('.card .card-body bail-question-renderer').eq(1).within(() => {
              cy.get('form-input').within(() => {
                cy.get('label').eq(0).should('have.text', 'Question 2');
                cy.get('input[type=text]').should('have.value', 'Bar');
              });
            });
          });

          cy.get('bail-control-group-renderer').eq(1).within(() => {
            cy.get('.card .card-body bail-question-renderer').eq(0).within(() => {
              cy.get('form-input').within(() => {
                cy.get('label').eq(0).should('have.text', 'Question 1');
                cy.get('input[type=text]').should('have.value', 'Baz');
              });
            });

            cy.get('.card .card-body bail-question-renderer').eq(1).within(() => {
              cy.get('form-input').within(() => {
                cy.get('label').eq(0).should('have.text', 'Question 2');
                cy.get('input[type=text]').should('have.value', 'Qux');
              });
            });
          });
        });
      });
    });
  });
});
