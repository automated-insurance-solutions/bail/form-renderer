describe('Form With Multiple Nested Groups Test', () => {
  it('should remove the required without validation if the related input has a value', () => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'required-without/standard.json'});

    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {

          cy.get('bail-control-group-renderer bail-question-renderer').eq(1).within(() => {
            cy.get('form-error').should('not.be.visible');

            // Focus the input then unfocus it to get the error message to display
            cy.get('input[type=text]').click();
            cy.get('label').click();

            cy.get('form-error').should('be.visible');
            cy.get('form-error').should('have.text', ' Question 2 is required when \'Question 1\' is not present ');
          });

          cy.get('bail-control-group-renderer bail-question-renderer').eq(0).within(() => {
            cy.get('input[type=text]').type('Test');
          });

          cy.wait(200);

          cy.get('bail-control-group-renderer bail-question-renderer').eq(1).within(() => {
            cy.get('form-error').should('not.exist');
          });
        });
      });
    });
  });

  it('should remove the required without validation if the subject input has a value', () => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'required-without/standard.json'});

    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {

          cy.get('bail-control-group-renderer bail-question-renderer').eq(1).within(() => {
            cy.get('form-error').should('not.be.visible');

            // Focus the input then unfocus it to get the error message to display
            cy.get('input[type=text]').click();
            cy.get('label').click();

            cy.get('form-error').should('be.visible');
            cy.get('form-error').should('have.text', ' Question 2 is required when \'Question 1\' is not present ');

            cy.get('input[type=text]').type('Test');

            cy.wait(200);

            cy.get('form-error').should('not.exist');
          });
        });
      });
    });
  });

  it('should handle the required without validation when the required without control is a location', () => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'required-without/location-required-without.json'});

    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {

          cy.get('bail-control-group-renderer bail-question-renderer').eq(1).within(() => {
            cy.get('form-error').should('not.be.visible');

            // Focus the input then unfocus it to get the error message to display
            cy.get('input[type=text]').click();
            cy.get('label').click();

            cy.get('form-error').should('be.visible');
            cy.get('form-error').should('have.text', ' Question 2 is required when \'Question 1\' is not present ');

            cy.get('input[type=text]').type('Test');

            cy.wait(200);

            cy.get('form-error').should('not.exist');

            cy.get('input[type=text]').clear();

            cy.wait(200);

            cy.get('form-error').should('be.visible');
            cy.get('form-error').should('have.text', ' Question 2 is required when \'Question 1\' is not present ');
          });

          cy.get('bail-control-group-renderer bail-question-renderer').eq(0).within(() => {
            cy.get('input[type=text]').type('Test');
          });

          cy.wait(200);

          cy.get('bail-control-group-renderer bail-question-renderer').eq(1).within(() => {
            cy.get('form-error').should('not.exist');
          });
        });
      });
    });
  });

  it('should handle the required without validation when the subject control is a location', () => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'required-without/location-subject.json'});

    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {

          cy.get('bail-control-group-renderer bail-question-renderer').eq(1).within(() => {
            cy.get('form-error').should('not.be.visible');

            // Focus the input then unfocus it to get the error message to display
            cy.get('input[type=text]').click();
            cy.get('label').click();

            cy.get('form-error').should('be.visible');
            cy.get('form-error').should('have.text', ' Question 2 is required when \'Question 1\' is not present ');

            cy.get('input[type=text]').type('Test');

            cy.wait(200);

            cy.get('form-error').should('not.exist');

            cy.get('input[type=text]').clear();

            cy.wait(200);

            cy.get('form-error').should('be.visible');
            cy.get('form-error').should('have.text', ' Question 2 is required when \'Question 1\' is not present ');
          });

          cy.get('bail-control-group-renderer bail-question-renderer').eq(0).within(() => {
            cy.get('input[type=text]').type('Test');
          });

          cy.wait(200);

          cy.get('bail-control-group-renderer bail-question-renderer').eq(1).within(() => {
            cy.get('form-error').should('not.exist');
          });
        });
      });
    });
  });
});
