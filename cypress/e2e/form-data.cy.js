describe('Form Data', () => {
  beforeEach(() => {
    // Configure stubbed routes
    cy.intercept('GET', 'http://api.bail.test/data', {
      fixture: 'formData.json'
    });

    // Visit the control test page
    cy.visit('form-data.html');
  });

  it('should display the form data', () => {
    cy.get('form-data').eq(0).within(() => {
      cy.get('> bail-group-data').eq(0).within(() => {
        cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Claim Details'));

        cy.get('bail-group-data').eq(0).within(() => {
          cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Details'));

          cy.get('bail-form-field').eq(0).should(($formField) => expect($formField[0].innerText).to.equal('Reference\n\nTestReference'));
          cy.get('bail-form-field').eq(1).should(($formField) => expect($formField[0].innerText).to.equal('Incident Date\n\n01/01/2022'));
          cy.get('bail-form-field').eq(2).should(($formField) => expect($formField[0].innerText).to.equal('Time\n\nNot Provided'));
        });

        cy.get('bail-group-data').eq(1).within(() => {
          cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Location'));

          cy.get('bail-form-field').eq(0).should(($formField) => expect($formField[0].innerText).to.equal('Location\n\nNorwich, UK'));
          cy.get('bail-map').should(($map) => expect($map).to.have.length(1));
          cy.get('.street-view-values .street-view-value').should(($streetViews) => expect($streetViews).to.have.length(1));
        });
      });

      cy.get('> bail-group-data').eq(1).within(($group) => {
        cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Policyholder'));

        cy.get('bail-group-data').eq(0).within(() => {
          cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Contact Details'));

          cy.get('bail-form-field').eq(0).should(($formField) => expect($formField[0].innerText).to.equal('Name\n\nJane Doe'));
          cy.get('bail-form-field').eq(1).should(($formField) => expect($formField[0].innerText).to.equal('Email Address\n\njane@example.com'));
          cy.get('bail-form-field').eq(2).should(($formField) => expect($formField[0].innerText).to.equal('Phone Number\n\n01234567890'));
          cy.get('bail-form-field').eq(3).should(($formField) => expect($formField[0].innerText).to.equal('Mobile\n\nNot Provided'));
        });

        cy.get('bail-group-data').eq(1).within(() => {
          cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Vehicle'));

          cy.get('bail-form-field').eq(0).should(($formField) => expect($formField[0].innerText).to.equal('Vehicle Type\n\nCar'));
          cy.get('bail-form-field').eq(1).should(($formField) => expect($formField[0].innerText).to.equal('Vehicle Registration\n\nFORD - FIESTA - GREY'));
        });
      });

      cy.get('> bail-group-data').eq(2).within(() => {
        cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Third Parties'));

        cy.get('bail-group-data').eq(0).within(() => {
          cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Contact Details'));

          cy.get('bail-form-field').eq(0).should(($formField) => expect($formField[0].innerText).to.equal('Name\n\nJohn Snow'));
          cy.get('bail-form-field').eq(1).should(($formField) => expect($formField[0].innerText).to.equal('Email Address\n\njohn@example.com'));
          cy.get('bail-form-field').eq(2).should(($formField) => expect($formField[0].innerText).to.equal('Phone Number\n\nNot Provided'));
          cy.get('bail-form-field').eq(3).should(($formField) => expect($formField[0].innerText).to.equal('Mobile\n\n07123 123123'));
        });

        cy.get('bail-group-data').eq(1).within(() => {
          cy.get('.group-data-label').should(($h3) => expect($h3[0].innerText).to.equal('Vehicle'));

          cy.get('bail-form-field').eq(0).should(($formField) => expect($formField[0].innerText).to.equal('Vehicle Type\n\nOther - Tractor'));
          cy.get('bail-form-field').eq(1).should(($formField) => expect($formField[0].innerText).to.equal('Vehicle Registration\n\nVOLKSWAGEN - GOLF - SILVER'));
        });
      });
    });
  });
});
