describe('Form With Multiple Nested Groups Test', () => {
  beforeEach(() => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'forms/form-with-multiple-nested-group.json'});
  });

  it('should render a form with multiple nested form groups', () => {
    // Visit the form test page
    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {
          cy.get('h3').eq(0).should('have.text', 'Witnesses');

          cy.get('bail-form-groups-renderer').should('have.length', 0);

          cy.get('button').contains('Add').eq(0).click();

          cy.get('bail-nested-form-groups-renderer').should('have.length', 1);

          cy.get('bail-nested-form-groups-renderer > bail-form-group-renderer').eq(0).within(() => {
            cy.get('bail-control-group-renderer').within(() => {
              cy.get('h3').should('have.text', 'Details');

              cy.get('.card .card-body bail-question-renderer').within(() => {
                cy.get('form-input').within(() => {
                  cy.get('label').should('have.text', 'Name');
                  cy.get('input[type=text]').should('have.value', '');
                });
              });
            });
          });

          cy.get('bail-nested-form-groups-renderer > bail-form-group-renderer').eq(1).within(() => {
            cy.get('bail-control-group-renderer').within(() => {
              cy.get('h3').should('have.text', 'Contact Details');

              cy.get('.card .card-body bail-question-renderer').within(() => {
                cy.get('form-input').within(() => {
                  cy.get('label').should('have.text', 'Phone Number');
                  cy.get('input[type=text]').should('have.value', '');
                });
              });
            });
          });
        });
      });
    });
  });

  it('should render a form with multiple nested groups and populate the data', () => {
    cy.intercept('GET', 'http://api.bail.test/data', {fixture: 'forms/form-with-multiple-nested-group-data.json'});

    // Visit the form test page
    cy.visit('form.html');

    cy.wait(100);

    cy.get('h3').click();

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').first().within(() => {
          cy.get('h3').eq(0).should('have.text', 'Witnesses');

          cy.get('bail-nested-form-groups-renderer').should('have.length', 2);

          cy.get('bail-nested-form-groups-renderer').eq(0).within(() => {
            cy.get('bail-form-group-renderer').eq(0).within(() => {
              cy.get('bail-control-group-renderer').within(() => {
                cy.get('h3').should('have.text', 'Details');

                cy.get('.card .card-body bail-question-renderer').within(() => {
                  cy.get('form-input').within(() => {
                    cy.get('label').should('have.text', 'Name');
                    cy.get('input[type=text]').should('have.value', 'John Doe');
                  });
                });
              });
            });

            cy.get('bail-form-group-renderer').eq(1).within(() => {
              cy.get('bail-control-group-renderer').within(() => {
                cy.get('h3').should('have.text', 'Contact Details');

                cy.get('.card .card-body bail-question-renderer').within(() => {
                  cy.get('form-input').within(() => {
                    cy.get('label').should('have.text', 'Phone Number');
                    cy.get('input[type=text]').should('have.value', '07123 123123');
                  });
                });
              });
            });
          });

          cy.get('bail-nested-form-groups-renderer').eq(1).within(() => {
            cy.get('bail-form-group-renderer').eq(0).within(() => {
              cy.get('bail-control-group-renderer').within(() => {
                cy.get('h3').should('have.text', 'Details');

                cy.get('.card .card-body bail-question-renderer').within(() => {
                  cy.get('form-input').within(() => {
                    cy.get('label').should('have.text', 'Name');
                    cy.get('input[type=text]').should('have.value', 'Joe Bloggs');
                  });
                });
              });
            });

            cy.get('bail-form-group-renderer').eq(1).within(() => {
              cy.get('bail-control-group-renderer').within(() => {
                cy.get('h3').should('have.text', 'Contact Details');

                cy.get('.card .card-body bail-question-renderer').within(() => {
                  cy.get('form-input').within(() => {
                    cy.get('label').should('have.text', 'Phone Number');
                    cy.get('input[type=text]').should('have.value', '07321 321321');
                  });
                });
              });
            });
          });

          cy.get('button.remove-group').eq(1).click();

          cy.get('bail-nested-form-groups-renderer').should('have.length', 1);
        });
      });
    });
  });
});
