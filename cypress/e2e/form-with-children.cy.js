describe('Form With Children', () => {
  beforeEach(() => {
    cy.intercept('GET', 'http://api.bail.test/form', {fixture: 'forms/form-with-children.json'});
  });

  it('should render a form with children', () => {
    // Visit the form test page
    cy.visit('form.html');

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {
          cy.get('h3').should('have.text', 'Test Group');

          cy.get('bail-control-group-renderer').within(() => {
            cy.get('.card .card-body bail-question-renderer').within(() => {
              cy.get('p').should('have.text', 'Test Question');

              cy.get('bail-child-question-renderer').eq(0).within(() => {
                cy.get('label').should('have.text', 'Child 1');
                cy.get('input[type=text]').should('have.value', '');
              });

              cy.get('bail-child-question-renderer').eq(1).within(() => {
                cy.get('label').should('have.text', 'Child 2');
                cy.get('input[type=text]').should('have.value', '');
              });
            });
          });
        });
      });
    });
  });

  it('should render a form with children and populate the data', () => {
    cy.intercept('GET', 'http://api.bail.test/data', {fixture: 'forms/form-with-children-data.json'});

    // Visit the form test page
    cy.visit('form.html');

    cy.wait(100);

    cy.get('form-renderer').within(() => {
      cy.get('bail-form-groups-renderer').within(() => {
        cy.get('bail-form-group-renderer').within(() => {
          cy.get('h3').should('have.text', 'Test Group');

          cy.get('bail-control-group-renderer').within(() => {
            cy.get('.card .card-body bail-question-renderer').within(() => {
              cy.get('p').should('have.text', 'Test Question');

              cy.get('bail-child-question-renderer').eq(0).within(() => {
                cy.get('label').should('have.text', 'Child 1');
                cy.get('input[type=text]').should('have.value', 'Foo');
              });

              cy.get('bail-child-question-renderer').eq(1).within(() => {
                cy.get('label').should('have.text', 'Child 2');
                cy.get('input[type=text]').should('have.value', 'Bar');
              });
            });
          });
        });
      });
    });
  });
});
