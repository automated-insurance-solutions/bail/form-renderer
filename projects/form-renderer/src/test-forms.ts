import { RenderedForm } from './lib/models/rendered-form.model';
import { FormData } from './lib/models/form-data.model';

export const basicForm: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Test Form',
  description: 'Test Description',
  groups: [
    {
      id: 'groupId',
      label: 'Test Group',
      name: 'test_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'questionId',
          type: 'text',
          body: 'Test Question',
          name: 'test_question',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    }
  ]
};

export const locationForm: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Location Form',
  description: 'Test Description',
  groups: [
    {
      id: 'GgroupId',
      label: 'Locations',
      name: 'locations',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'GgroupId-QwithMapId',
          type: 'location',
          body: 'With Map',
          name: 'with_map',
          default: null,
          hint: null,
          displayMap: true,
          displayStreetView: false,
          captureStreetView: false,
          showMarker: false,
          showLocationFinder: false,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        },
        {
          id: 'GgroupId-QwithStreetViewId',
          type: 'location',
          body: 'With Street View',
          name: 'with_street_view',
          default: null,
          hint: null,
          displayMap: true,
          displayStreetView: true,
          captureStreetView: false,
          showMarker: false,
          showLocationFinder: false,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        },
        {
          id: 'GgroupId-QcaptureStreetViewId',
          type: 'location',
          body: 'Capture Street View',
          name: 'capture_street_view',
          default: null,
          hint: null,
          displayMap: true,
          displayStreetView: true,
          captureStreetView: true,
          showMarker: false,
          showLocationFinder: false,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        },
        {
          id: 'GgroupId-QwithMarker',
          type: 'location',
          body: 'With Marker',
          name: 'with_marker',
          default: null,
          hint: null,
          displayMap: true,
          displayStreetView: false,
          captureStreetView: false,
          showMarker: true,
          showLocationFinder: false,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        },
        {
          id: 'GgroupId-QwithLocationFinder',
          type: 'location',
          body: 'With Location Finder',
          name: 'with_location_finder',
          default: null,
          hint: null,
          displayMap: true,
          displayStreetView: false,
          captureStreetView: false,
          showMarker: true,
          showLocationFinder: true,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    }
  ]
};

export const formWithMultipleGroup: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Foo Form',
  description: 'Test',
  groups: [
    {
      id: 'groupId',
      name: 'test_group',
      label: 'Test Group',
      multiple_label: 'Group',
      multiple: true,
      maximum_groups: null,
      questions: [
        {
          id: 'groupId-question1',
          type: 'text',
          name: 'question_1',
          body: 'Question 1',
          hint: null,
          default: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: []
        },
        {
          id: 'groupId-question2',
          type: 'text',
          name: 'question_2',
          body: 'Question 2',
          hint: null,
          default: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: []
        }
      ],
      conditions: []
    }
  ]
};

export const formWithNestedGroup: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Foo Form',
  description: 'Test',
  groups: [
    {
      id: 'groupId',
      name: 'parent_group',
      label: 'Parent Group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      groups: [
        {
          id: 'groupId-nestedFormId-nestedGroupId',
          name: 'test_group',
          label: 'Test Group',
          multiple: false,
          maximum_groups: null,
          questions: [
            {
              id: 'groupId-nestedFormId-nestedGroupId-question1',
              type: 'text',
              name: 'question_1',
              body: 'Question 1',
              hint: null,
              default: null,
              validators: {
                required: false,
                maxLength: null,
                minLength: null
              },
              conditions: []
            },
            {
              id: 'groupId-nestedFormId-nestedGroupId-question2',
              type: 'text',
              name: 'question_2',
              body: 'Question 2',
              hint: null,
              default: null,
              validators: {
                required: false,
                maxLength: null,
                minLength: null
              },
              conditions: []
            }
          ],
          conditions: []
        }
      ]
    }
  ]
};

export const formWithMultipleNestedGroups: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Foo Form',
  description: 'Test',
  groups: [
    {
      id: 'groupId',
      name: 'witnesses',
      label: 'Witnesses',
      multiple: true,
      maximum_groups: null,
      conditions: [],
      groups: [
        {
          id: 'groupId-nestedFormId-detailsId',
          name: 'details',
          label: 'Details',
          multiple: false,
          maximum_groups: null,
          conditions: [],
          questions: [
            {
              id: 'groupId-nestedFormId-detailsId-nameId',
              type: 'text',
              name: 'name',
              body: 'Name',
              hint: null,
              default: null,
              validators: {
                required: false,
                maxLength: null,
                minLength: null
              },
              conditions: []
            },
          ]
        },
        {
          id: 'groupId-nestedFormId-contactDetailsId',
          name: 'contact_details',
          label: 'Contact Details',
          multiple: false,
          maximum_groups: null,
          conditions: [],
          questions: [
            {
              id: 'groupId-nestedFormId-contactDetailsId-phoneId',
              type: 'text',
              name: 'phone_number',
              body: 'Phone Number',
              hint: null,
              default: null,
              validators: {
                required: false,
                maxLength: null,
                minLength: null
              },
              conditions: []
            },
          ]
        },
      ]
    }
  ]
};

export const formWithChildren: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Test Form',
  description: 'Test Description',
  groups: [
    {
      id: 'groupId',
      label: 'Test Group',
      name: 'test_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'questionId',
          type: 'text',
          body: 'Test Question',
          name: 'test_question',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
          children: [
            {
              id: 'childQuestionId',
              type: 'text',
              body: 'Child Question',
              name: 'child_question',
              default: null,
              hint: null,
              validators: {
                required: false,
                maxLength: null,
                minLength: null
              },
              conditions: [],
            }
          ]
        }
      ]
    }
  ]
};

export const formWithChildrenWithConditions: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Test Form',
  description: 'Test Description',
  groups: [
    {
      id: 'G1',
      label: 'Test Group',
      name: 'test_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'G1-Q1',
          type: 'text',
          body: 'Test Question',
          name: 'test_question',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        },
        {
          id: 'G1-Q2',
          type: 'text',
          body: 'Question With Children',
          name: 'question_with_children',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'equals',
              value: 'Foo'
            }
          ],
          children: [
            {
              id: 'childQuestionId',
              type: 'text',
              body: 'Child Question',
              name: 'child_question',
              default: null,
              hint: null,
              validators: {
                required: false,
                maxLength: null,
                minLength: null
              },
              conditions: [],
            }
          ]
        }
      ]
    }
  ]
};

export const formWithValidators: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Test Form',
  description: 'Test Description',
  groups: [
    {
      id: 'groupId',
      label: 'Test Group',
      name: 'test_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'questionId',
          type: 'text',
          body: 'Test Question',
          name: 'test_question',
          default: null,
          hint: null,
          validators: {
            required: true,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    }
  ]
};

export const formWithConditions: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Test Form',
  description: 'Test Description',
  groups: [
    {
      id: 'G1',
      label: 'Group Without Conditions',
      name: 'groupWithoutConditions',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'G1-Q1',
          type: 'text',
          body: 'Question With Conditions',
          name: 'question_with_conditions',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q2',
              operator: 'equals',
              value: 'Foo',
            }
          ],
        },
        {
          id: 'G1-Q2',
          type: 'text',
          body: 'Question Without Conditions',
          name: 'question_without_conditions',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    },
    {
      id: 'G2',
      label: 'Group With Conditions',
      name: 'group_with_conditions',
      multiple: false,
      maximum_groups: null,
      conditions: [
        {
          id: 'conditionId',
          comparator_id: 'G1-Q2',
          operator: 'equals',
          value: 'Foo',
        }
      ],
      questions: [
        {
          id: 'G2-Q1',
          type: 'text',
          body: 'Has Conditions',
          name: 'has_conditions',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    }
  ]
};

export const formWithStreetViews: RenderedForm = {
  id: 'formId',
  client_id: 'clientId',
  name: 'Test Form',
  description: 'Test Description',
  groups: [
    {
      id: 'G1',
      label: 'Test Group',
      name: 'test_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          "id": "G1-Q1",
          "type": "location",
          "name": "location",
          "body": "Location",
          "hint": null,
          "default": null,
          "validators": {
            "required": false
          },
          "conditions": [],
          "displayMap": true,
          "displayStreetView": true,
          "captureStreetView": true
        }
      ]
    }
  ]
};

export const basicFormData: FormData[] = [
  {
    id: 'groupId',
    type: 'group',
    label: 'Test Group',
    name: 'test_group',
    multiple: false,
    values: [
      {
        id: 'questionId',
        type: 'text',
        label: 'Test Question',
        name: 'test_question',
        value: 'Foo'
      }
    ]
  }
];

export const formWithMultipleGroupData: FormData[] = [
  {
    id: 'groupId',
    type: 'group',
    name: 'test_group',
    label: 'Test Group',
    multiple: true,
    values: [
      [
        {
          id: 'groupId-question1',
          type: 'text',
          name: 'question_1',
          label: 'Question 1',
          value: 'Foo',
        },
        {
          id: 'groupId-question2',
          type: 'text',
          name: 'question_2',
          label: 'Question 2',
          value: 'Bar'
        }
      ],
      [
        {
          id: 'groupId-question1',
          type: 'text',
          name: 'question_1',
          label: 'Question 1',
          value: 'Baz',
        },
        {
          id: 'groupId-question2',
          type: 'text',
          name: 'question_2',
          label: 'Question 2',
          value: 'Qux'
        }
      ],
    ]
  }
];

export const formWithConditionsData: FormData[] = [
  {
    id: 'G1',
    type: 'group',
    label: 'Group Without Conditions',
    name: 'groupWithoutConditions',
    multiple: false,
    values: [
      {
        id: 'G1-Q1',
        type: 'text',
        label: 'Question With Conditions',
        name: 'question_with_conditions',
        value: 'Bar',
      },
      {
        id: 'G1-Q2',
        type: 'text',
        label: 'Question Without Conditions',
        name: 'question_without_conditions',
        value: 'Foo',
      }
    ]
  },
  {
    id: 'G2',
    type: 'group',
    label: 'Group With Conditions',
    name: 'group_with_conditions',
    multiple: false,
    values: [
      {
        id: 'G2-Q1',
        type: 'text',
        label: 'Has Conditions',
        name: 'has_conditions',
        value: 'Baz'
      }
    ]
  }
];

export const formWithNestedGroupsData: FormData[] = [
  {
    id: 'groupId',
    type: 'group',
    name: 'parent_group',
    label: 'Parent Group',
    multiple: false,
    groups: [
      {
        id: 'groupId-nestedFormId-nestedGroupId',
        type: 'group',
        name: 'test_group',
        label: 'Test Group',
        multiple: false,
        values: [
          {
            id: 'groupId-nestedFormId-nestedGroupId-question1',
            type: 'text',
            name: 'question_1',
            label: 'Question 1',
            value: 'Foo',
          },
          {
            id: 'groupId-nestedFormId-nestedGroupId-question2',
            type: 'text',
            name: 'question_2',
            label: 'Question 2',
            value: 'Bar',
          }
        ],
      }
    ]
  }
];

export const formWithMultipleNestedGroupsData: FormData[] = [
  {
    id: 'groupId',
    type: 'group',
    name: 'parent_group',
    label: 'Parent Group',
    multiple: true,
    groups: [
      [
        {
          id: 'groupId-nestedFormId-nestedGroupId',
          type: 'group',
          name: 'test_group',
          label: 'Test Group',
          multiple: false,
          values: [
            {
              id: 'groupId-nestedFormId-nestedGroupId-question1',
              type: 'text',
              name: 'question_1',
              label: 'Question 1',
              value: 'Foo',
            },
            {
              id: 'groupId-nestedFormId-nestedGroupId-question2',
              type: 'text',
              name: 'question_2',
              label: 'Question 2',
              value: 'Bar',
            }
          ],
        }
      ],
      [
        {
          id: 'groupId-nestedFormId-nestedGroupId',
          type: 'group',
          name: 'test_group',
          label: 'Test Group',
          multiple: false,
          values: [
            {
              id: 'groupId-nestedFormId-nestedGroupId-question1',
              type: 'text',
              name: 'question_1',
              label: 'Question 1',
              value: 'Baz',
            },
            {
              id: 'groupId-nestedFormId-nestedGroupId-question2',
              type: 'text',
              name: 'question_2',
              label: 'Question 2',
              value: 'Qux',
            }
          ],
        }
      ]
    ]
  }
];

export const formWithChildrenData: FormData[] = [
  {
    id: 'groupId',
    type: 'group',
    label: 'Test Group',
    name: 'test_group',
    multiple: false,
    values: [
      {
        id: 'questionId',
        type: 'text',
        label: 'Test Question',
        name: 'test_question',
        children: [
          {
            id: 'childQuestionId',
            type: 'text',
            label: 'Child Question',
            name: 'child_question',
            value: 'Foo'
          }
        ]
      }
    ]
  }
];
