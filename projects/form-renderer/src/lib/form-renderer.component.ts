import { ChangeDetectorRef, Component, HostListener, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { RenderedForm } from './models/rendered-form.model';
import { Form } from './form';
import { UntypedFormGroup } from '@angular/forms';
import { FormFactoryService } from './form-factory.service';
import { RenderedFormAnswer } from './models/rendered-form-answer.model';
import { UtilitiesService } from '@automated-insurance-solutions/bail-form-components';

@Component({
  selector: 'bail-form-renderer',
  templateUrl: './form-renderer.component.html',
  styleUrls: []
})
export class FormRendererComponent implements OnInit, OnChanges {

  @Input() form: Form | RenderedForm | string | undefined;
  renderedForm: RenderedForm | undefined;
  groupsControl: UntypedFormGroup | undefined;

  constructor(
    protected formFactory: FormFactoryService,
    protected cd: ChangeDetectorRef,
    protected utilities: UtilitiesService
  ) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.form) {
      this.setForm(changes.form.currentValue);
    }
  }

  valid(): boolean {
    if (this.form instanceof Form) {
      const valid = this.form.valid();

      this.cd.detectChanges();

      return valid;
    }

    return false;
  }

  answers(): RenderedFormAnswer[] | undefined {
    if (this.form instanceof Form) {
      return this.form.answers();
    }

    return undefined;
  }

  protected setForm(form: Form | RenderedForm | string): void {
    switch (typeof form) {
      case 'string':
        const renderedForm = JSON.parse(atob(form)) as RenderedForm;

        form = this.makeForm(renderedForm);

        this.setFormProperties(form);
        break;

      case 'object':
        if (!form.hasOwnProperty('renderedForm')) {
          form = this.makeForm(form as RenderedForm);
        }

        this.setFormProperties(form as Form);

        break;
    }
  }

  protected setFormProperties(form: Form): void {
    this.form = form;
    this.renderedForm = form.renderedForm;
    this.groupsControl = form.formGroup?.get('groups') as UntypedFormGroup;
  }

  protected makeForm(renderedForm: RenderedForm): Form {
    return this.formFactory.make(renderedForm);
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent): void {
    if (!!event.target) {
      this.utilities.clickedElement.next(event.target);
    }
  }

}
