import {Inject, Injectable, Optional} from '@angular/core';
import { RenderedGroup } from './models/rendered-group.model';
import { AbstractControl, AsyncValidatorFn, UntypedFormBuilder, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import { RenderedQuestion } from './models/rendered-question.model';
import { LocalStorageService } from '@automated-insurance-solutions/bail-core-components';
import { RenderedForm } from './models/rendered-form.model';
import { CUSTOM_VALIDATORS, CustomValidator } from './validators/custom-validator';
import { RequiredWithout } from './validators/required-without';
import { debounceTime } from 'rxjs/operators';
import { maxlengthWithoutHtml, minLengthWithoutHtml } from './validators/length-without-html';

@Injectable({
  providedIn: 'root'
})
export class FormConverterService {

  clientId: string | undefined;
  customValidators: Map<string, CustomValidator> = new Map();

  constructor(
    protected fb: UntypedFormBuilder,
    protected localStorage: LocalStorageService,
    @Optional() @Inject(CUSTOM_VALIDATORS) protected validators: CustomValidator[]
  ) { }

  /**
   * Convert the rendered form into an angular FormGroup.
   */
  toFormGroup(form: RenderedForm): UntypedFormGroup {
    const groups = {};

    if (!!this.validators?.length) {
      this.validators.forEach(customValidator => {
        if (typeof customValidator.clientId === 'function')  {
          customValidator.clientId(this.clientId);
        }
        this.customValidators.set(customValidator.key(), customValidator);
      });
    }

    form.groups.forEach(group => {
      if (!group.conditions || !group.conditions.length) {
        groups[group.id] = this.groupToFormGroup(group);
      }
    });

    return this.fb.group({
      groups: this.fb.group(groups)
    });
  }

  /**
   * Convert the rendered group into an angular FormGroup.
   */
  groupToFormGroup(group: RenderedGroup): UntypedFormGroup {
    const formGroup = this.fb.group({
      group_id: group.id,
      child_id: null,
      answers: group.multiple ? this.fb.array([]) : this.groupAnswersControl(group)
    });

    this.addValidatorsThatRequireFormGroup(group, formGroup);

    return formGroup;
  }

  /**
   * Get the control that should be used for the group answers.
   */
  groupAnswersControl(group: RenderedGroup): UntypedFormGroup {
    const formGroup: { [key: string]: any } = {};

    // If the group has nested groups convert them to angular FormGroups and
    // then add them to the answers.
    if (!!group.groups && group.groups.length) {
      group.groups
           .filter(nestedGroup => !nestedGroup.conditions || !nestedGroup.conditions.length)
           .forEach(nestedGroup => formGroup[nestedGroup.id] = this.groupToFormGroup(nestedGroup));
    }

    // Convert the questions to angular FormGroups and added them to the
    // answers.
    if (!!group.questions && group.questions.length) {
      group.questions
           .filter(question => !question.conditions || !question.conditions.length)
           .forEach(question => formGroup[question.id] = this.questionToFormGroup(question));
    }

    return this.fb.group(formGroup);
  }

  /**
   * Convert the question to an angular FormGroup.
   */
  questionToFormGroup(question: RenderedQuestion): UntypedFormGroup {
    if (question.children) {
      const children = {};

      question.children
        // If the questions have conditions they could be specific to a child control
        // so we will let them be added after condition validation.
              .filter(child => !question.conditions || !question.conditions.length)
              .forEach(child => {
                children[child.id] = this.childToFormGroup(child);
              });

      return this.fb.group({
        question_id: question.id,
        children: this.fb.group(children)
      });
    }

    return this.fb.group({
      question_id: question.id,
      answer: this.questionToControl(question)
    });
  }

  /**
   * Convert the child question to an angular FormGroup.
   */
  childToFormGroup(child: RenderedQuestion): UntypedFormGroup {
    return this.fb.group({
      answer: this.questionToControl(child)
    });
  }

  /**
   * Convert the question to the answer FormControl/FormGroup.
   */
  questionToControl(question: RenderedQuestion): AbstractControl {
    switch (question.type) {
      case 'addressLookup':
        return this.fb.group({
          value: [
            this.getInitialValue(question),
            this.questionValidators(question),
            this.questionAsyncValidators(question)
          ],
          // If the address lookup is not required then need to stop the other components being required
          components: this.fb.group({
            address_line_1: [null, question.validators.required ? [Validators.required] : []],
            address_line_2: null,
            city: [null, question.validators.required ? [Validators.required] : []],
            postcode: [null, question.validators.required ? [Validators.required] : []]
          }),
        });
      case 'incidentType':
        return this.fb.group({
          value: [
            this.getInitialValue(question),
            this.questionValidators(question),
            this.questionAsyncValidators(question)
          ],
          parent: null,
        });
      case 'location':
        return this.fb.group({
          value: [
            this.getInitialValue(question),
            this.questionValidators(question),
            this.questionAsyncValidators(question)
          ],
          geocode: null,
          street_views: this.fb.array([]),
        });
      case 'vehicleSelector':
        return this.fb.group({
          value: [
            this.getInitialValue(question),
            this.questionValidators(question),
            this.questionAsyncValidators(question)
          ],
          vehicle: null
        });
      case 'vehicleType':
        return this.fb.group({
          value: [
            this.getInitialValue(question),
            this.questionValidators(question),
            this.questionAsyncValidators(question)
          ],
          other_vehicle_type: null
        });
      case 'vrnLookup':
        return this.fb.group({
          value: [
            this.getInitialValue(question),
            this.questionValidators(question),
            this.questionAsyncValidators(question)
          ],
          vehicle: null
        });
      case 'upload':
        return this.fb.control(
          question.answer || (question.multiple ? [] : null),
          this.questionValidators(question),
          this.questionAsyncValidators(question),
        );
      default:
        return this.fb.control(
          this.getInitialValue(question), this.questionValidators(question), this.questionAsyncValidators(question)
        );
    }
  }

  /**
   * Get the initial value for the question.
   */
  protected getInitialValue(question: RenderedQuestion): any {
    if (question.name === 'reference') {
      return this.localStorage.get(`clientReference.${this.clientId}`) || null;
    }

    return question.answer || question.default || null;
  }

  /**
   * Get the validators to apply to the question.
   */
  protected questionValidators(question: RenderedQuestion): ValidatorFn[] {
    const validators = [];

    if (!!question.validators) {
      if (question.validators.required) {
        validators.push(Validators.required);
      }

      if (question.type === 'email') {
        validators.push(Validators.email);
      }

      if (question.type === 'phone') {
        validators.push(Validators.pattern('^(\\+?)[\\d\\(\\)\\s-]{10,20}$'));
      }

      if (question.validators.maxLength) {
        validators.push(maxlengthWithoutHtml(question.validators.maxLength));
      }

      if (question.validators.minLength) {
        validators.push(minLengthWithoutHtml(question.validators.minLength));
      }

      if (question.validators.max) {
        validators.push(Validators.max(question.validators.max));
      }

      if (question.validators.min) {
        validators.push(Validators.min(question.validators.min));
      }
    }

    return validators;
  }

  /**
   * Get the async validators for the question.
   */
  protected questionAsyncValidators(question: RenderedQuestion): AsyncValidatorFn[] {
    const validators = [];

    if (!!question.validators && !!this.validators?.length) {
      Object.keys(question.validators).forEach(validator => {
        if (!!this.customValidators.get(validator)) {
          validators.push(this.customValidators.get(validator).validate());
        }
      });
    }
    return validators;
  }

  protected addValidatorsThatRequireFormGroup(group: RenderedGroup, formGroup: UntypedFormGroup): void {
    if (!!group.groups?.length) {
      group.groups.forEach(nestedGroup => {
        const nestedFormGroup: UntypedFormGroup | null = formGroup.get(`answers.${nestedGroup.id}`) as UntypedFormGroup;

        if (!!nestedFormGroup) {
          this.addValidatorsThatRequireFormGroup(nestedGroup, nestedFormGroup);
        }
      });
    }

    if (!!group.questions?.length) {
      const hasRequiredWithoutValidators = group.questions.reduce((previous, question) => {
        if (previous) {
          return previous;
        }

        return !!question.validators?.requiredWithout?.length;
      }, false);

      if (hasRequiredWithoutValidators) {
        this.validateRequiredWithout(group, formGroup);

        formGroup.valueChanges.pipe(
          debounceTime(200),
        ).subscribe(() => this.validateRequiredWithout(group, formGroup));
      }
    }
  }

  /**
   * For the required without validation we need access to the form group
   * answers. Unfortunately, we cannot get this in an angular validator.
   *
   * To get around this we run the required without validation when the
   * form group value changes.
   *
   * Here, we run the required without validation for each question in
   * the form group that requires it.
   */
  protected validateRequiredWithout(group: RenderedGroup, formGroup: UntypedFormGroup): void {
    group.questions.forEach(question => {
      if (!!question.validators?.requiredWithout?.length) {
        // Get the answer for the question
        let control = formGroup.get(`answers.${question.id}.answer`);

        if (control instanceof UntypedFormGroup) {
          control = control.get('value');
        }

        if (!!control) {
          // Check if the question already has an answer or if any of the required without
          // questions have answers
          const hasValue = !!control.value || question.validators.requiredWithout.reduce((previous, id) => {
            if (previous) {
              return previous;
            }

            // Get the answer from the form group for the required without control
            const answer = formGroup.value?.answers?.[id]?.answer;

            // We check if the answer is null because if you check the type of null
            // it will return 'object'
            if (answer === null) {
              return false;
            }

            // If the answer is an object it will be answer for a control with additional
            // answer properties e.g. a location control. If the answer is an object we
            // will check the value property of the object
            if (typeof answer === 'object' && answer.hasOwnProperty('value')) {
              return !!answer.value;
            }

            return !!answer;
          }, false);

          // If the control has a value we will reset the validators to the default
          // question validators
          if (hasValue) {
            control.clearValidators();
            control.setValidators(this.questionValidators(question));
          } else {
            // If there isn't an answer we set the required without validation error
            control.setValidators(RequiredWithout.validator(
              question.body,
              question.validators.requiredWithout.map(id => {
                return group.questions.find(question => question.id === id)?.body;
              }).filter(body => !!body)
            ));
          }

          control.updateValueAndValidity({emitEvent: false});
        }
      }
    });
  }
}
