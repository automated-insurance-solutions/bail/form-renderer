import { TestBed } from '@angular/core/testing';

import { FormFactoryService } from './form-factory.service';
import { UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import * as testForms from '../test-forms';
import { Form } from './form';
import { ChangeDetectorRef } from '@angular/core';

describe('FormFactoryService', () => {
  let service: FormFactoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
      ],
      providers: [
        ChangeDetectorRef
      ]
    });
    service = TestBed.inject(FormFactoryService);
  });

  it('should create a form', () => {
    const form = service.make(testForms.basicForm);

    expect(form).toBeInstanceOf(Form);
    expect(form.renderedForm).toBe(testForms.basicForm);
    expect(form.formGroup).toBeInstanceOf(UntypedFormGroup);
  });
});
