import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDataComponent } from './form-data.component';
import * as testForms from '../../test-forms';
import { By } from '@angular/platform-browser';
import { GroupDataComponent } from './group-data/group-data.component';
import { GroupValuesComponent } from './group-data/group-values/group-values.component';
import { ControlDataComponent } from './group-data/control-data/control-data.component';
import { FormFieldComponent } from './group-data/control-data/form-field/form-field.component';

describe('FormDataComponent', () => {
  let component: FormDataComponent;
  let fixture: ComponentFixture<FormDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        FormDataComponent,
        GroupDataComponent,
        GroupValuesComponent,
        ControlDataComponent,
        FormFieldComponent,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should render the form groups', () => {
    component.data = testForms.formWithConditionsData;

    fixture.detectChanges();

    const groupData = fixture.debugElement.queryAll(By.css('bail-group-data'));

    expect(groupData.length).toBe(2);
    expect(groupData[0].context.group).toEqual(testForms.formWithConditionsData[0]);
    expect(groupData[1].context.group).toEqual(testForms.formWithConditionsData[1]);
  });
});
