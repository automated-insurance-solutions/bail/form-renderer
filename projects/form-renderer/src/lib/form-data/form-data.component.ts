import { Component, Input, OnInit } from '@angular/core';
import { FormData } from '../models/form-data.model';

@Component({
  selector: 'bail-form-data',
  templateUrl: './form-data.component.html',
  styleUrls: ['./form-data.component.scss']
})
export class FormDataComponent implements OnInit {

  @Input() data: FormData[] = [];
  @Input() children: any[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
