import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupValuesComponent } from './group-values.component';
import * as testForms from '../../../../test-forms';
import { FormValue } from '../../../models/form-data.model';
import { By } from '@angular/platform-browser';
import { ControlDataComponent } from '../control-data/control-data.component';
import { FormFieldComponent } from '../control-data/form-field/form-field.component';

describe('GroupValuesComponent', () => {
  let component: GroupValuesComponent;
  let fixture: ComponentFixture<GroupValuesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GroupValuesComponent,
        ControlDataComponent,
        FormFieldComponent,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display the group values', () => {
    component.values = testForms.basicFormData[0].values as FormValue[];

    fixture.detectChanges();

    const controlData = fixture.debugElement.query(By.css('bail-control-data'));

    expect(controlData).toBeTruthy();
    expect(controlData.context.data).toEqual(testForms.basicFormData[0].values[0]);
  });

  it('should display a group with children', () => {
    component.values = testForms.formWithChildrenData[0].values as FormValue[];

    fixture.detectChanges();

    const label = fixture.debugElement.query(By.css('.group-value-child-label'));

    expect(label.nativeElement.textContent.trim()).toBe('Test Question');

    const controlData = fixture.debugElement.query(By.css('bail-control-data'));

    const value: FormValue = testForms.formWithChildrenData[0].values[0] as FormValue;

    expect(controlData).toBeTruthy();
    expect(controlData.context.data).toEqual(
      value.children[0]
    );
  });
});
