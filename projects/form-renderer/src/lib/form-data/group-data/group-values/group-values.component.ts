import { Component, Input, OnInit } from '@angular/core';
import { FormValue } from '../../../models/form-data.model';

@Component({
  selector: 'bail-group-values',
  templateUrl: './group-values.component.html',
  styleUrls: ['./group-values.component.scss']
})
export class GroupValuesComponent implements OnInit {

  @Input() values: FormValue[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
