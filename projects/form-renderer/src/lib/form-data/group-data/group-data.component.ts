import { Component, Input, OnInit } from '@angular/core';
import { FormData } from '../../models/form-data.model';

@Component({
  selector: 'bail-group-data',
  templateUrl: './group-data.component.html',
  styleUrls: ['./group-data.component.scss']
})
export class GroupDataComponent implements OnInit {

  @Input() group: FormData | undefined;
  @Input() child = false;

  constructor() { }

  ngOnInit(): void {
  }

}
