import { Component, Input, OnInit } from '@angular/core';
import { ChildValue, FormValue } from '../../../models/form-data.model';

@Component({
  selector: 'bail-control-data',
  templateUrl: './control-data.component.html',
  styleUrls: ['./control-data.component.scss']
})
export class ControlDataComponent implements OnInit {

  @Input() data: FormValue | ChildValue | undefined;
  value: any;

  constructor() { }

  ngOnInit(): void {
    switch (this.data.type) {
      case 'plainTextarea':
        // For plain textarea controls the content is stored in plain text, here we convert the new line characters to <br> HTML tags
        this.value = this.data?.value?.replace(/(?:\r\n|\r|\n)/g, '<br>');
        break;

      default:
        this.value = this.data?.value;
        break;
    }
  }

}
