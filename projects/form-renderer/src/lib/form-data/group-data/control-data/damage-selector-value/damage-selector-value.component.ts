import { Component, Input, OnInit } from '@angular/core';
import { DamageSelectorSection, damageSelectorSections } from '@automated-insurance-solutions/bail-form-components';

@Component({
  selector: 'bail-damage-selector-value',
  templateUrl: './damage-selector-value.component.html',
  styleUrls: ['./damage-selector-value.component.scss']
})
export class DamageSelectorValueComponent implements OnInit {

  @Input() section: DamageSelectorSection;
  @Input() label: string;
  @Input() image: string;
  sections = damageSelectorSections;
  imageUrl: string;
  imageId: string;
  protected defaultImage = 'assets/img/Car.svg';

  constructor() { }

  ngOnInit(): void {
    this.imageUrl = this.image || this.defaultImage;
    this.imageId = `${this.imageUrl.split('/').pop().split('.').shift()}-img`;
  }

}
