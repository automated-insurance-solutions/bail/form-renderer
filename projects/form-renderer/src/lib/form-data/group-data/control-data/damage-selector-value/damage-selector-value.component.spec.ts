import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageSelectorValueComponent } from './damage-selector-value.component';
import { By } from '@angular/platform-browser';
import { DamageSelectorSection } from '@automated-insurance-solutions/bail-form-components';

describe('DamageSelectorValueComponent', () => {
  let component: DamageSelectorValueComponent;
  let fixture: ComponentFixture<DamageSelectorValueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DamageSelectorValueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageSelectorValueComponent);
    component = fixture.componentInstance;
  });

  it('should display the label', () => {
    component.label = 'Test Label';

    fixture.detectChanges();

    const label = fixture.debugElement.query(By.css('.damage-selector-label'));

    expect(label.nativeElement.textContent.trim()).toEqual('Test Label');
  });

  it('should set the selected section', () => {
    component.section = DamageSelectorSection.front;

    fixture.detectChanges();

    const sections = fixture.debugElement.queryAll(By.css('.damage-polygon'));

    // 'front' is the section element in the damageSelectorSections. Here,
    // we are checking that only that section has the selected class
    // applied.
    sections.forEach((section, index) => {
      if (index === 1) {
        expect(section.classes.selected).toBeTrue();
      } else {
        expect(section.classes.selected).toBeUndefined();
      }
    });
  });

  it('should set the image', () => {
    component.image = 'http://localhost/truck.svg';

    fixture.detectChanges();

    // Check the url is being used
    const image = fixture.debugElement.query(By.css('pattern image'));
    expect(image.attributes.href).toEqual('http://localhost/truck.svg');

    // Check the id is set
    expect(component.imageId).toEqual('truck-img');
  });
});
