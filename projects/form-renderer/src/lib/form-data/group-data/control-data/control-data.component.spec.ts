import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlDataComponent } from './control-data.component';
import { By } from '@angular/platform-browser';
import { DateFormatterService, SafeHtmlPipe } from '@automated-insurance-solutions/bail-core-components';
import { UserSubmittedDatePipe } from '../../../user-submitted-date/user-submitted-date.pipe';
import { FormFieldComponent } from './form-field/form-field.component';
import { DamageSelectorValueComponent } from './damage-selector-value/damage-selector-value.component';
import { UploadImagePreviewComponent } from './upload-image-preview/upload-image-preview.component';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'bail-map',
  template: ''
})
class MockMapComponent {
  @Input() latitude: any;
  @Input() longitude: any;
}

describe('ControlDataComponent', () => {
  let component: ControlDataComponent;
  let fixture: ComponentFixture<ControlDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ControlDataComponent,
        UserSubmittedDatePipe,
        SafeHtmlPipe,
        FormFieldComponent,
        DamageSelectorValueComponent,
        UploadImagePreviewComponent,
        MockMapComponent
      ],
      providers: [
        DateFormatterService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlDataComponent);
    component = fixture.componentInstance;
  });

  it('should display a text input', () => {
    component.data = {
      id: 'componentId',
      type: 'text',
      name: 'test_value',
      label: 'Test Value',
      value: 'Foo',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('Foo');
  });

  it('should display a phone input', () => {
    component.data = {
      id: 'componentId',
      type: 'phone',
      name: 'test_value',
      label: 'Test Value',
      value: '07123 123123',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('07123 123123');
  });

  it('should display a range value', () => {
    component.data = {
      id: 'componentId',
      type: 'range',
      name: 'test_value',
      label: 'Test Value',
      value: 10,
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('10');
  });

  it('should display a text input without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'text',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display a multiple select input', () => {
    component.data = {
      id: 'componentId',
      type: 'select',
      name: 'test_value',
      label: 'Test Value',
      multiple: true,
      value: [
        {
          key: 'foo',
          value: 'Foo'
        },
        {
          key: 'bar',
          value: 'Bar'
        },
      ]
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.queryAll(By.css('bail-form-field p'));

    expect(content.length).toBe(2);
    expect(content[0].nativeElement.textContent.trim()).toEqual('Foo');
    expect(content[1].nativeElement.textContent.trim()).toEqual('Bar');
  });

  it('should display a multiple select input without values', () => {
    component.data = {
      id: 'componentId',
      type: 'select',
      name: 'test_value',
      label: 'Test Value',
      multiple: true,
      value: []
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display a location', () => {
    component.data = {
      id: 'componentId',
      type: 'location',
      name: 'test_value',
      label: 'Test Value',
      multiple: true,
      value: 'Norwich, UK'
    };

    fixture.detectChanges();

    // Check the answer is being displayed
    const content = fixture.debugElement.query(By.css('bail-form-field p'));
    expect(content.nativeElement.textContent.trim()).toEqual('Norwich, UK');

    // Check the map is not being displayed
    const map = fixture.debugElement.query(By.css('bail-map'));
    expect(map).toBeNull();

    // Check the street views are not being displayed
    const streetViews = fixture.debugElement.query(By.css('.street-view-values'));
    expect(streetViews).toBeNull();
  });

  it('should display a location with a map', () => {
    component.data = {
      id: 'componentId',
      type: 'location',
      name: 'test_value',
      label: 'Test Value',
      multiple: true,
      value: 'Norwich, UK',
      geocode: {
        geometry: {
          location: {
            lat: 1,
            lng: 2
          }
        }
      }
    };

    fixture.detectChanges();

    // Check the answer is being displayed
    const content = fixture.debugElement.query(By.css('bail-form-field p'));
    expect(content.nativeElement.textContent.trim()).toEqual('Norwich, UK');

    // Check the map is being displayed
    const map = fixture.debugElement.query(By.css('bail-map'));
    expect(map).toBeTruthy();
    expect(map.context.latitude).toEqual(1);
    expect(map.context.longitude).toEqual(2);

    // Check the street views are not being displayed
    const streetViews = fixture.debugElement.query(By.css('.street-view-values'));
    expect(streetViews).toBeNull();
  });

  it('should display a location with a street views', () => {
    component.data = {
      id: 'componentId',
      type: 'location',
      name: 'test_value',
      label: 'Test Value',
      multiple: true,
      value: 'Norwich, UK',
      street_views: [
        {
          url: 'http://localhost/image1',
          caption: null,
        },
        {
          url: 'http://localhost/image2',
          caption: 'Test Caption',
        },
      ]
    };

    fixture.detectChanges();

    // Check the answer is being displayed
    const content = fixture.debugElement.query(By.css('bail-form-field p'));
    expect(content.nativeElement.textContent.trim()).toEqual('Norwich, UK');

    // Check the map is not being displayed
    const map = fixture.debugElement.query(By.css('bail-map'));
    expect(map).toBeNull();

    // Check the street views are being displayed
    const streetViews = fixture.debugElement.query(By.css('.street-view-values'));
    expect(streetViews).toBeTruthy();

    const streetViewImages = fixture.debugElement.queryAll(By.css('.street-view-values .street-view-value .img-thumbnail'));
    expect(streetViewImages.length).toEqual(2);
    expect(streetViewImages[0].properties.src).toEqual('http://localhost/image1');
    expect(streetViewImages[1].properties.src).toEqual('http://localhost/image2');

    const captions = fixture.debugElement.queryAll(By.css('.street-view-values .street-view-value .street-view-caption'));
    expect(captions.length).toEqual(1);
    expect(captions[0].nativeElement.textContent.trim()).toEqual('Test Caption');
  });

  it('should display a textarea', () => {
    component.data = {
      id: 'componentId',
      type: 'textarea',
      name: 'test_value',
      label: 'Test Value',
      value: '<p>Test answer</p>',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field div div'));

    expect(content.properties.innerHTML).toEqual('<p>Test answer</p>');
  });

  it('should display a textarea without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'textarea',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display a textarea', () => {
    component.data = {
      id: 'componentId',
      type: 'textarea',
      name: 'test_value',
      label: 'Test Value',
      value: '<p>Test answer</p>',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field div div'));

    expect(content.properties.innerHTML).toEqual('<p>Test answer</p>');
  });

  it('should display a textarea without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'textarea',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display a plain textarea', () => {
    component.data = {
      id: 'componentId',
      type: 'plainTextarea',
      name: 'test_value',
      label: 'Test Value',
      value: 'Test Answer\nTesting Line breaks',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field div div'));

    expect(content.properties.innerHTML).toEqual('Test Answer<br>Testing Line breaks');
  });

  it('should display a plain textarea without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'plainTextarea',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display a date', () => {
    component.data = {
      id: 'componentId',
      type: 'date',
      name: 'test_value',
      label: 'Test Value',
      value: '2021-01-01T00:00:00+00:00',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('01/01/2021');
  });

  it('should display a date without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'date',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display a switch', () => {
    component.data = {
      id: 'componentId',
      type: 'switch',
      name: 'test_value',
      label: 'Test Value',
      value: true,
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field .badge-success'));

    expect(content.nativeElement.textContent.trim()).toEqual('Yes');
  });

  it('should display a switch with a false value', () => {
    component.data = {
      id: 'componentId',
      type: 'switch',
      name: 'test_value',
      label: 'Test Value',
      value: false,
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field .badge-danger'));

    expect(content.nativeElement.textContent.trim()).toEqual('No');
  });

  it('should display a switch without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'switch',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field .badge-danger'));

    expect(content.nativeElement.textContent.trim()).toEqual('No');
  });

  it('should display an upload', () => {
    component.data = {
      id: 'componentId',
      type: 'upload',
      name: 'test_value',
      label: 'Test Value',
      value: {
        url: 'http://localhost/test.jpg',
        name: 'test.jpg',
      },
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const rows = fixture.debugElement.queryAll(By.css('bail-form-field .table tr'));

    expect(rows.length).toBe(1);

    const cells = rows[0].queryAll(By.css('td'));

    expect(cells[0].query(By.css('bail-upload-image-preview')).context.url).toEqual('http://localhost/test.jpg');
    expect(cells[1].nativeElement.textContent.trim()).toEqual('test.jpg');
  });

  it('should use the web url for an upload if it is provided', () => {
    component.data = {
      id: 'componentId',
      type: 'upload',
      name: 'test_value',
      label: 'Test Value',
      value: {
        url: 'http://localhost/test.jpg',
        thumbnail_url: 'http://localhost/thumbnail.jpg',
        web_url: 'http://localhost/web.jpg',
        name: 'test.jpg',
      },
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const rows = fixture.debugElement.queryAll(By.css('bail-form-field .table tr'));

    expect(rows.length).toBe(1);

    const cells = rows[0].queryAll(By.css('td'));

    expect(cells[0].query(By.css('bail-upload-image-preview')).context.url).toEqual('http://localhost/web.jpg');
    expect(cells[1].nativeElement.textContent.trim()).toEqual('test.jpg');
  });

  it('should use the thumbnail url for an upload if the web url is not provided', () => {
    component.data = {
      id: 'componentId',
      type: 'upload',
      name: 'test_value',
      label: 'Test Value',
      value: {
        url: 'http://localhost/test.jpg',
        thumbnail_url: 'http://localhost/thumbnail.jpg',
        name: 'test.jpg',
      },
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const rows = fixture.debugElement.queryAll(By.css('bail-form-field .table tr'));

    expect(rows.length).toBe(1);

    const cells = rows[0].queryAll(By.css('td'));

    expect(cells[0].query(By.css('bail-upload-image-preview')).context.url).toEqual('http://localhost/thumbnail.jpg');
    expect(cells[1].nativeElement.textContent.trim()).toEqual('test.jpg');
  });

  it('should display a multiple upload', () => {
    component.data = {
      id: 'componentId',
      type: 'upload',
      name: 'test_value',
      label: 'Test Value',
      multiple: true,
      value: [
        {
          url: 'http://localhost/test.jpg',
          name: 'test.jpg',
        },
        {
          url: 'http://localhost/test.docx',
          name: 'test.docx',
        },
      ],
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const rows = fixture.debugElement.queryAll(By.css('bail-form-field .table tr'));

    expect(rows.length).toBe(2);

    let cells = rows[0].queryAll(By.css('td'));

    expect(cells[0].query(By.css('bail-upload-image-preview')).context.url).toEqual('http://localhost/test.jpg');
    expect(cells[1].nativeElement.textContent.trim()).toEqual('test.jpg');

    cells = rows[1].queryAll(By.css('td'));

    expect(cells[0].query(By.css('bail-upload-image-preview')).context.url).toEqual('http://localhost/test.docx');
    expect(cells[1].nativeElement.textContent.trim()).toEqual('test.docx');
  });

  it('should display an upload without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'upload',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p.no-value i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display an incidentType', () => {
    component.data = {
      id: 'componentId',
      type: 'incidentType',
      name: 'test_value',
      label: 'Test Value',
      key: 't_junction',
      value: 'T-Junction',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('T-Junction');
  });

  it('should display an incidentType with a parent', () => {
    component.data = {
      id: 'componentId',
      type: 'incidentType',
      name: 'test_value',
      label: 'Test Value',
      key: 'swerve_to_avoid',
      value: 'Swerve To Avoid',
      parent: 't_junction',
      parent_value: 'T-Junction'
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('T-Junction - Swerve To Avoid');
  });

  it('should not display the parent if the parent is the same as the value', () => {
    component.data = {
      id: 'componentId',
      type: 'incidentType',
      name: 'test_value',
      label: 'Test Value',
      key: 't_junction',
      value: 'T-Junction',
      parent: 't_junction',
      parent_value: 'T-Junction'
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('T-Junction');
  });

  it('should display an incidentType without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'incidentType',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p.no-value i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display a vehicleType', () => {
    component.data = {
      id: 'componentId',
      type: 'vehicleType',
      name: 'test_value',
      label: 'Test Value',
      key: 'carId',
      value: 'Car',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('Car');
  });

  it('should display a vehicleType with an other vehicle description', () => {
    component.data = {
      id: 'componentId',
      type: 'vehicleType',
      name: 'test_value',
      label: 'Test Value',
      key: 'otherId',
      value: 'Other',
      other_vehicle_type: 'Forklift'
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.nativeElement.textContent.trim()).toEqual('Other - Forklift');
  });

  it('should display a vehicleType without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'vehicleType',
      name: 'test_value',
      label: 'Test Value',
      key: null,
      value: null,
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p.no-value i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });

  it('should display a damageSelector without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'damageSelector',
      name: 'test_value',
      label: 'Test Value',
      image: 'http://localhost/image1',
      value: 'front'
    };

    fixture.detectChanges();

    const damageSelector = fixture.debugElement.query(By.css('bail-damage-selector-value'));

    expect(damageSelector).toBeTruthy();
    expect(damageSelector.context.label).toEqual('Test Value');
    expect(damageSelector.context.section).toEqual('front');
    expect(damageSelector.context.image).toEqual('http://localhost/image1');
  });

  it('should display a helperText', () => {
    component.data = {
      id: 'componentId',
      type: 'helperText',
      body: '<p>Testing helper text</p>'
    };

    fixture.detectChanges();

    const helperText = fixture.debugElement.query(By.css('.control-helper-text'));

    expect(helperText).toBeTruthy();
    expect(helperText.nativeElement.textContent.trim()).toEqual('Testing helper text');
  });

  it('should display an address lookup', () => {
    component.data = {
      id: 'componentId',
      type: 'addressLookup',
      name: 'test_value',
      label: 'Test Value',
      value: 'Test address',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field p'));

    expect(content.properties.innerHTML).toEqual('Test address');
  });

  it('should display an address lookup without a value', () => {
    component.data = {
      id: 'componentId',
      type: 'addressLookup',
      name: 'test_value',
      label: 'Test Value',
    };

    fixture.detectChanges();

    const formField = fixture.debugElement.query(By.css('bail-form-field'));

    expect(formField).toBeTruthy();
    expect(formField.context.label).toEqual('Test Value');

    const content = fixture.debugElement.query(By.css('bail-form-field i'));

    expect(content.nativeElement.textContent.trim()).toEqual('Not Provided');
  });
});
