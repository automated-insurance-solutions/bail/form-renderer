import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFieldComponent } from './form-field.component';
import { By } from '@angular/platform-browser';

describe('FormFieldComponent', () => {
  let component: FormFieldComponent;
  let fixture: ComponentFixture<FormFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display the label', () => {
    component.label = 'Test Label';

    fixture.detectChanges();

    const label = fixture.debugElement.query(By.css('label'));

    expect(label.nativeElement.textContent.trim()).toEqual('Test Label');
  });
});
