import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bail-upload-image-preview',
  templateUrl: './upload-image-preview.component.html',
  styleUrls: ['./upload-image-preview.component.css']
})
export class UploadImagePreviewComponent implements OnInit {

  @Input() url: string | undefined;
  isImage = false;
  protected imageExtensions = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg', 'webp'];

  constructor() {
  }

  ngOnInit(): void {
    if (!!this.url) {
      const parts = this.url.split('?')[0].split('.');
      const extension = parts[parts.length - 1] || '';

      this.isImage = this.imageExtensions.includes(extension);
    }
  }

}
