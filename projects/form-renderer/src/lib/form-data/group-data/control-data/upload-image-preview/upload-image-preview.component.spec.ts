import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadImagePreviewComponent } from './upload-image-preview.component';
import { By } from '@angular/platform-browser';

describe('UploadImagePreviewComponent', () => {
  let component: UploadImagePreviewComponent;
  let fixture: ComponentFixture<UploadImagePreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadImagePreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadImagePreviewComponent);
    component = fixture.componentInstance;
  });

  it('should display the image', () => {
    component.url = 'http://localhost/uploadId/test.jpg';

    fixture.detectChanges();

    expect(component.isImage).toBeTrue();
    expect(fixture.debugElement.query(By.css('img.upload-preview'))).toBeTruthy();
  });

  it('should not display the image if the upload is not an image', () => {
    component.url = 'http://localhost/uploadId/test.docx';

    fixture.detectChanges();

    expect(component.isImage).toBeFalse();
    expect(fixture.debugElement.query(By.css('img.upload-preview'))).toBeFalsy();
  });


  it('should check if the file is an image when the url has query parameters', () => {
    component.url = 'http://localhost/uploadId/test.jpg?expires=1234567';

    fixture.detectChanges();

    expect(component.isImage).toBeTrue();
    expect(fixture.debugElement.query(By.css('img.upload-preview'))).toBeTruthy();
  });
});
