import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupDataComponent } from './group-data.component';
import * as testForms from '../../../test-forms';
import { By } from '@angular/platform-browser';
import { GroupValuesComponent } from './group-values/group-values.component';
import { ControlDataComponent } from './control-data/control-data.component';
import { FormFieldComponent } from './control-data/form-field/form-field.component';

describe('GroupDataComponent', () => {
  let component: GroupDataComponent;
  let fixture: ComponentFixture<GroupDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GroupDataComponent,
        GroupValuesComponent,
        ControlDataComponent,
        FormFieldComponent,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display group data', () => {
    component.group = testForms.basicFormData[0];

    fixture.detectChanges();

    // Check that the heading is being displayed
    const heading = fixture.debugElement.query(By.css('.group-data-label'));
    expect(heading.nativeElement.textContent.trim()).toEqual('Test Group');

    // Check that the child heading is not being displayed
    const childHeading = fixture.debugElement.query(By.css('.group-data-child-label'));
    expect(childHeading).toBeNull();

    // Check the group data is being displayed
    const groupData = fixture.debugElement.query(By.css('bail-group-values'));
    expect(groupData).toBeTruthy();
    expect(groupData.context.values).toEqual(testForms.basicFormData[0].values);
  });

  it('should display the data for a multiple group', () => {
    component.group = testForms.formWithMultipleGroupData[0];

    fixture.detectChanges();

    // Check that the heading is being displayed
    const heading = fixture.debugElement.query(By.css('.group-data-label'));
    expect(heading.nativeElement.textContent.trim()).toEqual('Test Group');

    // Check that the child heading is not being displayed
    const childHeading = fixture.debugElement.query(By.css('.group-data-child-label'));
    expect(childHeading).toBeNull();

    // Check the group data is being displayed
    const groupData = fixture.debugElement.queryAll(By.css('bail-group-values'));
    expect(groupData.length).toBe(4);
    expect(groupData[0].context.values).toEqual(testForms.formWithMultipleGroupData[0].values[0][0]);
    expect(groupData[1].context.values).toEqual(testForms.formWithMultipleGroupData[0].values[0][1]);
    expect(groupData[2].context.values).toEqual(testForms.formWithMultipleGroupData[0].values[1][0]);
    expect(groupData[3].context.values).toEqual(testForms.formWithMultipleGroupData[0].values[1][1]);
  });

  it('should display the data for nested group', () => {
    component.group = testForms.formWithNestedGroupsData[0];

    fixture.detectChanges();

    // Check that the heading is being displayed
    const heading = fixture.debugElement.query(By.css('.group-data-label'));
    expect(heading.nativeElement.textContent.trim()).toEqual('Parent Group');

    // Check that the child heading is not being displayed
    const childHeading = fixture.debugElement.query(By.css('.group-data-child-label'));
    expect(childHeading).toBeNull();

    // Check the group data is being displayed
    const groupData = fixture.debugElement.queryAll(By.css('bail-group-data'));
    expect(groupData.length).toBe(1);
  });

  it('should display the data for a multiple nested group', () => {
    component.group = testForms.formWithMultipleNestedGroupsData[0];

    fixture.detectChanges();

    // Check that the heading is being displayed
    const heading = fixture.debugElement.query(By.css('.group-data-label'));
    expect(heading.nativeElement.textContent.trim()).toEqual('Parent Group');

    // Check that the child heading is not being displayed
    const childHeading = fixture.debugElement.queryAll(By.css('.group-data-child-label'));
    expect(childHeading.length).toBe(2);
    expect(childHeading[0].nativeElement.textContent.trim()).toEqual('Test Group');

    // Check the group data is being displayed
    const groupData = fixture.debugElement.queryAll(By.css('bail-group-data'));
    expect(groupData.length).toBe(2);
  });
});
