import { RenderedQuestion } from './rendered-question.model';
import { Condition } from './condition.model';

export interface RenderedGroup {
  id: string;
  name: string;
  label: string;
  multiple: boolean;
  multiple_label?: string;
  add_button_label?: string;
  remove_button_label?: string;
  maximum_groups?: number;
  questions?: RenderedQuestion[];
  groups?: RenderedGroup[];
  conditions: Condition[];
}
