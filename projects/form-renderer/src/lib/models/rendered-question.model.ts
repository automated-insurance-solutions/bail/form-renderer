import { FormControlTypeNames } from './form-control-type.model';
import { Condition } from './condition.model';
import { SelectOption } from '@automated-insurance-solutions/bail-form-components';

export interface RenderedQuestion {
  id: string;
  type: FormControlTypeNames;
  body?: string;
  name?: string;
  hint?: string;
  placeholder?: string;
  default?: any;
  answer?: any;
  hidden?: boolean;
  multiple?: boolean;
  image?: string;
  validators?: {
    required: boolean;
    requiredWithout?: string[];
    uniqueReference?: boolean;
    maxLength?: number;
    minLength?: number;
    max?: number;
    min?: number;
  };
  values?: QuestionValue[];
  children?: RenderedQuestion[];
  conditions?: Condition[];
  [key: string]: any;
}

export interface QuestionValue extends SelectOption {
  key: string;
  value: string | {[key: string]: any, value: string | number};
  image?: string;
}
