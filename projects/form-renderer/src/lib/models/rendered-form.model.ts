import { RenderedGroup } from './rendered-group.model';

export interface RenderedForm {
  id: string;
  client_id: string;
  url?: string;
  name: string;
  description: string;
  groups: RenderedGroup[];
}
