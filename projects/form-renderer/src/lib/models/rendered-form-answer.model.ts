export interface RenderedFormAnswer {
  group_id?: string;
  question_id?: string;
  child_id?: string | null;
  answers?: RenderedFormAnswer[] | {[key: string]: RenderedFormAnswer};
  answer?: string | number | {[key: string]: any, value: string | number};
}
