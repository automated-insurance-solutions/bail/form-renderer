import { FormControlTypeNames } from './form-control-type.model';

export interface FormData {
  id: string;
  type: 'group';
  name: string;
  label: string;
  multiple: boolean;
  child_id?: string;
  groups?: FormData[] | Array<FormData[]>;
  values?: FormValue[] | Array<FormValue[]>;
}

export interface FormValue {
  id: string;
  type?: FormControlTypeNames;
  name?: string;
  label?: string;
  multiple?: boolean;
  value?: any;
  children?: ChildValue[];
  [key: string]: any;
}

export interface ChildValue {
  id: string;
  type: FormControlTypeNames;
  label: string;
  value: any;
  multiple?: boolean;
  [key: string]: any;
}
