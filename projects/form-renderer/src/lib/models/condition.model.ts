export type ConditionOperator =
  'equals'
  | 'does_not_equal'
  | 'greater_than'
  | 'greater_than_or_equal_to'
  | 'less_than'
  | 'less_than_or_equal_to';

export type ConditionConjunction = 'and' | 'or';

export interface Condition {
  id: string;
  comparator_id: string;
  operator: ConditionOperator;
  conjunction?: ConditionConjunction;
  value: any;
}
