export type FormControlTypeNames =
  'text'
  | 'textarea'
  | 'number'
  | 'date'
  | 'time'
  | 'email'
  | 'select'
  | 'selectDialog'
  | 'radio'
  | 'checkbox'
  | 'range'
  | 'switch'
  | 'location'
  | 'partySelect'
  | 'partyType'
  | 'incidentType'
  | 'upload'
  | 'vehicleType'
  | 'vrnLookup'
  | 'vehicleSelector'
  | 'damageSelector'
  | 'helperText'
  | 'addressLookup'
  | 'plainTextarea'
  | 'phone';

export interface FormControlType {
  id?: string;
  name: FormControlTypeNames;
  label: string;
  icon: string;
}
