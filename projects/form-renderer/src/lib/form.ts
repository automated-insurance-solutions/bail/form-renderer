import { UntypedFormArray, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { ChildValue, FormData, FormValue } from './models/form-data.model';
import { except } from './utils';
import isEqual from 'lodash/isEqual';
import { RenderedForm } from './models/rendered-form.model';
import { RenderedGroup } from './models/rendered-group.model';
import { RenderedQuestion } from './models/rendered-question.model';
import { RenderedFormAnswer } from './models/rendered-form-answer.model';
import { FormConverterService } from './form-converter.service';

class FormattedGroup {
  [key: string]: FormattedGroupAnswers;
}

class FormattedGroupAnswers {
  answers: FormattedGroup | FormattedGroup[];
}

export class Form {
  renderedForm: RenderedForm | undefined;
  formGroup: UntypedFormGroup | undefined;
  settingValues = false;

  constructor(
    protected fb: UntypedFormBuilder,
    protected formConverter: FormConverterService,
  ) {
  }

  /**
   * Convert the rendered form into a formGroup and register the condition
   * validator.
   */
  init(form: RenderedForm): Form {
    this.formConverter.clientId = form.client_id;
    this.renderedForm = form;
    this.formGroup = this.formConverter.toFormGroup(this.renderedForm);
    return this;
  }

  /**
   * Get the answers from the form.
   */
  answers(): RenderedFormAnswer[] {
    return Object.values(this.formGroup?.get('groups')?.value || {});
  }

  /**
   * Check if the form is valid.
   */
  valid(): boolean {
    if (!!this.formGroup) {
      this.formGroup.markAllAsTouched();

      return this.formGroup.valid;
    }

    return false;
  }

  /**
   * Convert the rendered group into an angular FormGroup.
   */
  groupToFormGroup(group: RenderedGroup): UntypedFormGroup {
    return this.formConverter.groupToFormGroup(group);
  }

  /**
   * Get the control that should be used for the group answers.
   */
  groupAnswersControl(group: RenderedGroup): UntypedFormGroup {
    return this.formConverter.groupAnswersControl(group);
  }

  /**
   * Convert the rendered group into an angular FormGroup and add it to the
   * provided form group.
   */
  addGroup(formGroup: UntypedFormGroup, group: RenderedGroup): void {
    if (!!formGroup) {
      formGroup.addControl(group.id, this.groupToFormGroup(group));
    }
  }

  /**
   * Remove the group from the FormGroup.
   */
  removeGroup(formGroup: UntypedFormGroup, group: RenderedGroup): void {
    if (!!formGroup) {
      formGroup.removeControl(group.id);
    }
  }

  /**
   * Convert the question to a FormGroup and add it to the provided FormGroup.
   */
  addQuestion(formGroup: UntypedFormGroup, question: RenderedQuestion): void {
    if (!!formGroup) {
      formGroup.addControl(question.id, this.formConverter.questionToFormGroup(question));
    }
  }

  /**
   * Remove the question from the provided FormGroup.
   */
  removeQuestion(formGroup: UntypedFormGroup, question: RenderedQuestion): void {
    if (!!formGroup) {
      formGroup.removeControl(question.id);
    }
  }

  /**
   * Add the child question to the FormGroup.
   */
  addChild(formGroup: UntypedFormGroup, question: RenderedQuestion, child: RenderedQuestion): void {
    if (!!formGroup) {
      formGroup.addControl(child.id, this.formConverter.childToFormGroup(child));
    }
  }

  /**
   * Remove the child question from the FormGroup.
   */
  removeChild(formGroup: UntypedFormGroup, question: RenderedQuestion, child: RenderedQuestion): void {
    if (!!formGroup) {
      formGroup.removeControl(child.id);
    }
  }

  /**
   * Set the answers for the form.
   */
  setValues(groups: FormData[]): Promise<Form> {
    this.settingValues = true;

    return new Promise((resolve) => {
      this.recursivelySetValues(groups, resolve);
    });
  }

  protected recursivelySetValues(groups: FormData[], resolve: (form: Form | PromiseLike<Form>) => void): void {
    const value = this.formGroup.value;

    this.setFormGroupValues(groups);

    // Once we've patched the value of the form the condition validators will be run.
    // Here, we check if the form group has new groups/controls added to it and if it
    // has changed then we call the setValues method recursively until the form group
    // hasn't changed.
    //
    // We tried using ngZone instead of a set timeout but it happens too quickly
    // for the condition validators to have been updated.
    setTimeout(() => {
      // If the form value hasn't updated then all the conditions have been run and
      // we can resolve the promise.
      if (isEqual(value, this.formGroup.value)) {
        this.settingValues = false;

        resolve(this);
      } else {
        this.recursivelySetValues(groups, resolve);
      }
    }, 100);
  }

  protected setFormGroupValues(groups: FormData[]): void {
    groups.forEach(group => {
      const formGroup = this.formGroup.get(`groups.${group.id}`) as UntypedFormGroup;

      if (formGroup) {
        return this.setGroupValues(
          group,
          formGroup,
          this.renderedForm?.groups?.find(g => g.id === group.id)
        );
      }
    });
  }

  /**
   * Set the value of a FormGroup control.
   */
  protected setGroupValues(group: FormData, formGroup: UntypedFormGroup | undefined, renderedGroup: RenderedGroup): void {
    const answersControl = formGroup?.get('answers');

    if (!!answersControl) {
      if (answersControl instanceof UntypedFormArray) {
        this.setArrayValues(group, answersControl, renderedGroup);
      } else if (answersControl instanceof UntypedFormGroup) {
        answersControl.patchValue(this.formatGroup(group).answers);
      }
    }
  }

  /**
   * Set the value of a FormArray control.
   */
  protected setArrayValues(group: FormData, formArray: UntypedFormArray, renderedGroup: RenderedGroup): void {
    if (!!group.groups) {
      if (group.groups.length !== formArray.length) {
        formArray.clear();

        group.groups.forEach((g: FormData | FormData[]) => {
          if (Array.isArray(g)) {
            const formGroup = this.groupAnswersControl({
              ...renderedGroup,
              multiple: false,
            });

            g.forEach((nestedGroup: FormData, index: number) => {
              const nestedFormGroup = formGroup.get(nestedGroup.id) as UntypedFormGroup;

              this.setGroupValues(nestedGroup, nestedFormGroup, renderedGroup[index]);

              if (nestedGroup.child_id) {
                nestedFormGroup?.get('child_id')?.setValue(nestedGroup.child_id);
              }
            });

            formArray.push(formGroup);
          } else {
            const formGroup = this.groupToFormGroup(renderedGroup.groups[0]);

            this.setGroupValues(g as FormData, formGroup, renderedGroup.groups[0]);

            formArray.push(formGroup);
          }
        });
      } else {
        group.groups.forEach((g: FormData | FormData[], index: number) => {
          if (Array.isArray(g)) {
            const formGroup = formArray.at(index);

            g.forEach((nestedGroup: FormData, index: number) => {
              this.setGroupValues(nestedGroup, formGroup.get(nestedGroup.id) as UntypedFormGroup, renderedGroup[index]);
            });
          } else {
            const formGroup = formArray.at(index) as UntypedFormGroup;

            this.setGroupValues(g as FormData, formGroup, renderedGroup.groups[0]);
          }
        });
      }
    } else if (!!group.values) {
      if (group.values.length !== formArray.length) {
        formArray.clear();

        group.values.forEach((value: FormValue | FormValue[]) => {
          const formGroup = this.groupAnswersControl({
            ...renderedGroup,
            multiple: false,
          });

          if (Array.isArray(value)) {
            value.forEach(v => {
              formGroup.get(v.id)?.patchValue(this.formatValue(v));
            });
          } else {
            formGroup.patchValue(this.formatValue(value));
          }

          formArray.push(formGroup);
        });
      }
    }
  }

  /**
   * Format the group answers so they can be patched to the FormGroup.
   */
  protected formatGroup(group: FormData): FormattedGroupAnswers {
    // If the group has the multiple flag set we need to format the values
    // for a FormArray.
    if (group.multiple) {
      return this.formatMultipleGroupAnswers(group);
    }

    const formatted = {
      child_id: null,
      answers: {}
    };

    if (group.child_id) {
      formatted.child_id = group.child_id;
    }

    if (!!group.groups && !!group.groups.length) {
      group.groups.forEach(innerGroup => {
        formatted.answers[innerGroup.id] = this.formatGroup(innerGroup);
      });
    }

    if (!!group.values && !!group.values.length) {
      group.values.forEach(value => {
        const answer = this.formatValue(value);

        if (answer) {
          formatted.answers[value.id] = answer;
        }
      });
    }

    return formatted;
  }

  /**
   * Format the answers for a multiple group.
   */
  protected formatMultipleGroupAnswers(group: FormData): FormattedGroupAnswers {
    const formatted = {
      answers: []
    };

    if (!!group.groups && !!group.groups.length) {
      group.groups.forEach(data => {
        const groupAnswers = {};

        data.forEach(innerGroup => {
          groupAnswers[innerGroup.id] = this.formatGroup(innerGroup);
        });

        formatted.answers.push(groupAnswers);
      });
    } else if (!!group.values && !!group.values.length) {
      group.values.forEach(data => {
        formatted.answers.push(
          data.map(value => this.formatValue(value))
        );
      });
    }

    return formatted;
  }

  /**
   * Format the FormValue to be ready to be patched to a FormGroup.
   */
  protected formatValue(value: FormValue): { [key: string]: any } | undefined {
    if (value.children) {
      const answers = {};

      value.children.forEach(child => {
        answers[child.id] = {answer: this.getAnswer(child)};
      });

      return {children: answers};
    }

    const answer = this.getAnswer(value);

    if (answer) {
      return {answer};
    }

    return undefined;
  }

  /**
   * Get the answer for the from the FormValue. Some questions use a FormGroup
   * rather than a FormControl so this can either return an object to populate
   * a FormGroup or just the value to set for the answer FormControl.
   */
  protected getAnswer(value: FormValue | ChildValue): string | number | { [key: string]: any } {
    switch (value.type) {
      case 'addressLookup':
        return {
          value: value.value,
          components: value.components || null
        };

      case 'incidentType':
        return {
          value: value.key,
          parent: value.parent || null
        };

      case 'location':
        this.setStreetViews(value);

        return except(value, [
          'id',
          'type',
          'name',
          'label',
          'children',
          'street_views',
        ]);

      case 'radio':
        return value.key;

      case 'select':
      case 'selectDialog':
        if (value.multiple) {
          return value.value;
        }

        return value.key;

      case 'vehicleSelector':
      case 'vrnLookup':
        return except(value, [
          'id',
          'type',
          'name',
          'label',
          'children',
        ]);


      case 'vehicleType':
        return {
          value: value.key,
          other_vehicle_type: value.other_vehicle_type || null
        };

      default:
        return value.value;
    }
  }

  /**
   * The street views are stored as a FormArray. Form arrays don't get repopulated
   * by doing formGroup.patch() so here we are manually pushing the controls into
   * the FormArray.
   */
  protected setStreetViews(value: FormValue | ChildValue): void {
    const groupIds: string[] = [];
    const parts = value.id.split('-');

    parts.reduce((carry: string, part: string) => {
      if (!carry) {
        carry = part;
      } else {
        carry = `${carry}-${part}`;
      }

      if (part.startsWith('G')) {
        groupIds.push(carry);
      }

      return carry;
    }, '');

    if (!!value.street_views && !!value.street_views.length) {
      const control = this.formGroup.get(
        `groups.${groupIds.join('.answers.')}.answers.${value.id}.answer.street_views`
      ) as UntypedFormArray;

      if (!!control) {
        value.street_views.forEach(streetView => {
          // Check if the street view has already been added to the form
          // control.
          if (!this.containsStreetView(control, streetView)) {
            control.push(this.fb.group(streetView));
          }
        });
      }
    }
  }

  /**
   * Check to see if the street view as already been added to the form array.
   */
  protected containsStreetView(control: UntypedFormArray, streetView: any): boolean {
    for (const value of control.value) {
      if (value.url === streetView.url && value.caption === streetView.caption) {
        return true;
      }
    }

    return false;
  }
}
