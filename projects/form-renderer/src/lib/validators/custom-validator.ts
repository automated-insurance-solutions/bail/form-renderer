import { InjectionToken } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';

export const CUSTOM_VALIDATORS: InjectionToken<CustomValidator[]> = new InjectionToken<CustomValidator[]>('CustomValidators');

export interface CustomValidator {
  key(): string;

  validate(): (control: AbstractControl) => Observable<ValidationErrors | null>;

  clientId?(id: string): CustomValidator;
}
