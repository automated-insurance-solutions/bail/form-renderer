import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

/**
 * Validator that checks the value is more than the minimum length. If
 * the value is an HTML string we strip the HTML before checking.
 *
 * @example minLengthWithoutHtml(10)
 */
export function minLengthWithoutHtml(minLength: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = stripHtml(control.value);

    if (isEmptyInputValue(value) || !hasValidLength(value)) {
      // don't validate empty values to allow optional controls
      // don't validate values without `length` property
      return null;
    }

    return value.length < minLength ?
      {minlength: {requiredLength: minLength, actualLength: value.length}} :
      null;
  };
}


/**
 * Validator that checks the value is less than the maximum length. If
 * the value is an HTML string we strip the HTML before checking.
 *
 * @example maxLengthWithoutHtml(10)
 */
export function maxlengthWithoutHtml(maxLength: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = stripHtml(control.value);

    return hasValidLength(value) && value.length > maxLength ?
      {maxlength: {requiredLength: maxLength, actualLength: value.length}} :
      null;
  };
}

function isEmptyInputValue(value: any): boolean {
  /**
   * Check if the object is a string or array before evaluating the length attribute.
   * This avoids falsely rejecting objects that contain a custom length attribute.
   * For example, the object {id: 1, length: 0, width: 0} should not be returned as empty.
   */
  return value == null ||
    ((typeof value === 'string' || Array.isArray(value)) && value.length === 0);
}

function hasValidLength(value: any): boolean {
  // non-strict comparison is intentional, to check for both `null` and `undefined` values
  return value != null && typeof value.length === 'number';
}

function stripHtml(value: any): any {
  if (typeof value === 'string') {
    return value?.replace(/<[^>]*>/g, '') || null;
  }

  return value;
}
