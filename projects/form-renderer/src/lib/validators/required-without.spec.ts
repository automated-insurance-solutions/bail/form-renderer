import { RequiredWithout } from './required-without';
import { UntypedFormControl } from '@angular/forms';

describe('Required Without', () => {
  it('should build the validation message for a single label', () => {
    const validator = RequiredWithout.validator('Foo', ['Bar']);

    expect(validator(new UntypedFormControl()).requiredWithout).toEqual(`Foo is required when 'Bar' is not present`);
  });

  it('should build the validation message for a multiple labels', () => {
    const validator = RequiredWithout.validator('Foo', ['Bar', 'Baz', 'Qux']);

    expect(validator(new UntypedFormControl()).requiredWithout).toEqual(`Foo is required when 'Bar', 'Baz', or 'Qux' are not present`);
  });

  it('should return null if there are no labels', () => {
    const validator = RequiredWithout.validator('Foo', []);

    expect(validator(new UntypedFormControl())).toBeNull();
  });
});
