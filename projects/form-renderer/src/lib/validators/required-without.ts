import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export class RequiredWithout {
  public static validator(label: string, labels: string[] = []): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (labels.length) {
        return {
          requiredWithout: `${label} is required when ${this.naturalJoin(labels)} ${labels.length === 1 ? 'is' :
            'are'} not present`
        };
      }

      return null;
    }
  }

  protected static naturalJoin(labels: string[]): string {
    if (labels.length === 1) {
      return `'${labels[0]}'`;
    }

    return labels.map((label: string, index: number) => {
      if (index === labels.length - 1) {
        return `or '${label}'`;
      }

      return `'${label}'`;
    }).join(', ');
  }
}
