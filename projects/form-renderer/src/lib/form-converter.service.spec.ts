import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormConverterService } from './form-converter.service';
import { RenderedGroup } from './models/rendered-group.model';
import { AbstractControl, UntypedFormGroup, ReactiveFormsModule, ValidationErrors, Validators } from '@angular/forms';
import { CUSTOM_VALIDATORS, CustomValidator } from './validators/custom-validator';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
class UniqueReference implements CustomValidator {
  key(): string {
    return 'customValidator';
  }

  validate(): (control: AbstractControl) => Observable<ValidationErrors | null> {
    return (control) => {
      return null;
    };
  }

}

describe('FormConverterService', () => {
  let service: FormConverterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
      ],
      providers: [
        FormConverterService,
        {provide: CUSTOM_VALIDATORS, useClass: UniqueReference, multi: true}
      ],
    });
    service = TestBed.inject(FormConverterService);
  });

  it('should inject a token', () => {
    const token = TestBed.inject(CUSTOM_VALIDATORS);
    expect(token[0]).toBeInstanceOf(UniqueReference);
    expect(token[0].key()).toEqual('customValidator');
  });

  it('should convert the group to a form group', () => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newQuestionId',
          type: 'text',
          body: 'New Question',
          name: 'new_question',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);
    expect(formGroup.value).toEqual({
      group_id: 'newGroupId',
      child_id: null,
      answers: {
        newQuestionId: {
          question_id: 'newQuestionId',
          answer: null,
        }
      }
    });
  });

  it('should convert the multiple group to a form group', () => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: true,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newQuestionId',
          type: 'text',
          body: 'New Question',
          name: 'new_question',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);
    expect(formGroup.value).toEqual({
      group_id: 'newGroupId',
      child_id: null,
      answers: []
    });
  });

  it('should convert the nested group to a form group', () => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      groups: [
        {
          id: 'nestedGroupId',
          label: 'Nested Group',
          name: 'nested_group',
          multiple: false,
          maximum_groups: null,
          conditions: [],
          questions: [
            {
              id: 'questionId',
              type: 'text',
              body: 'Test Question',
              name: 'test_question',
              default: null,
              hint: null,
              validators: {
                required: false,
                maxLength: null,
                minLength: null
              },
              conditions: [],
            }
          ]
        }
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);
    expect(formGroup.value).toEqual({
      group_id: 'newGroupId',
      child_id: null,
      answers: {
        nestedGroupId: {
          group_id: 'nestedGroupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            }
          }
        }
      }
    });
  });

  it('should not add the group questions if they have conditions', () => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-newQuestionId',
          type: 'text',
          body: 'New Question',
          name: 'new_question',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'groupId-questionId',
              operator: 'equals',
              value: 'Foo',
            }
          ],
        },
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);
    expect(formGroup.value).toEqual({
      group_id: 'newGroupId',
      child_id: null,
      answers: {}
    });
  });

  it('should not add the nested groups if they have conditions', () => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      groups: [
        {
          id: 'nestedGroupId',
          label: 'Nested Group',
          name: 'nested_group',
          multiple: false,
          maximum_groups: null,
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'groupId-questionId',
              operator: 'equals',
              value: 'Foo',
            }
          ],
          questions: [
            {
              id: 'questionId',
              type: 'text',
              body: 'Test Question',
              name: 'test_question',
              default: null,
              hint: null,
              validators: {
                required: false,
                maxLength: null,
                minLength: null
              },
              conditions: [],
            }
          ]
        }
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);
    expect(formGroup.value).toEqual({
      group_id: 'newGroupId',
      child_id: null,
      answers: {}
    });
  });

  it('should add max length validation', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-question1',
          type: 'text',
          body: 'Question 1',
          name: 'question_1',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: 10,
            minLength: null
          },
          conditions: [],
        },
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    expect(formGroup.get(`answers.newGroupId-question1.answer`).getError('maxLength')).toBeNull();

    formGroup.get('answers.newGroupId-question1.answer').setValue('Testing Max Length');

    expect(formGroup.get(`answers.newGroupId-question1.answer`).errors.maxlength).toBeTruthy();

    formGroup.get('answers.newGroupId-question1.answer').setValue('ValidValue');

    expect(formGroup.get(`answers.newGroupId-question1.answer`).errors).toBeNull();
  }));

  it('should strip html from a value before checking max length', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-question1',
          type: 'text',
          body: 'Question 1',
          name: 'question_1',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: 10,
            minLength: null
          },
          conditions: [],
        },
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    expect(formGroup.get(`answers.newGroupId-question1.answer`).getError('maxLength')).toBeNull();

    formGroup.get('answers.newGroupId-question1.answer').setValue('<p>Testing max length</p>');

    expect(formGroup.get(`answers.newGroupId-question1.answer`).errors.maxlength.actualLength).toEqual(18);

    formGroup.get('answers.newGroupId-question1.answer').setValue('<p>ValidValue</p>');

    expect(formGroup.get(`answers.newGroupId-question1.answer`).errors).toBeNull();
  }));

  it('should add the required without validation', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-question1',
          type: 'text',
          body: 'Question 1',
          name: 'question_1',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        },
        {
          id: 'newGroupId-question2',
          type: 'text',
          body: 'Question 2',
          name: 'question_2',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null,
            requiredWithout: [
              'newGroupId-question1'
            ]
          },
          conditions: [],
        }
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    expect(formGroup.get(`answers.newGroupId-question2.answer`).getError('requiredWithout')).toEqual(
      `Question 2 is required when 'Question 1' is not present`
    );

    formGroup.get('answers.newGroupId-question1.answer').setValue('Test');

    tick(300);

    expect(formGroup.get(`answers.newGroupId-question2.answer`).getError('requiredWithout')).toBeNull();

    formGroup.get('answers.newGroupId-question1.answer').setValue('');

    tick(300);

    expect(formGroup.get(`answers.newGroupId-question2.answer`).getError('requiredWithout')).toEqual(
      `Question 2 is required when 'Question 1' is not present`
    );
  }));

  it('should add the required without validation for an input with an object answer', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          "id": "newGroupId-question1",
          "type": "location",
          "name": "question_1",
          "body": "Question 1",
          "default": null,
          "hint": null,
          "validators": {
            "required": false
          },
          "conditions": [],
          "displayMap": null,
          "displayStreetView": null,
          "captureStreetView": null
        },
        {
          id: 'newGroupId-question2',
          type: 'text',
          body: 'Question 2',
          name: 'question_2',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null,
            requiredWithout: [
              'newGroupId-question1'
            ]
          },
          conditions: [],
        }
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    expect(formGroup.get(`answers.newGroupId-question2.answer`).getError('requiredWithout')).toEqual(
      `Question 2 is required when 'Question 1' is not present`
    );

    formGroup.get('answers.newGroupId-question1.answer.value').setValue('Test');

    tick(300);

    expect(formGroup.get(`answers.newGroupId-question2.answer`).getError('requiredWithout')).toBeNull();

    formGroup.get('answers.newGroupId-question1.answer.value').setValue('');

    tick(300);

    expect(formGroup.get(`answers.newGroupId-question2.answer`).getError('requiredWithout')).toEqual(
      `Question 2 is required when 'Question 1' is not present`
    );
  }));

  it('should add the required without validation when the subject question has an object value', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-question1',
          type: 'text',
          body: 'Question 1',
          name: 'question_1',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        },
        {
          "id": "newGroupId-question2",
          "type": "location",
          "name": "question_2",
          "body": "Question 2",
          "default": null,
          "hint": null,
          "validators": {
            "required": false,
            "requiredWithout": [
              'newGroupId-question1'
            ]
          },
          "conditions": [],
          "displayMap": null,
          "displayStreetView": null,
          "captureStreetView": null
        }
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    // Assert the error displays initially
    expect(formGroup.get(`answers.newGroupId-question2.answer.value`).getError('requiredWithout')).toEqual(
      `Question 2 is required when 'Question 1' is not present`
    );

    formGroup.get('answers.newGroupId-question1.answer').setValue('Test');

    tick(300);

    // Assert the error does not display if the required without control has a value
    expect(formGroup.get(`answers.newGroupId-question2.answer.value`).getError('requiredWithout')).toBeNull();

    formGroup.get('answers.newGroupId-question1.answer').setValue('');

    tick(300);

    // Assert the warning displays when the required without value is removed
    expect(formGroup.get(`answers.newGroupId-question2.answer.value`).getError('requiredWithout')).toEqual(
      `Question 2 is required when 'Question 1' is not present`
    );

    formGroup.get('answers.newGroupId-question2.answer.value').setValue('Norwich');

    tick(300);

    // Assert the warning does not display if the location control has a value
    expect(formGroup.get(`answers.newGroupId-question2.answer.value`).getError('requiredWithout')).toBeNull();
  }));

  it('should add required to address lookup components if address lookup is required', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-question1',
          type: 'addressLookup',
          body: 'Question 1',
          name: 'question_1',
          default: null,
          hint: null,
          validators: {
            required: true,
          },
          conditions: [],
        },
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    const answerFormGroup = formGroup.get(`answers.newGroupId-question1.answer`);
    const componentsFormGroup = answerFormGroup.get('components');

    expect(componentsFormGroup.get(`address_line_1`).hasValidator(Validators.required)).toBeTrue();
    expect(componentsFormGroup.get(`address_line_2`).hasValidator(Validators.required)).toBeFalse();
    expect(componentsFormGroup.get(`city`).hasValidator(Validators.required)).toBeTrue();
    expect(componentsFormGroup.get(`postcode`).hasValidator(Validators.required)).toBeTrue();
  }));

  it('should not add required to address lookup components if address lookup is optional', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-question1',
          type: 'addressLookup',
          body: 'Question 1',
          name: 'question_1',
          default: null,
          hint: null,
          validators: {
            required: false,
          },
          conditions: [],
        },
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    const answerFormGroup = formGroup.get(`answers.newGroupId-question1.answer`);
    const componentsFormGroup = answerFormGroup.get('components');

    expect(componentsFormGroup.get(`address_line_1`).hasValidator(Validators.required)).toBeFalse();
    expect(componentsFormGroup.get(`address_line_2`).hasValidator(Validators.required)).toBeFalse();
    expect(componentsFormGroup.get(`city`).hasValidator(Validators.required)).toBeFalse();
    expect(componentsFormGroup.get(`postcode`).hasValidator(Validators.required)).toBeFalse();
  }));

  it('should add email validation to email controls', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-question1',
          type: 'email',
          body: 'Question 1',
          name: 'question_1',
          default: null,
          hint: null,
          validators: {
            required: false,
          },
          conditions: [],
        },
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    // Set an invalid email
    formGroup.get('answers.newGroupId-question1.answer').setValue('abc');

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    expect(formGroup.get(`answers.newGroupId-question1.answer`).hasValidator(Validators.email)).toBeTrue();

    // Assert the error displays initially
    expect(formGroup.get(`answers.newGroupId-question1.answer`).getError('email')).toBeTruthy();

    formGroup.get('answers.newGroupId-question1.answer').setValue('test@example.com');

    tick(300);

    // Assert the error does not display if the control has a correct value
    expect(formGroup.get(`answers.newGroupId-question1.answer`).getError('email')).toBeNull();
  }));

  it('should add pattern validation to phone controls', fakeAsync(() => {
    const group: RenderedGroup = {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newGroupId-question1',
          type: 'phone',
          body: 'Question 1',
          name: 'question_1',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    };

    const formGroup = service.groupToFormGroup(group);

    // Set an invalid phone number
    formGroup.get('answers.newGroupId-question1.answer').setValue('abc');

    expect(formGroup).toBeInstanceOf(UntypedFormGroup);

    // Assert the error displays initially
    expect(formGroup.get(`answers.newGroupId-question1.answer`).getError('pattern')).toBeTruthy();

    formGroup.get('answers.newGroupId-question1.answer').setValue('01234567890');

    tick(300);

    // Assert the error does not display if the control has a correct value
    expect(formGroup.get(`answers.newGroupId-question1.answer`).getError('pattern')).toBeNull();
  }));
});
