import { Pipe, PipeTransform } from '@angular/core';

/**
 * When users submit dates we store them in the ISO format
 * (e.g. 2023-01-01T00:00:00+01:00).
 *
 * When the user selects the date it will store it with their timezone.
 * If a user in a different timezone views the date it will convert it
 * to their own timezone which can cause the dates to be different.
 *
 * For example, if a user was in India and selected a date like so
 * 2023-06-11T00:00:00+05:30 it would display as 10/06/2023 to someone
 * in the UK.
 *
 * When we are displaying user submitted dates we only care about the
 * date they have selected, not their timezone or the time they selected
 * the value.
 *
 * Here, we remove the additional properties from the date string and just
 * format the date to DD/MM/YYYY format.
 *
 * This should only be used for user submitted dates, if you are dealing
 * with system dates you should use the standard angular date pipe.
 *
 * @example claimData.incident_date | userSubmittedDate
 */

@Pipe({
  name: 'userSubmittedDate'
})
export class UserSubmittedDatePipe implements PipeTransform {

  transform(value: string): string {
    if (value) {
      const matches = value.match(/\d{4}-\d{2}-\d{2}/gs);

      if (matches?.length) {
        return matches[0].split('-').reverse().join('/');
      }
    }

    return value;
  }

}
