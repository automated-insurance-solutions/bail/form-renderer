import { UserSubmittedDatePipe } from './user-submitted-date.pipe';

describe('UserSubmittedDatePipe', () => {
  let pipe: UserSubmittedDatePipe;

  beforeEach(() => {
    pipe = new UserSubmittedDatePipe();
  });

  it('should format an ISO date string', () => {
    const date = pipe.transform('2023-03-23T00:00:00+00:00');

    expect(date).toEqual('23/03/2023');
  });

  it('should ignore the timezone in an ISO date string', () => {
    const date = pipe.transform('2023-03-23T00:00:00+05:30');

    expect(date).toEqual('23/03/2023');
  });

  it('should handle the value not being an ISO date string', () => {
    const date = pipe.transform('01/01/2024');

    expect(date).toEqual('01/01/2024');
  });
});
