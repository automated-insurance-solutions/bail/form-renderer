import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRendererNavigationComponent } from './form-renderer-navigation.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Form } from '../form';
import { UntypedFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { FormFactoryService } from '../form-factory.service';
import { FormConverterService } from '../form-converter.service';
import * as testForms from '../../test-forms';
import { ChangeDetectorRef } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('FormRendererNavigationComponent', () => {
  let component: FormRendererNavigationComponent;
  let fixture: ComponentFixture<FormRendererNavigationComponent>;
  let form: Form;
  let formFactory: FormFactoryService;
  let formConverter: FormConverterService;
  let fb: UntypedFormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormRendererNavigationComponent ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
      ],
      providers: [
        ChangeDetectorRef,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    formFactory = TestBed.inject(FormFactoryService);
    formConverter = TestBed.inject(FormConverterService);
    fb = TestBed.inject(UntypedFormBuilder);
    fixture = TestBed.createComponent(FormRendererNavigationComponent);
    component = fixture.componentInstance;
  });

  it('should render the menu items', () => {
    form = formFactory.make(testForms.basicForm);

    component.form = form;
    component.items = [
      {
        fragment: 'foo',
        label: 'Foo'
      },
      {
        fragment: 'bar',
        label: 'Bar'
      },
    ];

    fixture.detectChanges();

    expect(fixture.debugElement.queryAll(By.css('.form-renderer-navigation-item')).length).toEqual(2);

    const links = fixture.debugElement.queryAll(By.css('.form-renderer-navigation-link'));

    expect(links.length).toEqual(2);
    expect(links[0].nativeElement.innerText).toEqual('Foo');
    expect(links[1].nativeElement.innerText).toEqual('Bar');
    expect(links[0].properties.href).toEqual('#foo');
    expect(links[1].properties.href).toEqual('#bar');
  });
});
