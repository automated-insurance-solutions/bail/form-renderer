import { AfterViewInit, Component, ElementRef, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RenderedGroup } from '../models/rendered-group.model';
import { combineLatest, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Form } from '../form';

declare const $: any;

export class MenuItem {
  fragment: string;
  label: string;
  active?: boolean;
}

@Component({
  selector: 'bail-form-renderer-navigation',
  templateUrl: './form-renderer-navigation.component.html',
  styleUrls: ['./form-renderer-navigation.component.scss']
})
export class FormRendererNavigationComponent implements OnInit, AfterViewInit {

  @Input() element: ElementRef | undefined;
  @Input() form: Form | undefined;
  items: MenuItem[] = [];

  constructor(protected route: ActivatedRoute, protected router: Router) {
  }

  ngOnInit(): void {
    this.route?.fragment.subscribe(fragment => {
      const element = document.querySelector(`[data-scroll="${fragment}"]`);

      if (element && !!this.items?.length) {
        element.scrollIntoView(true);

        const item = this.items.find(i => i.fragment === fragment);

        this.activate(item);
      }
    });
  }

  ngAfterViewInit(): void {
    if (!!this.element) {
      // We run this in a setTimeout to force it to be run in the next digest
      // cycle, this fixes an issue with angular throwing an exception saying
      // the items have been changed after it had already been checked.
      setTimeout(() => {
        this.items = [];

        $(this.element).find('[data-scroll]').each((index, item) => {
          this.items.push({
            fragment: $(item).data('scroll'),
            label: item.innerText.trim()
          });
        });
      }, 0);

      const items = $(this.element).find('[data-scroll]');

      $(window).scroll(() => {
        const fromTop = $(window).scrollTop();

        if (fromTop + $(window).height() === $(document).height()) {
          this.activate(this.items[this.items.length - 1]);
        } else {
          const scrolledPast = items.map(function() {
            if (($(this).offset().top - 15) < fromTop) {
              return this;
            }
          });

          if (scrolledPast.length) {
            const lastItem = scrolledPast[scrolledPast.length - 1];
            const fragment = $(lastItem).data('scroll');

            const matches = this.items.filter(item => item.fragment === fragment);

            if (matches.length) {
              this.activate(matches[0]);
            }
          } else {
            this.activate(this.items[0]);
          }
        }
      });
    }
  }

  navigate(event: any, item: MenuItem): void {
    event.preventDefault();

    this.router.navigate([], {fragment: item.fragment});
  }

  protected resetActive(): void {
    this.items?.forEach(item => {
      item.active = false;
    });
  }

  protected activate(item: MenuItem | undefined): void {
    if (!!item) {
      this.resetActive();

      item.active = true;
    }
  }

}
