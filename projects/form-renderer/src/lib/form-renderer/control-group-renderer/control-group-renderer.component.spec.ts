import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { ControlGroupRendererComponent } from './control-group-renderer.component';
import { FormFactoryService } from '../../form-factory.service';
import { UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import * as testForms from '../../../test-forms';
import { By } from '@angular/platform-browser';
import { formWithConditions } from '../../../test-forms';
import { ConditionValidator } from '../../condition-validator';
import { QuestionRendererComponent } from '../question-renderer/question-renderer.component';
import { QuestionControlComponent } from '../question-renderer/question-control/question-control.component';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';

describe('ControlGroupRendererComponent', () => {
  let component: ControlGroupRendererComponent;
  let fixture: ComponentFixture<ControlGroupRendererComponent>;
  let formFactory: FormFactoryService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ControlGroupRendererComponent,
        QuestionRendererComponent,
        QuestionControlComponent,
      ],
      imports: [ReactiveFormsModule, FormComponentsModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    formFactory = TestBed.inject(FormFactoryService);
    fixture = TestBed.createComponent(ControlGroupRendererComponent);
    component = fixture.componentInstance;
  });

  it('should render a basic form group', () => {
    const form = formFactory.make(testForms.basicForm);

    component.form = form;
    component.renderedGroup = testForms.basicForm.groups[0];
    component.group = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    // Check the heading is displaying correctly
    const heading = fixture.debugElement.query(By.css('h3'));
    expect(heading.nativeElement.textContent.trim()).toBe('Test Group');

    // Check the questions are displaying
    const questions = fixture.debugElement.queryAll(By.css('bail-question-renderer'));
    expect(questions.length).toBe(1);
  });

  it('should render a group if its conditions pass', fakeAsync(() => {
    const form = formFactory.make(testForms.formWithConditions);

    component.form = form;
    component.renderedGroup = testForms.formWithConditions.groups[1];
    component.group = form.formGroup.get('groups') as UntypedFormGroup;
    component.validator = new ConditionValidator(testForms.formWithConditions.groups, component.group);

    fixture.detectChanges();

    // Check the control group is not displaying
    expect(fixture.debugElement.query(By.css('h3'))).toBeNull();
    expect(fixture.debugElement.query(By.css('bail-question-renderer'))).toBeNull();

    form.formGroup.get('groups.G1.answers.G1-Q2.answer').setValue('Foo');

    tick(200);

    fixture.detectChanges();

    // Check the heading is displaying correctly
    const heading = fixture.debugElement.query(By.css('h3'));
    expect(heading.nativeElement.textContent.trim()).toBe('Group With Conditions');

    // Check the questions are displaying
    const questions = fixture.debugElement.queryAll(By.css('bail-question-renderer'));
    expect(questions.length).toBe(1);
  }));

  it('should not render a group if its conditions fail', fakeAsync(() => {
    const form = formFactory.make(testForms.formWithConditions);

    component.form = form;
    component.renderedGroup = testForms.formWithConditions.groups[1];
    component.group = form.formGroup.get('groups') as UntypedFormGroup;
    component.validator = new ConditionValidator(testForms.formWithConditions.groups, component.group);

    fixture.detectChanges();

    // Check the control group is not displaying
    expect(fixture.debugElement.query(By.css('h3'))).toBeNull();
    expect(fixture.debugElement.query(By.css('bail-question-renderer'))).toBeNull();

    form.formGroup.get('groups.G1.answers.G1-Q2.answer').setValue('Bar');

    tick(200);

    fixture.detectChanges();

    // Check the control group is still not displaying
    expect(fixture.debugElement.query(By.css('h3'))).toBeNull();
    expect(fixture.debugElement.query(By.css('bail-question-renderer'))).toBeNull();
  }));
});
