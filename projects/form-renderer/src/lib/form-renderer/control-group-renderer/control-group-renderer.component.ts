import { Component, Input, OnInit } from '@angular/core';
import { RenderedGroup } from '../../models/rendered-group.model';
import { UntypedFormArray, UntypedFormGroup } from '@angular/forms';
import { ConditionValidator } from '../../condition-validator';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Form } from '../../form';

@Component({
  selector: 'bail-control-group-renderer',
  templateUrl: './control-group-renderer.component.html',
  styleUrls: ['./control-group-renderer.component.css']
})
export class ControlGroupRendererComponent implements OnInit {

  @Input() form: Form;
  @Input() renderedGroup: RenderedGroup;
  @Input() group: UntypedFormGroup;
  @Input() validator: ConditionValidator;
  @Input() index: number | undefined;
  @Input() hideHeading = false;
  render$: Observable<boolean> | undefined = of(true);

  get formGroup(): UntypedFormGroup | undefined {
    const group = this.group?.get(this.renderedGroup?.id + '.answers');

    if (group instanceof UntypedFormArray) {
      return group.at(this.index) as UntypedFormGroup;
    }

    return group as UntypedFormGroup;
  }

  constructor() { }

  ngOnInit(): void {
    if (!!this.validator) {
      this.render$ = this.validator.shouldRenderGroup(
        this.renderedGroup
      )?.pipe(
        tap(shouldRender => {
          if (!!this.group) {
            if (shouldRender && !this.group.get(this.renderedGroup.id)) {
              this.form.addGroup(this.group, this.renderedGroup);
            } else if (!shouldRender && !!this.group.get(this.renderedGroup.id)) {
              this.form.removeGroup(this.group, this.renderedGroup);
            }
          }
        })
      );
    }
  }

}
