import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { ConditionValidator } from '../../../condition-validator';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MultipleFormGroupDirective } from '../../multiple-form-group.directive';

@Component({
  selector: 'bail-nested-form-group-renderer',
  templateUrl: './nested-form-group-renderer.component.html',
  styleUrls: ['./nested-form-group-renderer.component.css']
})
export class NestedFormGroupRendererComponent extends MultipleFormGroupDirective implements OnInit {
  @Input() validator: ConditionValidator;
  @Input() nestedFormGroupRenderer: TemplateRef<any>;
  render$: Observable<boolean> | undefined = of(true);

  ngOnInit(): void {
    if (!!this.validator) {
      this.render$ = this.validator.shouldRenderGroup(
        this.group
      )?.pipe(
        tap(shouldRender => {
          if (!!this.group) {
            if (shouldRender && !this.groupsControl.get(this.group.id)) {
              this.form.addGroup(this.groupsControl, this.group);
            } else if (!shouldRender && !!this.groupsControl.get(this.group.id)) {
              this.form.removeGroup(this.groupsControl, this.group);
            }
          }
        })
      );
    }
  }
}
