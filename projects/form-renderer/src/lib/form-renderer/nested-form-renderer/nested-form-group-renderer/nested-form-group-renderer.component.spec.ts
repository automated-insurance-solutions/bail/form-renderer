import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFormGroupRendererComponent } from './nested-form-group-renderer.component';
import * as testForms from '../../../../test-forms';
import { FormFactoryService } from '../../../form-factory.service';
import { Form } from '../../../form';
import { UntypedFormGroup } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('NestedFormGroupRendererComponent', () => {
  let component: NestedFormGroupRendererComponent;
  let fixture: ComponentFixture<NestedFormGroupRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NestedFormGroupRendererComponent ]
    })
    .compileComponents();

    formFactory = TestBed.inject(FormFactoryService);
    fixture = TestBed.createComponent(NestedFormGroupRendererComponent);
    component = fixture.componentInstance;
  });

  it('should render a nested form', () => {
    form = formFactory.make(testForms.formWithNestedGroup);

    component.form = form;
    component.group = form.renderedForm.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    const renderer = fixture.debugElement.query(By.css('.nested-form-group'));

    expect(renderer).toBeTruthy();
  });

  it('should render a multiple nested form group', () => {
    form = formFactory.make(testForms.formWithMultipleNestedGroups);

    component.form = form;
    component.group = {
      ...form.renderedForm.groups[0],
      multiple_label: 'Witness'
    };
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    let renderer = fixture.debugElement.queryAll(By.css('.nested-form-group'));

    expect(renderer.length).toEqual(0);

    component.addGroup();
    component.addGroup();

    fixture.detectChanges();

    renderer = fixture.debugElement.queryAll(By.css('.nested-form-group'));

    expect(renderer.length).toEqual(2);

    const headings = fixture.debugElement.queryAll(By.css('h3'));

    expect(headings[0].nativeElement.textContent.trim()).toEqual('Witness 1');
    expect(headings[1].nativeElement.textContent.trim()).toEqual('Witness 2');
  });

  it('should remove a multiple nested form group', () => {
    form = formFactory.make(testForms.formWithMultipleNestedGroups);

    component.form = form;
    component.group = {
      ...form.renderedForm.groups[0],
      multiple_label: 'Witness'
    };
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    let renderer = fixture.debugElement.queryAll(By.css('.nested-form-group'));

    expect(renderer.length).toEqual(0);

    component.addGroup();
    component.addGroup();

    fixture.detectChanges();

    renderer = fixture.debugElement.queryAll(By.css('.nested-form-group'));
    expect(renderer.length).toEqual(2);

    // Get the button
    const removeButton = fixture.debugElement.query(By.css('button.remove-group'));

    // Trigger the confirm event
    removeButton.triggerEventHandler('confirm');

    fixture.detectChanges();

    renderer = fixture.debugElement.queryAll(By.css('.nested-form-group'));
    expect(renderer.length).toEqual(1);
  });
});
