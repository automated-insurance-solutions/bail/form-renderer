import { Component, OnInit } from '@angular/core';
import { BaseNestedFormRendererDirective } from './base-nested-form-renderer.directive';

@Component({
  selector: 'bail-nested-form-renderer',
  templateUrl: './nested-form-renderer.component.html',
  styleUrls: ['./nested-form-renderer.component.css']
})
export class NestedFormRendererComponent extends BaseNestedFormRendererDirective {

}
