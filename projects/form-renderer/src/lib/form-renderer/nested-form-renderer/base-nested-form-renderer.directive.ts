import { Directive, Input } from '@angular/core';
import { MultipleFormGroupDirective } from '../multiple-form-group.directive';
import { ConditionValidator } from '../../condition-validator';


@Directive()
export class BaseNestedFormRendererDirective extends MultipleFormGroupDirective {
  @Input() validator: ConditionValidator;
}
