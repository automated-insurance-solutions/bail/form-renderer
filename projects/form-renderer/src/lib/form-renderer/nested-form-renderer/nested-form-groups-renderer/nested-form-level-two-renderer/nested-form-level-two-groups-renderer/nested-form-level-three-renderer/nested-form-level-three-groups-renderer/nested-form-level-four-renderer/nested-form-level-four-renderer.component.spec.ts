import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFormLevelFourRendererComponent } from './nested-form-level-four-renderer.component';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'bail-nested-form-group-renderer',
  template: ''
})
class MockNestedFormGroupRendererComponent {
  @Input() form: any;
  @Input() group: any;
  @Input() groupsControl: any;
  @Input() validator: any;
  @Input() nestedFormGroupRenderer: any;
}

describe('NestedFormLevelFourRendererComponent', () => {
  let component: NestedFormLevelFourRendererComponent;
  let fixture: ComponentFixture<NestedFormLevelFourRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        NestedFormLevelFourRendererComponent,
        MockNestedFormGroupRendererComponent
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NestedFormLevelFourRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
