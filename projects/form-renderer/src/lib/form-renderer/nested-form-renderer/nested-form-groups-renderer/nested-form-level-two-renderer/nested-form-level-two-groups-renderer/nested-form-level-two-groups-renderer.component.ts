import { Component } from '@angular/core';
import { FormGroupsRendererDirective } from '../../../../form-groups-renderer/form-groups-renderer.directive';

@Component({
  selector: 'bail-nested-form-level-two-groups-renderer',
  templateUrl: './nested-form-level-two-groups-renderer.component.html',
  styleUrls: ['./nested-form-level-two-groups-renderer.component.css']
})
export class NestedFormLevelTwoGroupsRendererComponent extends FormGroupsRendererDirective {
}
