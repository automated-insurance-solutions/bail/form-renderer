import { Component } from '@angular/core';
import { BaseNestedFormRendererDirective } from '../../base-nested-form-renderer.directive';

@Component({
  selector: 'bail-nested-form-level-two-renderer',
  templateUrl: './nested-form-level-two-renderer.component.html',
  styleUrls: ['./nested-form-level-two-renderer.component.css']
})
export class NestedFormLevelTwoRendererComponent extends BaseNestedFormRendererDirective {

}
