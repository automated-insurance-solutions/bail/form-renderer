import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFormLevelThreeGroupsRendererComponent } from './nested-form-level-three-groups-renderer.component';
import { FormFactoryService } from '../../../../../../../form-factory.service';
import { Form } from '../../../../../../../form';
import { FormBuilder, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import * as testForms from '../../../../../../../../test-forms';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'bail-form-group-renderer',
  template: ''
})
class MockFormGroupRendererComponent {
  @Input() form: any;
  @Input() group: any;
  @Input() groupsControl: any;
  @Input() validator: any;
  @Input() nestedFormRenderer: any;
}

describe('NestedFormLevelThreeGroupsRendererComponent', () => {
  let component: NestedFormLevelThreeGroupsRendererComponent;
  let fixture: ComponentFixture<NestedFormLevelThreeGroupsRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;
  let fb: FormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        NestedFormLevelThreeGroupsRendererComponent,
        MockFormGroupRendererComponent
      ]
    })
    .compileComponents();

    formFactory = TestBed.inject(FormFactoryService);
    fb = TestBed.inject(UntypedFormBuilder);
    fixture = TestBed.createComponent(NestedFormLevelThreeGroupsRendererComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    form = formFactory.make(testForms.formWithNestedGroup);

    component.form = form;
    component.groups = form.renderedForm.groups[0].groups;
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    expect(component).toBeTruthy();
  });
});
