import { Component } from '@angular/core';
import { FormGroupsRendererDirective } from '../../../../../../../../form-groups-renderer/form-groups-renderer.directive';

@Component({
  selector: 'bail-nested-form-level-four-groups-renderer',
  templateUrl: './nested-form-level-four-groups-renderer.component.html',
  styleUrls: ['./nested-form-level-four-groups-renderer.component.css']
})
export class NestedFormLevelFourGroupsRendererComponent extends FormGroupsRendererDirective {

}
