import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFormLevelTwoRendererComponent } from './nested-form-level-two-renderer.component';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'bail-nested-form-group-renderer',
  template: ''
})
class MockNestedFormGroupRendererComponent {
  @Input() form: any;
  @Input() group: any;
  @Input() groupsControl: any;
  @Input() validator: any;
  @Input() nestedFormGroupRenderer: any;
}

describe('NestedFormLevelTwoRendererComponent', () => {
  let component: NestedFormLevelTwoRendererComponent;
  let fixture: ComponentFixture<NestedFormLevelTwoRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NestedFormLevelTwoRendererComponent, MockNestedFormGroupRendererComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NestedFormLevelTwoRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
