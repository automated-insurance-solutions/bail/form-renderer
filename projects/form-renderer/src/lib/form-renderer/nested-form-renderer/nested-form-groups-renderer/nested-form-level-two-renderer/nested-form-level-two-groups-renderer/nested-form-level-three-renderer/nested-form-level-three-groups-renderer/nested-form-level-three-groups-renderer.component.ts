import { Component } from '@angular/core';
import { FormGroupsRendererDirective } from '../../../../../../form-groups-renderer/form-groups-renderer.directive';

@Component({
  selector: 'bail-nested-form-level-three-groups-renderer',
  templateUrl: './nested-form-level-three-groups-renderer.component.html',
  styleUrls: ['./nested-form-level-three-groups-renderer.component.css']
})
export class NestedFormLevelThreeGroupsRendererComponent extends FormGroupsRendererDirective {

}
