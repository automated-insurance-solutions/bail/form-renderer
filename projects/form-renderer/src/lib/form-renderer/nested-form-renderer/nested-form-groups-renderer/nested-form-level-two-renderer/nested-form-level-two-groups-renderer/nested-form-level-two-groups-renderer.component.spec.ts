import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFormLevelTwoGroupsRendererComponent } from './nested-form-level-two-groups-renderer.component';
import * as testForms from '../../../../../../test-forms';
import { FormBuilder, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { FormFactoryService } from '../../../../../form-factory.service';
import { Form } from '../../../../../form';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'bail-form-group-renderer',
  template: ''
})
class MockFormGroupRendererComponent {
  @Input() form: any;
  @Input() group: any;
  @Input() groupsControl: any;
  @Input() validator: any;
  @Input() nestedFormRenderer: any;
}

describe('NestedFormLevelTwoGroupsRendererComponent', () => {
  let component: NestedFormLevelTwoGroupsRendererComponent;
  let fixture: ComponentFixture<NestedFormLevelTwoGroupsRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;
  let fb: FormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NestedFormLevelTwoGroupsRendererComponent, MockFormGroupRendererComponent ]
    })
    .compileComponents();

    formFactory = TestBed.inject(FormFactoryService);
    fb = TestBed.inject(UntypedFormBuilder);
    fixture = TestBed.createComponent(NestedFormLevelTwoGroupsRendererComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    form = formFactory.make(testForms.formWithNestedGroup);

    component.form = form;
    component.groups = form.renderedForm.groups[0].groups;
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    expect(component).toBeTruthy();
  });
});
