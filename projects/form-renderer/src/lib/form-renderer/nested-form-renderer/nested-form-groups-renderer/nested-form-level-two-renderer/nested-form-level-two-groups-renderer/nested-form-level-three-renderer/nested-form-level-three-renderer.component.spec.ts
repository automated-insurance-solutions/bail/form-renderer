import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFormLevelThreeRendererComponent } from './nested-form-level-three-renderer.component';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'bail-nested-form-group-renderer',
  template: ''
})
class MockNestedFormGroupRendererComponent {
  @Input() form: any;
  @Input() group: any;
  @Input() groupsControl: any;
  @Input() validator: any;
  @Input() nestedFormGroupRenderer: any;
}

describe('NestedFormLevelThreeRendererComponent', () => {
  let component: NestedFormLevelThreeRendererComponent;
  let fixture: ComponentFixture<NestedFormLevelThreeRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NestedFormLevelThreeRendererComponent, MockNestedFormGroupRendererComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NestedFormLevelThreeRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
