import { Component } from '@angular/core';
import { BaseNestedFormRendererDirective } from '../../../../base-nested-form-renderer.directive';

@Component({
  selector: 'bail-nested-form-level-three-renderer',
  templateUrl: './nested-form-level-three-renderer.component.html',
  styleUrls: ['./nested-form-level-three-renderer.component.css']
})
export class NestedFormLevelThreeRendererComponent extends BaseNestedFormRendererDirective {

}
