import { Component } from '@angular/core';
import { BaseNestedFormRendererDirective } from '../../../../../../base-nested-form-renderer.directive';

@Component({
  selector: 'bail-nested-form-level-four-renderer',
  templateUrl: './nested-form-level-four-renderer.component.html',
  styleUrls: ['./nested-form-level-four-renderer.component.css']
})
export class NestedFormLevelFourRendererComponent extends BaseNestedFormRendererDirective {

}
