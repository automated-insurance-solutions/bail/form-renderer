import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFormLevelFourGroupsRendererComponent } from './nested-form-level-four-groups-renderer.component';
import { FormFactoryService } from '../../../../../../../../../form-factory.service';
import { Form } from '../../../../../../../../../form';
import { FormBuilder, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import * as testForms from '../../../../../../../../../../test-forms';
import { FormGroupRendererComponent } from '../../../../../../../../form-groups-renderer/form-group-renderer/form-group-renderer.component';
import { ControlGroupRendererComponent } from '../../../../../../../../control-group-renderer/control-group-renderer.component';
import { QuestionRendererComponent } from '../../../../../../../../question-renderer/question-renderer.component';
import { QuestionControlComponent } from '../../../../../../../../question-renderer/question-control/question-control.component';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';

describe('NestedFormLevelFourGroupsRendererComponent', () => {
  let component: NestedFormLevelFourGroupsRendererComponent;
  let fixture: ComponentFixture<NestedFormLevelFourGroupsRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;
  let fb: FormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        NestedFormLevelFourGroupsRendererComponent,
        FormGroupRendererComponent,
        ControlGroupRendererComponent,
        QuestionRendererComponent,
        QuestionControlComponent,
      ],
      imports: [
        FormComponentsModule,
      ]
    })
    .compileComponents();

    formFactory = TestBed.inject(FormFactoryService);
    fb = TestBed.inject(UntypedFormBuilder);
    fixture = TestBed.createComponent(NestedFormLevelFourGroupsRendererComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    form = formFactory.make(testForms.formWithNestedGroup);

    component.form = form;
    component.groups = form.renderedForm.groups[0].groups;
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    expect(component).toBeTruthy();
  });
});
