import { Component } from '@angular/core';
import { FormGroupsRendererDirective } from '../../form-groups-renderer/form-groups-renderer.directive';

@Component({
  selector: 'bail-nested-form-groups-renderer',
  templateUrl: './nested-form-groups-renderer.component.html',
  styleUrls: ['./nested-form-groups-renderer.component.css']
})
export class NestedFormGroupsRendererComponent extends FormGroupsRendererDirective {

}
