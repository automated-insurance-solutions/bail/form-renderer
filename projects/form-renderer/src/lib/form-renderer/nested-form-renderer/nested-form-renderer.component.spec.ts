import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedFormRendererComponent } from './nested-form-renderer.component';
import { FormFactoryService } from '../../form-factory.service';
import * as testForms from '../../../test-forms';
import { Form } from '../../form';
import { UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'bail-nested-form-group-renderer',
  template: ''
})
class MockFormGroupRendererComponent {
  @Input() form: any;
  @Input() group: any;
  @Input() groupsControl: any;
  @Input() validator: any;
  @Input() nestedFormGroupRenderer: any;
}

describe('NestedFormRendererComponent', () => {
  let component: NestedFormRendererComponent;
  let fixture: ComponentFixture<NestedFormRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NestedFormRendererComponent, MockFormGroupRendererComponent ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    formFactory = TestBed.inject(FormFactoryService);
    fixture = TestBed.createComponent(NestedFormRendererComponent);
    component = fixture.componentInstance;
  });

  it('should render the nested form group renderer', () => {
    form = formFactory.make(testForms.formWithNestedGroup);

    component.form = form;
    component.group = form.renderedForm.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    const renderer = fixture.debugElement.query(By.css('bail-nested-form-group-renderer'));

    expect(renderer).toBeTruthy();
    expect(renderer.context.form).toEqual(form);
    expect(renderer.context.group).toEqual(form.renderedForm.groups[0]);
    expect(renderer.context.groupsControl).toEqual(component.groupsControl);
  });
});
