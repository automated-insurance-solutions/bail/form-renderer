import { Directive, Input } from '@angular/core';
import { UntypedFormArray, UntypedFormGroup } from '@angular/forms';
import { Form } from '../form';
import { RenderedGroup } from '../models/rendered-group.model';

@Directive()
export class MultipleFormGroupDirective {
  @Input() form: Form;
  @Input() group: RenderedGroup;
  @Input() groupsControl: UntypedFormGroup;

  get arrayAnswersControl(): UntypedFormArray | undefined {
    if (!this.group?.multiple) {
      return undefined;
    }

    return this.groupsControl?.get(`${this.group.id}.answers`) as UntypedFormArray || undefined;
  }

  addGroup(): void {
    if (!this.arrayAnswersControl) {
      return;
    }

    if (this.group.maximum_groups && this.arrayAnswersControl.length >= this.group.maximum_groups) {
      return;
    }

    this.arrayAnswersControl.push(this.form.groupAnswersControl(this.group));
  }

  removeGroup(index: number): void {
    this.arrayAnswersControl?.removeAt(index);
  }
}
