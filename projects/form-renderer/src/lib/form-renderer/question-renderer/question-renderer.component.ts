import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { RenderedQuestion } from '../../models/rendered-question.model';
import { ConditionValidator } from '../../condition-validator';
import { tap } from 'rxjs/operators';
import { Form } from '../../form';

@Component({
  selector: 'bail-question-renderer',
  templateUrl: './question-renderer.component.html',
  styleUrls: ['./question-renderer.component.scss']
})
export class QuestionRendererComponent implements OnInit {

  @Input() question: RenderedQuestion | undefined;
  @Input() group: UntypedFormGroup | undefined;
  @Input() form: Form;
  @Input() validator: ConditionValidator;
  render$: Observable<boolean> = of(true);

  get answerControl(): AbstractControl | undefined {
    return this.group?.get(`${this.question.id}.answer`);
  }

  get childrenControl(): UntypedFormGroup | undefined {
    return this.group?.get(`${this.question.id}.children`) as UntypedFormGroup;
  }

  constructor() {
  }

  ngOnInit(): void {
    const validator: Observable<boolean> | undefined = this.validator?.shouldRenderQuestion(this.question);

    if (!!validator) {
      this.render$ = validator.pipe(
        tap(shouldRender => {
          if (!!this.group) {
            if (shouldRender && !this.group.get(this.question.id)) {
              this.form.addQuestion(this.group, this.question);
            } else if (!shouldRender && !!this.group.get(this.question.id)) {
              this.form.removeQuestion(this.group, this.question);
            }
          }
        })
      );
    }
  }

}
