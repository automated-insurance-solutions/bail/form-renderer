import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionRendererComponent } from './question-renderer.component';
import { FormFactoryService } from '../../form-factory.service';
import * as testForms from '../../../test-forms';
import { Form } from '../../form';
import { UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ChangeDetectorRef } from '@angular/core';
import { ConditionValidator } from '../../condition-validator';
import { ControlDataComponent } from '../../form-data/group-data/control-data/control-data.component';
import { QuestionControlComponent } from './question-control/question-control.component';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';
import { ChildQuestionRendererComponent } from './child-question-renderer/child-question-renderer.component';

describe('QuestionRendererComponent', () => {
  let component: QuestionRendererComponent;
  let fixture: ComponentFixture<QuestionRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        QuestionRendererComponent,
        ControlDataComponent,
        QuestionControlComponent,
        ChildQuestionRendererComponent,
      ],
      imports: [
        ReactiveFormsModule,
        FormComponentsModule,
      ],
      providers: [
        ChangeDetectorRef,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    formFactory = TestBed.inject(FormFactoryService);
    fixture = TestBed.createComponent(QuestionRendererComponent);
    component = fixture.componentInstance;
  });

  it('should display a question', () => {
    form = formFactory.make(testForms.basicForm);

    component.form = form;
    component.question = testForms.basicForm.groups[0].questions[0];
    component.group = form.formGroup.get('groups.groupId.answers') as UntypedFormGroup;

    fixture.detectChanges();

    // Check that the control is displaying
    const control = fixture.debugElement.query(By.css('bail-question-control'));
    expect(control).toBeTruthy();

    // Check no child controls are displaying
    const childControls = fixture.debugElement.queryAll(By.css('bail-child-question-renderer'));
    expect(childControls.length).toBe(0);

    // Check the question body is not displaying
    const questionBody = fixture.debugElement.query(By.css('p'));
    expect(questionBody).toBeNull();
  });

  it('should display a question with children', () => {
    form = formFactory.make(testForms.formWithChildren);

    component.form = form;
    component.question = testForms.formWithChildren.groups[0].questions[0];
    component.group = form.formGroup.get('groups.groupId.answers') as UntypedFormGroup;

    fixture.detectChanges();

    // Check that the control is displaying
    const control = fixture.debugElement.query(By.css('bail-question-control'));
    expect(control).toBeNull();

    // Check no child controls are displaying
    const childControls = fixture.debugElement.queryAll(By.css('bail-child-question-renderer'));
    expect(childControls.length).toBe(1);

    // Check the question body is displaying
    const questionBody = fixture.debugElement.query(By.css('p'));
    expect(questionBody.nativeElement.textContent.trim()).toBe('Test Question');
  });

  it('should only display the question when the conditions pass', (done: DoneFn) => {
    form = formFactory.make(testForms.formWithConditions);

    component.form = form;
    component.question = testForms.formWithConditions.groups[0].questions[0];
    component.group = form.formGroup.get('groups.G1.answers') as UntypedFormGroup;
    component.validator = new ConditionValidator(form.renderedForm.groups, form.formGroup.get('groups') as UntypedFormGroup);

    component.ngOnInit();

    fixture.detectChanges();

    // Check that the control is not displaying
    let control = fixture.debugElement.query(By.css('.question-renderer'));
    expect(control).toBeNull();

    // Set the form value
    component.form.formGroup.get('groups.G1.answers.G1-Q2.answer').setValue('Foo');

    // Set a time out as we need to wait for the condition validator to be applied
    // before checking if the question is being rendered.
    setTimeout(() => {
      fixture.detectChanges();

      // Check that the control is displaying
      control = fixture.debugElement.query(By.css('.question-renderer'));
      expect(control).toBeTruthy();
      done();
    }, 100);
  });
});
