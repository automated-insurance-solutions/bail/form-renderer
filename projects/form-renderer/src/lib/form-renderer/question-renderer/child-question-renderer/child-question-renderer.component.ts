import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RenderedQuestion } from '../../../models/rendered-question.model';
import { Form } from '../../../form';
import { ConditionValidator } from '../../../condition-validator';

@Component({
  selector: 'bail-child-question-renderer',
  templateUrl: './child-question-renderer.component.html',
  styleUrls: ['./child-question-renderer.component.scss']
})
export class ChildQuestionRendererComponent implements OnInit {

  @Input() form: Form | undefined;
  @Input() question: RenderedQuestion | undefined;
  @Input() child: RenderedQuestion | undefined;
  @Input() group: UntypedFormGroup | undefined;
  @Input() validator: ConditionValidator;
  render$: Observable<boolean> = of(false);

  get answerControl(): UntypedFormControl | undefined {
    return this.group?.get(`${this.child.id}.answer`) as UntypedFormControl;
  }

  constructor() { }

  ngOnInit(): void {
    const validator = this.validator?.shouldRenderChild(this.question, this.child);

    if (!!validator) {
      this.render$ = validator.pipe(
        tap(shouldRender => {
          if (!!this.group) {
            if (shouldRender && !this.group.get(this.question.id)) {
              this.form.addChild(this.group, this.question, this.child);
            } else if (!shouldRender && !!this.group.get(this.question.id)) {
              this.form.removeChild(this.group, this.question, this.child);
            }
          }
        })
      );
    }
  }

}
