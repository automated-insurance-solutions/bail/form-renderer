import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildQuestionRendererComponent } from './child-question-renderer.component';
import { FormFactoryService } from '../../../form-factory.service';
import { Form } from '../../../form';
import * as testForms from '../../../../test-forms';
import { UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ChangeDetectorRef } from '@angular/core';
import { ConditionValidator } from '../../../condition-validator';
import { QuestionControlComponent } from '../question-control/question-control.component';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';

describe('ChildQuestionRendererComponent', () => {
  let component: ChildQuestionRendererComponent;
  let fixture: ComponentFixture<ChildQuestionRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildQuestionRendererComponent, QuestionControlComponent ],
      imports: [
        ReactiveFormsModule,
        FormComponentsModule,
      ],
      providers: [
        ChangeDetectorRef,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    formFactory = TestBed.inject(FormFactoryService);
    fixture = TestBed.createComponent(ChildQuestionRendererComponent);
    component = fixture.componentInstance;
  });

  it('should render a child control', () => {
    form = formFactory.make(testForms.formWithChildren);

    component.form = form;
    component.question = testForms.formWithChildren.groups[0].questions[0];
    component.child = testForms.formWithChildren.groups[0].questions[0].children[0];
    component.group = form.formGroup.get('groups.groupId.answers.questionId.children') as UntypedFormGroup;
    component.validator = new ConditionValidator(form.renderedForm.groups, form.formGroup.get('groups') as UntypedFormGroup);

    fixture.detectChanges();

    const questionControl = fixture.debugElement.query(By.css('bail-question-control'));

    expect(questionControl).toBeTruthy();
  });

  it('should only render a child if the conditions pass', (done: DoneFn) => {
    form = formFactory.make(testForms.formWithChildrenWithConditions);

    component.form = form;
    component.question = testForms.formWithChildrenWithConditions.groups[0].questions[1];
    component.child = testForms.formWithChildrenWithConditions.groups[0].questions[1].children[0];
    component.validator = new ConditionValidator(form.renderedForm.groups, form.formGroup.get('groups') as UntypedFormGroup);

    // The question won't get added to the form if the conditions are not
    // passing. Here, we wait for the conditions to pass and then add
    // the question to the form and set the group on the component.
    component.validator.shouldRenderQuestion(
      testForms.formWithChildrenWithConditions.groups[0].questions[1]
    ).subscribe((render) => {
      if (render) {
        form.addQuestion(
          form.formGroup.get('groups.G1.answers') as UntypedFormGroup,
          testForms.formWithChildrenWithConditions.groups[0].questions[1]
        );

        component.group = form.formGroup.get('groups.G1.answers.G1-Q2.children') as UntypedFormGroup;
      }
    });

    fixture.detectChanges();

    let questionControl = fixture.debugElement.query(By.css('bail-question-control'));
    expect(questionControl).toBeNull();

    component.form.formGroup.get('groups.G1.answers.G1-Q1.answer').setValue('Foo');

    // We set a timeout here so that the form conditions can be evaluated
    setTimeout(() => {
      fixture.detectChanges();

      questionControl = fixture.debugElement.query(By.css('bail-question-control'));
      expect(questionControl).toBeTruthy();

      done();
    }, 300);
  });
});
