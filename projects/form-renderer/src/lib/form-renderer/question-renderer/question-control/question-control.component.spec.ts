import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionControlComponent } from './question-control.component';
import { UntypedFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { MapService, SafeHtmlPipe } from '@automated-insurance-solutions/bail-core-components';
import { SimpleChange } from '@angular/core';
import moment from 'moment';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';
import { NgMapsGoogleModule } from '@ng-maps/google';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

// Create a jquery mock
(window as any).$ = () => {
  return new Proxy({}, {
    get: function get(target, name): any {
      return function wrapper(): void {};
    },
  });
};

describe('QuestionControlComponent', () => {
  let component: QuestionControlComponent;
  let fixture: ComponentFixture<QuestionControlComponent>;
  let fb: UntypedFormBuilder;
  let mapService: {find: jasmine.Spy};
  let http: {get: jasmine.Spy};

  beforeEach(async () => {
    mapService = jasmine.createSpyObj('MapService', ['find']);
    http = jasmine.createSpyObj('HttpClient', ['get']);

    await TestBed.configureTestingModule({
      declarations: [ QuestionControlComponent, SafeHtmlPipe ],
      imports: [
        ReactiveFormsModule,
        FormComponentsModule.forRoot({
          vrnLookup: {
            url: 'http://localhost',
          }
        }),
        NgMapsGoogleModule.forRoot({}),
      ],
      providers: [
        {provide: MapService, useValue: mapService},
        {provide: HttpClient, useValue: http}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fb = TestBed.inject(UntypedFormBuilder);
    fixture = TestBed.createComponent(QuestionControlComponent);
    component = fixture.componentInstance;
  });

  it('should render a text control', () => {
    component.question = {
      id: 'questionId',
      type: 'text',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-input'));

    expect(control).toBeTruthy();
    expect(control.attributes.type).toEqual('text');
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should render a textarea control', () => {
    component.question = {
      id: 'questionId',
      type: 'textarea',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-text'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should render a plain textarea control', () => {
    component.question = {
      id: 'questionId',
      type: 'plainTextarea',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-textarea'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should render an email control', () => {
    component.question = {
      id: 'questionId',
      type: 'email',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-email'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should render a number control', () => {
    component.question = {
      id: 'questionId',
      type: 'number',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-number'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should render a phone control', () => {
    component.question = {
      id: 'questionId',
      type: 'phone',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-phone'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should render a datepicker control', () => {
    component.question = {
      id: 'questionId',
      type: 'date',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-datepicker'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should set the maxDate option for a datepicker', () => {
    component.question = {
      id: 'questionId',
      type: 'date',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
      maxDate: {
        value: 5,
        unit: 'days'
      }
    };

    component.control = fb.control('');

    component.ngOnChanges({
      control: new SimpleChange(undefined, component.control, true),
      question: new SimpleChange(undefined, component.question, true),
    });

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-datepicker'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.datepickerOptions.maxDate.format('YYYY-MM-DD')).toEqual(moment().add(5, 'days').format('YYYY-MM-DD'));
    expect(control.context.datepickerOptions.minDate).toBeUndefined();
  });

  it('should set the minDate option for a datepicker', () => {
    component.question = {
      id: 'questionId',
      type: 'date',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
      minDate: {
        value: 5,
        unit: 'days'
      }
    };

    component.control = fb.control('');

    component.ngOnChanges({
      control: new SimpleChange(undefined, component.control, true),
      question: new SimpleChange(undefined, component.question, true),
    });

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-datepicker'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.datepickerOptions.minDate.format('YYYY-MM-DD')).toEqual(moment().subtract(5, 'days').format('YYYY-MM-DD'));
    expect(control.context.datepickerOptions.maxDate).toBeUndefined();
  });

  it('should render a timepicker control', () => {
    component.question = {
      id: 'questionId',
      type: 'time',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-timepicker'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should render a select control', () => {
    component.question = {
      id: 'questionId',
      type: 'select',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      placeholder: 'Test placeholder',
      multiple: false,
      values: [],
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-select'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.placeholder).toEqual('Test placeholder');
    expect(control.context.multiple).toEqual(false);
    expect(control.context.data).toEqual([]);
  });

  it('should repopulate a multi-select control', () => {
    component.question = {
      id: 'questionId',
      type: 'select',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      placeholder: 'Test placeholder',
      multiple: true,
      values: [],
      validators: {
        required: false,
      },
    };

    component.control = fb.control([
      {key: 'foo', value: 'Foo'},
      {key: 'bar', value: 'Bar'},
    ]);

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-select'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.placeholder).toEqual('Test placeholder');
    expect(control.context.multiple).toEqual(true);
    expect(control.context.data).toEqual([]);
    expect(component.control.value).toEqual(['foo', 'bar']);
  });

  it('should render a selectDialog control', () => {
    component.question = {
      id: 'questionId',
      type: 'selectDialog',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      values: [],
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-select-dialog'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.data).toEqual([]);
  });

  it('should render a radio control', () => {
    component.question = {
      id: 'questionId',
      type: 'radio',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      values: [],
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-radio'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.data).toEqual([]);
  });

  it('should render a checkbox control', () => {
    component.question = {
      id: 'questionId',
      type: 'checkbox',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      multiple: false,
      values: [],
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-checkbox'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.multiple).toEqual(false);
    expect(control.context.data).toEqual([]);
  });

  it('should render a range control', () => {
    component.question = {
      id: 'questionId',
      type: 'range',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      from: 10,
      to: 50,
      step: 5,
      from_label: 'From',
      to_label: 'To',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-range'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.from).toBe(10);
    expect(control.context.to).toBe(50);
    expect(control.context.step).toBe(5);
    expect(control.context.fromLabel).toBe('From');
    expect(control.context.toLabel).toBe('To');
  });

  it('should render a switch control', () => {
    component.question = {
      id: 'questionId',
      type: 'switch',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-switch'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
  });

  it('should render a filepond control', () => {
    component.question = {
      id: 'questionId',
      type: 'upload',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      multiple: false,
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-upload'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.multiple).toEqual(false);
  });

  it('should render a location control', () => {
    component.question = {
      id: 'questionId',
      type: 'location',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      displayMap: true,
      displayStreetView: false,
      captureStreetView: false,
      validators: {
        required: false,
      },
    };

    component.control = fb.group({
      value: null,
      geocode: null,
    });

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-location'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.group).toBe(component.control);
  });

  it('should render a partyType control', () => {
    component.question = {
      id: 'questionId',
      type: 'partyType',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      values: [],
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-select'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.control).toBe(component.control);
    expect(control.context.data).toEqual([]);
  });

  it('should render an incidentType control', () => {
    component.question = {
      id: 'questionId',
      type: 'incidentType',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      placeholder: 'Test placeholder',
      values: [],
      validators: {
        required: false,
      },
    };

    component.control = fb.group({
      value: null,
    });

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-incident-type'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.group).toBe(component.control);
    expect(control.context.placeholder).toEqual('Test placeholder');
    expect(control.context.data).toEqual([]);
  });

  it('should render a vrnLookup control', () => {
    component.question = {
      id: 'questionId',
      type: 'vrnLookup',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.group({
      value: null,
      vehicle: null,
    });

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-vrn-lookup'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.group).toBe(component.control);
  });

  it('should render a vehicleSelector control', () => {
    http.get.and.returnValue(of([]));

    component.question = {
      id: 'questionId',
      type: 'vehicleSelector',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.group({
      value: null,
    });

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-vehicle-selector'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.group).toBe(component.control);
  });

  it('should render a vehicleType control', () => {
    component.question = {
      id: 'questionId',
      type: 'vehicleType',
      name: 'test_question',
      body: 'Test Question',
      values: [
        {
          key: 'carId',
          value: 'Car'
        },
        {
          key: 'otherId',
          value: 'Other'
        },
      ],
      hint: 'Test hint',
      validators: {
        required: false,
      },
    };

    component.control = fb.group({
      value: null,
    });

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-vehicle-type'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.group).toBe(component.control);
    expect(control.context.data).toBe(component.question.values);
  });

  it('should render a damageSelector control', () => {
    component.question = {
      id: 'questionId',
      type: 'damageSelector',
      name: 'test_question',
      body: 'Test Question',
      hint: 'Test hint',
      image: 'image',
      validators: {
        required: false,
      },
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('form-damage-selector'));

    expect(control).toBeTruthy();
    expect(control.context.hint).toEqual('Test hint');
    expect(control.context.label).toEqual('Test Question');
    expect(control.context.image).toEqual('image');
    expect(control.context.control).toBe(component.control);
  });

  it('should render a helperText control', () => {
    component.question = {
      id: 'questionId',
      type: 'helperText',
      body: 'This is some test helper text',
    };

    component.control = fb.control('');

    fixture.detectChanges();

    const control = fixture.debugElement.query(By.css('.control-helper-text'));

    expect(control).toBeTruthy();
    expect(control.nativeElement.textContent.trim()).toEqual('This is some test helper text');
  });
});
