import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { RenderedQuestion } from '../../../models/rendered-question.model';
import { DatepickerOptions } from '@automated-insurance-solutions/bail-form-components';
import moment from 'moment';

@Component({
  selector: 'bail-question-control',
  templateUrl: './question-control.component.html',
  styleUrls: ['./question-control.component.scss']
})
export class QuestionControlComponent implements OnInit, OnChanges {

  @Input() question: RenderedQuestion | undefined;
  @Input() control: AbstractControl | undefined;
  datepickerOptions: DatepickerOptions | undefined;

  constructor() { }

  ngOnInit(): void {
    if (this.question.type === 'select' && this.question.multiple) {
      const controlValue = this.control.value?.map(value => value.key);

      this.control.patchValue(controlValue);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.setDatepickerOptions();
  }

  protected setDatepickerOptions(): void {
    if (this.question.type !== 'date') {
      this.datepickerOptions = undefined;

      return;
    }

    const options: DatepickerOptions = {};

    if (!!this.question.maxDate) {
      options.maxDate = this.getDate(this.question.maxDate.value, this.question.maxDate.unit);
    }

    if (!!this.question.minDate) {
      options.minDate = this.getDate(this.question.minDate.value, this.question.minDate.unit, 'subtract');
    }

    this.datepickerOptions = options;
  }

  protected getDate(value?: number, unit?: any, operation: 'add' | 'subtract' = 'add'): moment.Moment {
    if (!value || !unit) {
      return moment();
    }

    if (operation === 'subtract') {
      return moment().subtract(value, unit);
    }

    return moment().add(value, unit);
  }

}
