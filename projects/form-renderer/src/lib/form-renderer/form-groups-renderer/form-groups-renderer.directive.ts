import { Directive, Input } from '@angular/core';
import { Form } from '../../form';
import { RenderedGroup } from '../../models/rendered-group.model';
import { UntypedFormGroup } from '@angular/forms';
import { ConditionValidator } from '../../condition-validator';

@Directive()
export class FormGroupsRendererDirective {
  @Input() form: Form | undefined;
  @Input() groups: RenderedGroup[] = [];
  @Input() groupsControl: UntypedFormGroup | undefined;
  validator: ConditionValidator | undefined;

  constructor() { }

  ngOnInit(): void {
    this.validator = new ConditionValidator(this.groups, this.groupsControl);
  }
}
