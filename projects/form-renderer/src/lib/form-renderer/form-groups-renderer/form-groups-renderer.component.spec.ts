import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGroupsRendererComponent } from './form-groups-renderer.component';
import { UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import { FormFactoryService } from '../../form-factory.service';
import { Form } from '../../form';
import * as testForms from '../../../test-forms';
import { By } from '@angular/platform-browser';
import { ConditionValidator } from '../../condition-validator';
import { FormGroupRendererComponent } from './form-group-renderer/form-group-renderer.component';
import { ControlGroupRendererComponent } from '../control-group-renderer/control-group-renderer.component';
import { QuestionRendererComponent } from '../question-renderer/question-renderer.component';
import { QuestionControlComponent } from '../question-renderer/question-control/question-control.component';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';

describe('FormGroupsRendererComponent', () => {
  let component: FormGroupsRendererComponent;
  let fixture: ComponentFixture<FormGroupsRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        FormGroupsRendererComponent,
        FormGroupRendererComponent,
        ControlGroupRendererComponent,
        QuestionRendererComponent,
        QuestionControlComponent,
      ],
      imports: [ ReactiveFormsModule, FormComponentsModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    formFactory = TestBed.inject(FormFactoryService);
    fixture = TestBed.createComponent(FormGroupsRendererComponent);
    component = fixture.componentInstance;
  });

  it('should render the form groups', () => {
    form = formFactory.make(testForms.basicForm);

    component.form = form;
    component.groups = form.renderedForm.groups;
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    // Check the validator is created
    expect(component.validator).toBeInstanceOf(ConditionValidator);

    // Make sure the form groups are being rendered
    expect(fixture.debugElement.queryAll(By.css('bail-form-group-renderer')).length).toEqual(1);

    const groupRenderer = fixture.debugElement.query(By.css('bail-form-group-renderer'));

    // Check the form group renderer properties are set correctly
    expect(groupRenderer.context.form).toEqual(form);
    expect(groupRenderer.context.group).toEqual(form.renderedForm.groups[0]);
    expect(groupRenderer.context.groupsControl).toEqual(component.groupsControl);
    expect(groupRenderer.context.validator).toEqual(component.validator);
  });
});
