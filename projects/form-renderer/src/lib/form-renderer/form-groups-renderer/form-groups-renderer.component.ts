import { Component, Input, OnInit } from '@angular/core';
import { RenderedGroup } from '../../models/rendered-group.model';
import { UntypedFormGroup } from '@angular/forms';
import { Form } from '../../form';
import { ConditionValidator } from '../../condition-validator';
import { FormGroupsRendererDirective } from './form-groups-renderer.directive';

@Component({
  selector: 'bail-form-groups-renderer',
  templateUrl: './form-groups-renderer.component.html',
  styleUrls: ['./form-groups-renderer.component.css']
})
export class FormGroupsRendererComponent extends FormGroupsRendererDirective implements OnInit {

}
