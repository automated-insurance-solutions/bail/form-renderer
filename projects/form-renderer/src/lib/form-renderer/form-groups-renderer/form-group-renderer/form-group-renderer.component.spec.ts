import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGroupRendererComponent } from './form-group-renderer.component';
import * as testForms from '../../../../test-forms';
import { UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FormFactoryService } from '../../../form-factory.service';
import { Form } from '../../../form';
import { RenderedForm } from '../../../models/rendered-form.model';
import { ControlGroupRendererComponent } from '../../control-group-renderer/control-group-renderer.component';
import { QuestionRendererComponent } from '../../question-renderer/question-renderer.component';
import { QuestionControlComponent } from '../../question-renderer/question-control/question-control.component';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';
import {Directive, Input} from '@angular/core';

// Mock directive so that the tests don't try and load the real SweetAlert JS package.
@Directive({
  selector: '[swal]'
})
class MockSwalConfirmDirective {
  @Input() swal: any;
}

describe('FormGroupRendererComponent', () => {
  let component: FormGroupRendererComponent;
  let fixture: ComponentFixture<FormGroupRendererComponent>;
  let formFactory: FormFactoryService;
  let form: Form;
  let fb: UntypedFormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        FormGroupRendererComponent,
        ControlGroupRendererComponent,
        QuestionRendererComponent,
        QuestionControlComponent,
        MockSwalConfirmDirective
      ],
      imports: [ ReactiveFormsModule, FormComponentsModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    formFactory = TestBed.inject(FormFactoryService);
    fb = TestBed.inject(UntypedFormBuilder);
    fixture = TestBed.createComponent(FormGroupRendererComponent);
    component = fixture.componentInstance;
  });

  it('should display a basic group', () => {
    form = formFactory.make(testForms.basicForm);

    component.form = form;
    component.group = form.renderedForm.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    // Make sure the form groups renderer is not displaying
    expect(fixture.debugElement.query(By.css('bail-form-groups-renderer'))).toBeNull();

    // Make sure the control group renderer is displaying
    expect(fixture.debugElement.queryAll(By.css('bail-control-group-renderer')).length).toEqual(1);

    const controlGroupRenderer = fixture.debugElement.query(By.css('bail-control-group-renderer'));

    // Make sure the control group renderer context are set correctly
    expect(controlGroupRenderer.context.renderedGroup).toEqual(testForms.basicForm.groups[0]);
    expect(controlGroupRenderer.context.group).toEqual(component.groupsControl);
    expect(controlGroupRenderer.context.validator).toEqual(component.validator);
    expect(controlGroupRenderer.context.form).toEqual(form);
  });

  it('should render a multiple group with questions', () => {
    form = formFactory.make(testForms.formWithMultipleGroup);

    component.form = form;
    component.group = form.renderedForm.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    // Make sure the form groups renderer is not displaying because there
    // aren't any groups added
    expect(fixture.debugElement.query(By.css('bail-form-groups-renderer'))).toBeNull();

    // Make sure the control group renderer is not displaying
    expect(fixture.debugElement.query(By.css('bail-control-group-renderer'))).toBeNull();

    // Add groups
    component.addGroup();
    component.addGroup();

    fixture.detectChanges();

    // Make sure the form groups renderer is not displaying because there
    // aren't any groups added
    expect(fixture.debugElement.query(By.css('bail-form-groups-renderer'))).toBeNull();

    // Make sure the control groups are displaying
    expect(fixture.debugElement.queryAll(By.css('bail-control-group-renderer')).length).toEqual(2);

    // Remove a group
    component.removeGroup(1);

    fixture.detectChanges();

    // Make sure the form groups renderer is not displaying because there
    // aren't any groups added
    expect(fixture.debugElement.query(By.css('bail-form-groups-renderer'))).toBeNull();

    // Make sure a control group was removed
    expect(fixture.debugElement.queryAll(By.css('bail-control-group-renderer')).length).toEqual(1);
  });

  it('should render a nested group', () => {
    form = formFactory.make(testForms.formWithNestedGroup);

    component.form = form;
    component.group = form.renderedForm.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    // Make sure the form groups renderer is not displaying
    expect(fixture.debugElement.query(By.css('.nested-form'))).toBeTruthy();

    // Make sure the control group renderer is displaying
    expect(fixture.debugElement.query(By.css('bail-control-group-renderer'))).toBeNull();
  });

  it('should render multiple nested form groups', () => {
    form = formFactory.make(testForms.formWithMultipleNestedGroups);

    component.form = form;
    component.group = form.renderedForm.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    fixture.detectChanges();

    // Make sure the form groups renderer is not displaying because there
    // aren't any groups added
    expect(fixture.debugElement.query(By.css('.nested-form'))).toBeTruthy();

    // Make sure the control group renderer is not displaying
    expect(fixture.debugElement.query(By.css('bail-control-group-renderer'))).toBeNull();

    // Add groups
    component.addGroup();
    component.addGroup();

    fixture.detectChanges();

    // Make sure the nested form groups are displaying
    expect(fixture.debugElement.queryAll(By.css('.nested-form')).length).toEqual(1);

    // Make sure the control groups are not displaying
    expect(fixture.debugElement.query(By.css('bail-control-group-renderer'))).toBeNull()

    // Remove a group
    component.removeGroup(1);

    fixture.detectChanges();

    // Make sure a nested form group was removed
    expect(fixture.debugElement.queryAll(By.css('.nested-form')).length).toEqual(1);

    // Make sure the control groups are not displaying
    expect(fixture.debugElement.query(By.css('bail-control-group-renderer'))).toBeNull()
  });

  it('should customise the labels for a multiple group', () => {
    const renderedForm = testForms.formWithMultipleGroup;

    renderedForm.groups[0] = {
      ...renderedForm.groups[0],
      multiple_label: 'Multiple Test Group',
      add_button_label: 'Add Test Group',
      remove_button_label: 'Remove Test Group',
    }

    form = formFactory.make(renderedForm);

    component.form = form;
    component.group = form.renderedForm.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    // Add groups
    component.addGroup();
    component.addGroup();

    fixture.detectChanges();

    const multipleHeadings = fixture.debugElement.queryAll(By.css('h3'));

    expect(multipleHeadings.length).toEqual(3);
    expect(multipleHeadings[0].nativeElement.innerText).toEqual('Test Group');
    expect(multipleHeadings[1].nativeElement.innerText).toEqual('Multiple Test Group 1');
    expect(multipleHeadings[2].nativeElement.innerText).toEqual('Multiple Test Group 2');

    const removeButtons = fixture.debugElement.queryAll(By.css('.remove-group'));

    expect(removeButtons.length).toEqual(2);
    expect(removeButtons[0].nativeElement.innerText).toEqual('Remove Test Group');
    expect(removeButtons[1].nativeElement.innerText).toEqual('Remove Test Group');

    const addButton = fixture.debugElement.queryAll(By.css('button.add-group'));

    expect(addButton.length).toEqual(1);
    expect(addButton[0].nativeElement.innerText).toEqual('Add Test Group');
  });

  it('should get the array answers control', () => {
    form = formFactory.make(testForms.formWithMultipleGroup);

    component.form = form;
    component.group = testForms.formWithMultipleGroup.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    const answersControl = component.arrayAnswersControl;

    expect(answersControl).toBeInstanceOf(UntypedFormArray);
    expect(answersControl).toBe(form.formGroup.get('groups.groupId.answers') as UntypedFormArray);
  });

  it('should return undefined while getting the answers control if the group is not multiple', () => {
    form = formFactory.make(testForms.basicForm);

    component.form = form;
    component.group = testForms.basicForm.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    expect(component.arrayAnswersControl).toBeUndefined();
  });

  it('should return undefined while getting the answers control if the group is not set', () => {
    expect(component.arrayAnswersControl).toBeUndefined();
  });

  it('should return undefined while getting the answers control if the form group is not set', () => {
    component.group = {
      id: 'groupId',
      name: 'test_group',
      label: 'Test Group',
      multiple: true,
      conditions: [],
    };

    expect(component.arrayAnswersControl).toBeUndefined();
  });

  it('should return undefined while getting the answers control if the group id isn\'t set', () => {
    component.group = {
      id: 'groupId',
      name: 'test_group',
      label: 'Test Group',
      multiple: true,
      conditions: [],
    };
    component.groupsControl = fb.group([]);

    expect(component.arrayAnswersControl).toBeUndefined();
  });

  it('should return undefined while getting the answers control if the answers control is not set', () => {
    component.group = {
      id: 'groupId',
      name: 'test_group',
      label: 'Test Group',
      multiple: true,
      conditions: [],
    };
    component.groupsControl = fb.group({
      groupId: fb.group({})
    });

    expect(component.arrayAnswersControl).toBeUndefined();
  });

  it('should add a group', () => {
    form = formFactory.make(testForms.formWithMultipleGroup);

    component.form = form;
    component.group = testForms.formWithMultipleGroup.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    component.addGroup();

    expect(component.arrayAnswersControl.length).toBe(1);
  });

  it('shouldn\'t add a group if the maximum amount of groups have been added', () => {
    const data: RenderedForm = {
      ...testForms.formWithMultipleGroup,
      groups: [
        {
          ...testForms.formWithMultipleGroup.groups[0],
          maximum_groups: 1,
        }
      ]
    };

    form = formFactory.make(data);

    component.form = form;
    component.group = data.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    component.addGroup();
    component.addGroup();

    expect(component.arrayAnswersControl.length).toBe(1);
  });

  it('should remove a group', () => {
    form = formFactory.make(testForms.formWithMultipleGroup);

    component.form = form;
    component.group = testForms.formWithMultipleGroup.groups[0];
    component.groupsControl = form.formGroup.get('groups') as UntypedFormGroup;

    component.addGroup();

    expect(component.arrayAnswersControl.length).toBe(1);

    fixture.detectChanges();

    // Get the button
    const removeButton = fixture.debugElement.query(By.css('button.remove-group'));

    // Trigger the confirm event
    removeButton.triggerEventHandler('confirm');

    expect(component.arrayAnswersControl.length).toBe(0);
  });
});
