import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Form } from '../../../form';
import { RenderedGroup } from '../../../models/rendered-group.model';
import { FormGroup } from '@angular/forms';
import { ConditionValidator } from '../../../condition-validator';
import { MultipleFormGroupDirective } from '../../multiple-form-group.directive';

@Component({
  selector: 'bail-form-group-renderer',
  templateUrl: './form-group-renderer.component.html',
  styleUrls: ['./form-group-renderer.component.css']
})
export class FormGroupRendererComponent extends MultipleFormGroupDirective implements OnInit {

  @Input() nestedFormRenderer: TemplateRef<any>;
  @Input() validator: ConditionValidator;

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

}
