import { TestBed } from '@angular/core/testing';
import { UntypedFormBuilder, UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import { ConditionValidator } from './condition-validator';
import { RenderedForm } from './models/rendered-form.model';
import * as testForms from '../test-forms';
import { FormConverterService } from './form-converter.service';

describe('ConditionValidator', () => {
  let fb: UntypedFormBuilder;
  let formConverter: FormConverterService;
  let formGroup: UntypedFormGroup;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
      ]
    });
    fb = TestBed.inject(UntypedFormBuilder);
    formConverter = TestBed.inject(FormConverterService);
  });

  it('should render a group without conditions', (done: DoneFn) => {
    formGroup = formConverter.toFormGroup(testForms.formWithConditions);

    const service = new ConditionValidator(testForms.formWithConditions.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(testForms.formWithConditions.groups[0]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should render a group if the conditions pass', (done: DoneFn) => {
    formGroup = formConverter.toFormGroup(testForms.formWithConditions);

    formGroup.get('groups.G1.answers.G1-Q2.answer').setValue('Foo');

    const service = new ConditionValidator(testForms.formWithConditions.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(testForms.formWithConditions.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should render a group if the conditions pass', (done: DoneFn) => {
    formGroup = formConverter.toFormGroup(testForms.formWithConditions);

    formGroup.get('groups.G1.answers.G1-Q2.answer').setValue('Foo');

    const service = new ConditionValidator(testForms.formWithConditions.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(testForms.formWithConditions.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should not render the group if the conditions fail', (done: DoneFn) => {
    formGroup = formConverter.toFormGroup(testForms.formWithConditions);

    const service = new ConditionValidator(testForms.formWithConditions.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(testForms.formWithConditions.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should render a question without conditions', (done: DoneFn) => {
    formGroup = formConverter.toFormGroup(testForms.formWithConditions);

    const service = new ConditionValidator(testForms.formWithConditions.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderQuestion(testForms.formWithConditions.groups[0].questions[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should render a question if the conditions pass', (done: DoneFn) => {
    formGroup = formConverter.toFormGroup(testForms.formWithConditions);

    formGroup.get('groups.G1.answers.G1-Q2.answer').setValue('Foo');

    const service = new ConditionValidator(testForms.formWithConditions.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderQuestion(testForms.formWithConditions.groups[0].questions[0]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should not render the question if the conditions fail', (done: DoneFn) => {
    formGroup = formConverter.toFormGroup(testForms.formWithConditions);

    const service = new ConditionValidator(testForms.formWithConditions.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderQuestion(testForms.formWithConditions.groups[0].questions[0]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should pass an equals condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'equals',
              value: 1,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(1);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should fail an equals condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'equals',
              value: 1,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(2);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should pass a does not equal condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'does_not_equal',
              value: 1,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(2);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should fail a does not equal condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'does_not_equal',
              value: 1,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(1);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should pass a greater than condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'greater_than',
              value: 2,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(3);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should fail a greater than condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'greater_than',
              value: 2,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(1);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should pass a greater than or equal to condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'greater_than_or_equal_to',
              value: 2,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(2);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should fail a greater than or equal to condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'greater_than_or_equal_to',
              value: 2,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(1);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should pass a less than condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'less_than',
              value: 2,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(1);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should fail a less than condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'less_than',
              value: 2,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(2);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should pass a less than or equal to condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'less_than_or_equal_to',
              value: 2,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(2);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should fail a less than or equal to condition', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'less_than_or_equal_to',
              value: 2,
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue(3);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should pass a condition when the value is an array', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'select',
              name: 'test_question',
              body: 'Test Question',
              multiple: true,
              values: [
                {
                  key: 'foo',
                  value: 'Foo',
                },
                {
                  key: 'bar',
                  value: 'Bar',
                },
              ],
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'equals',
              value: 'foo',
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue([
      'foo'
    ]);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should pass a condition when the value has multiple answers', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'select',
              name: 'test_question',
              body: 'Test Question',
              multiple: true,
              values: [
                {
                  key: 'foo',
                  value: 'Foo',
                },
                {
                  key: 'bar',
                  value: 'Bar',
                },
              ],
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'equals',
              value: 'foo',
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue([
      'foo', 'bar'
    ]);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should fail a condition when the value is not in the multiple answers', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'select',
              name: 'test_question',
              body: 'Test Question',
              multiple: true,
              values: [
                {
                  key: 'foo',
                  value: 'Foo',
                },
                {
                  key: 'bar',
                  value: 'Bar',
                },
                {
                  key: 'qux',
                  value: 'Qux',
                },
              ],
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'equals',
              value: 'foo',
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue([
      'bar', 'qux'
    ]);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });

  it('should pass a does not equal condition when the value has multiple answers', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'select',
              name: 'test_question',
              body: 'Test Question',
              multiple: true,
              values: [
                {
                  key: 'foo',
                  value: 'Foo',
                },
                {
                  key: 'bar',
                  value: 'Bar',
                },
                {
                  key: 'qux',
                  value: 'Qux',
                },
              ],
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'does_not_equal',
              value: 'foo',
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue([
      'bar', 'qux'
    ]);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeTrue();

      done();
    }, done.fail);
  });

  it('should fail a does not equal condition when the value has multiple answers', (done: DoneFn) => {
    const form: RenderedForm = {
      id: 'formId',
      client_id: 'clientId',
      name: 'Foo',
      description: 'Test',
      groups: [
        {
          id: 'G1',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G1-Q1',
              type: 'select',
              name: 'test_question',
              body: 'Test Question',
              multiple: true,
              values: [
                {
                  key: 'foo',
                  value: 'Foo',
                },
                {
                  key: 'bar',
                  value: 'Bar',
                },
              ],
              validators: {
                required: false,
              }
            }
          ],
          conditions: [],
        },
        {
          id: 'G2',
          name: 'test_form',
          label: 'Test Form',
          multiple: false,
          questions: [
            {
              id: 'G2-Q2',
              type: 'number',
              name: 'test_question',
              body: 'Test Question',
              validators: {
                required: false,
              }
            }
          ],
          conditions: [
            {
              id: 'conditionId',
              comparator_id: 'G1-Q1',
              operator: 'does_not_equal',
              value: 'foo',
            }
          ],
        }
      ]
    };

    formGroup = formConverter.toFormGroup(form);

    formGroup.get('groups.G1.answers.G1-Q1.answer').setValue([
      'foo', 'bar'
    ]);

    const service = new ConditionValidator(form.groups, formGroup.get('groups') as UntypedFormGroup);

    service.shouldRenderGroup(form.groups[1]).subscribe(render => {
      expect(render).toBeFalse();

      done();
    }, done.fail);
  });
});
