import { UntypedFormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { Condition, ConditionConjunction } from './models/condition.model';
import { RenderedGroup } from './models/rendered-group.model';
import { RenderedQuestion } from './models/rendered-question.model';

class ParsedCondition {
  result: boolean;
  conjunction?: ConditionConjunction;
}

export class ConditionValidator {
  protected cachedGroupResults: {[key: string]: BehaviorSubject<boolean>} = {};
  protected cachedQuestionResults: {[key: string]: BehaviorSubject<boolean>} = {};
  protected cachedChildrenResults: {[key: string]: BehaviorSubject<boolean>} = {};
  protected previous: string;
  protected value: any;

  constructor(protected renderedGroups: RenderedGroup[], protected formGroup: UntypedFormGroup) {
    this.value = this.formGroup?.value || {};

    this.validateForm();

    // The form was constantly triggering even when there weren't changes, here
    // we debounce and check that the value has changed before running the
    // condition validator.
    this.formGroup.valueChanges.pipe(
      debounceTime(100),
      filter(value => JSON.stringify(value) !== JSON.stringify(this.previous))
    ).subscribe((value) => {
      this.value = value;

      this.validateForm();

      this.previous = value;
    });
  }

  /**
   * Check if the group should be rendered.
   */
  shouldRenderGroup(group: RenderedGroup): BehaviorSubject<boolean> | undefined {
    return this.cachedGroupResults[group.id];
  }

  /**
   * Check if the question should be rendered.
   */
  shouldRenderQuestion(question: RenderedQuestion): BehaviorSubject<boolean> | undefined {
    return this.cachedQuestionResults[question.id];
  }

  /**
   * Check if the child question should be rendered.
   */
  shouldRenderChild(question: RenderedQuestion, child: RenderedQuestion): BehaviorSubject<boolean> | undefined {
    const key = this.buildChildKey(question, child);

    return this.cachedChildrenResults[key];
  }

  /**
   * Validate the form conditions pass.
   */
  protected validateForm(): void {
    this.renderedGroups.forEach(group => this.validateGroupConditions(group));
  }

  /**
   * Validate that the group conditions pass.
   */
  protected validateGroupConditions(group: RenderedGroup): void {
    const valid: boolean = this.validateConditions(group, group.conditions);

    if (!this.cachedGroupResults.hasOwnProperty(group.id)) {
      this.cachedGroupResults[group.id] = new BehaviorSubject<boolean>(valid);
    } else {
      this.cachedGroupResults[group.id].next(valid);
    }

    if (group.questions && group.questions.length) {
      group.questions.forEach(question => this.validationQuestionConditions(group, question));
    }
  }

  /**
   * Validate that the question conditions pass.
   */
  protected validationQuestionConditions(group: RenderedGroup, question: RenderedQuestion): void {
    let valid: boolean = this.validateConditions(group, question.conditions, undefined);

    // If the question has child questions validate the child conditions
    if (!!question.children) {
      question.children.forEach(child => this.validateChildControl(group, question, child));

      // If the question shouldn't be displayed currently it might have a condition bound
      // to a child value. Here, we loop through the children and check if any of them
      // should be displayed, if they should then we need to display the child question
      // as well so we make it valid.
      if (!valid) {
        for (const child of question.children) {
          if (this.shouldRenderChild(question, child).getValue()) {
            valid = true;
            break;
          }
        }
      }
    }

    if (!this.cachedQuestionResults.hasOwnProperty(question.id)) {
      this.cachedQuestionResults[question.id] = new BehaviorSubject<boolean>(valid);
    } else {
      this.cachedQuestionResults[question.id].next(valid);
    }
  }

  /**
   * Validate that the child control conditions pass.
   */
  protected validateChildControl(group: RenderedGroup, question: RenderedQuestion, child: RenderedQuestion): void {
    const key = this.buildChildKey(question, child);

    const valid: boolean = this.validateConditions(group, question.conditions, child.id);

    if (!this.cachedChildrenResults.hasOwnProperty(key)) {
      this.cachedChildrenResults[key] = new BehaviorSubject<boolean>(valid);
    } else {
      this.cachedChildrenResults[key].next(valid);
    }
  }

  /**
   * Validate the conditions against the rendered group.
   */
  protected validateConditions(group: RenderedGroup, conditions: Condition[], childId?: string): boolean {
    // If there are no conditions to validate return true
    if (!conditions?.length) {
      return true;
    }

    const parsedConditions = conditions.map(condition => this.parseCondition(condition, childId));
    const groupedConditions = this.groupConditions(parsedConditions);

    for (const conditionGroup of groupedConditions) {
      // Each condition in a group will be an 'and' condition so we have to make
      // sure all of the conditions in a group pass.
      const passes = conditionGroup.reduce((accumulator, condition) => {
        if (! condition.result) {
          return condition.result;
        }

        return accumulator;
      }, true);

      // The grouped conditions are grouped by the 'or' conjunction, so if any
      // of the groups are valid we will return true
      if (passes) {
        return true;
      }
    }

    return false;
  }

  /**
   * Run the condition and get the type of conjunction.
   */
  protected parseCondition(condition: Condition, childId?: string): ParsedCondition {
    const answer = this.getComparatorAnswer(condition.comparator_id, childId);

    return {
      result: this.validateCondition(condition, answer),
      conjunction: condition.conjunction
    };
  }

  /**
   * Get the answer for the comparator from the form.
   */
  protected getComparatorAnswer(comparatorId: string, childId?: string): any {
    if (!this.value) {
      return null;
    }

    // Here, we split the comparator question ID into it's parts and find
    // all of the group ids.
    const ids = comparatorId.split('-');
    const groupIds = ids.filter(id => id.startsWith('G'));
    const groups = [];

    // Then, we stitch all of the group IDs back to their full path
    groupIds.forEach(groupId => {
      const parts = [];

      for (const id of ids) {
        parts.push(id);

        if (id === groupId) {
          groups.push(parts.join('-'));

          break;
        }
      }
    });

    // Next, we find the relevant form group by looping through the parsed
    // group IDs and attempting to find the relevant form group.
    let formGroup: UntypedFormGroup | null = null;

    groups.forEach(id => {
      const fg = formGroup || this.formGroup;
      const group = fg.get(`${id}.answers`);

      if (group) {
        formGroup = group as UntypedFormGroup;
      }
    });

    if (formGroup) {
      // If a child id is supplied and the form group has children then
      // we get the answer for the child
      if (!!childId && !!formGroup.get(`${comparatorId}.children`)) {
        const answer = formGroup.get(`${comparatorId}.children.${childId}.answer`);

        if (answer) {
          return answer.value;
        }
      } else {
        // Get the answer from the form group
        const answer = formGroup.get(`${comparatorId}.answer`);

        if (answer) {
          // Some questions are stored as a form group instead of a form control
          // because we save additional metadata, e.g. location component saves
          // the geocode data. Here, we access the value correctly for those
          // questions.
          if (answer instanceof UntypedFormGroup) {
            return answer.value?.value;
          } else {
            return answer.value;
          }
        }
      }
    }

    return undefined;
  }

  /**
   * Validate the condition against the answer.
   */
  protected validateCondition(condition: Condition, answer: any|any[]): boolean {
    switch (condition.operator) {
      case 'equals':
        if (Array.isArray(answer)) {
          return answer.includes(condition.value);
        }

        return condition.value == answer;
      case 'does_not_equal':
        if (Array.isArray(answer)) {
          return !answer.includes(condition.value);
        }

        return condition.value != answer;
      case 'greater_than':
        return answer > condition.value;
      case 'greater_than_or_equal_to':
        return answer >= condition.value;
      case 'less_than':
        return answer < condition.value;
      case 'less_than_or_equal_to':
        return answer <= condition.value;
      default:
        return false;
    }
  }

  /**
   * Group the conditions by their conjunction.
   */
  protected groupConditions(parsedConditions: ParsedCondition[]): Array<ParsedCondition[]> {
    const grouped: Array<ParsedCondition[]> = [];
    let currentGroup: ParsedCondition[] = [];

    parsedConditions.forEach(rule => {
      // If the rules does not have a conjunction it will usually be the first
      // condition and should be pushed into the current group
      if (! rule.conjunction) {
        currentGroup.push(rule);
      } else if (rule.conjunction === 'and') {
        // If the condition has the 'and' conjunction it should be pushed into the
        // current group
        currentGroup.push(rule);
      } else {
        // If the condition has an or conjunction we should start a new condition
        // group
        grouped.push(currentGroup);

        currentGroup = [rule];
      }
    });

    grouped.push(currentGroup);

    return grouped;
  }

  /**
   * Build a key for the child question.
   */
  protected buildChildKey(question: RenderedQuestion, child: RenderedQuestion): string {
    return `${question.id}-${child.id}`;
  }
}
