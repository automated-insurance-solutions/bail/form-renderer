import { NgModule } from '@angular/core';
import { FormRendererComponent } from './form-renderer.component';
import { CommonModule } from '@angular/common';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';
import { ReactiveFormsModule } from '@angular/forms';
import { QuestionRendererComponent } from './form-renderer/question-renderer/question-renderer.component';
import { QuestionControlComponent } from './form-renderer/question-renderer/question-control/question-control.component';
import { ChildQuestionRendererComponent } from './form-renderer/question-renderer/child-question-renderer/child-question-renderer.component';
import { FormRendererNavigationComponent } from './form-renderer-navigation/form-renderer-navigation.component';
import { FormDataComponent } from './form-data/form-data.component';
import { GroupDataComponent } from './form-data/group-data/group-data.component';
import { GroupValuesComponent } from './form-data/group-data/group-values/group-values.component';
import { ControlDataComponent } from './form-data/group-data/control-data/control-data.component';
import { DamageSelectorValueComponent } from './form-data/group-data/control-data/damage-selector-value/damage-selector-value.component';
import { FormFieldComponent } from './form-data/group-data/control-data/form-field/form-field.component';
import { FormGroupsRendererComponent } from './form-renderer/form-groups-renderer/form-groups-renderer.component';
import { ControlGroupRendererComponent } from './form-renderer/control-group-renderer/control-group-renderer.component';
import { FormGroupRendererComponent } from './form-renderer/form-groups-renderer/form-group-renderer/form-group-renderer.component';
import { NestedFormRendererComponent } from './form-renderer/nested-form-renderer/nested-form-renderer.component';
import { UploadImagePreviewComponent } from './form-data/group-data/control-data/upload-image-preview/upload-image-preview.component';
import { NestedFormGroupsRendererComponent } from './form-renderer/nested-form-renderer/nested-form-groups-renderer/nested-form-groups-renderer.component';
import { NestedFormLevelTwoRendererComponent } from './form-renderer/nested-form-renderer/nested-form-groups-renderer/nested-form-level-two-renderer/nested-form-level-two-renderer.component';
import { NestedFormLevelTwoGroupsRendererComponent } from './form-renderer/nested-form-renderer/nested-form-groups-renderer/nested-form-level-two-renderer/nested-form-level-two-groups-renderer/nested-form-level-two-groups-renderer.component';
import { NestedFormGroupRendererComponent } from './form-renderer/nested-form-renderer/nested-form-group-renderer/nested-form-group-renderer.component';
import { NestedFormLevelThreeRendererComponent } from './form-renderer/nested-form-renderer/nested-form-groups-renderer/nested-form-level-two-renderer/nested-form-level-two-groups-renderer/nested-form-level-three-renderer/nested-form-level-three-renderer.component';
import { NestedFormLevelThreeGroupsRendererComponent } from './form-renderer/nested-form-renderer/nested-form-groups-renderer/nested-form-level-two-renderer/nested-form-level-two-groups-renderer/nested-form-level-three-renderer/nested-form-level-three-groups-renderer/nested-form-level-three-groups-renderer.component';
import { NestedFormLevelFourRendererComponent } from './form-renderer/nested-form-renderer/nested-form-groups-renderer/nested-form-level-two-renderer/nested-form-level-two-groups-renderer/nested-form-level-three-renderer/nested-form-level-three-groups-renderer/nested-form-level-four-renderer/nested-form-level-four-renderer.component';
import { NestedFormLevelFourGroupsRendererComponent } from './form-renderer/nested-form-renderer/nested-form-groups-renderer/nested-form-level-two-renderer/nested-form-level-two-groups-renderer/nested-form-level-three-renderer/nested-form-level-three-groups-renderer/nested-form-level-four-renderer/nested-form-level-four-groups-renderer/nested-form-level-four-groups-renderer.component';
import { UserSubmittedDatePipe } from './user-submitted-date/user-submitted-date.pipe';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';

@NgModule({
  declarations: [
    FormRendererComponent,
    QuestionRendererComponent,
    QuestionControlComponent,
    ChildQuestionRendererComponent,
    FormRendererNavigationComponent,
    FormDataComponent,
    GroupDataComponent,
    GroupValuesComponent,
    ControlDataComponent,
    DamageSelectorValueComponent,
    FormFieldComponent,
    FormGroupsRendererComponent,
    ControlGroupRendererComponent,
    FormGroupRendererComponent,
    NestedFormRendererComponent,
    UploadImagePreviewComponent,
    NestedFormGroupsRendererComponent,
    NestedFormLevelTwoRendererComponent,
    NestedFormLevelTwoGroupsRendererComponent,
    NestedFormGroupRendererComponent,
    NestedFormLevelThreeRendererComponent,
    NestedFormLevelThreeGroupsRendererComponent,
    NestedFormLevelFourRendererComponent,
    NestedFormLevelFourGroupsRendererComponent,
    UserSubmittedDatePipe,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormComponentsModule,
    SweetAlert2Module,
  ],
  exports: [
    FormRendererComponent,
    FormRendererNavigationComponent,
    FormDataComponent,
    UserSubmittedDatePipe,
  ]
})
export class FormRendererModule {

}
