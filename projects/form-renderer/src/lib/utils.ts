export function copy<T>(value: T): T {
  return JSON.parse(JSON.stringify(value)) as T;
}

/**
 * Inspired by the except function in laravel. Generates a copy of the
 * target and removes all of the keys in the except array.
 */
export function except<T>(value: T, keys: string[] = []): T {
  value = copy(value);

  keys.forEach(key => delete value[key]);

  return value;
}

/**
 * Inspired by the data_get function in laravel. Allows you to provide
 * a target at a dot notated key and it will split the key and loop
 * access each property from the target. If a property is not set the
 * default value will be returned otherwise it will keep on looping
 * until it gets the final result.
 */
export function dataGet<T>(target, key: string, defaultValue = null): T {
  if (! target) {
    return defaultValue;
  }

  const parts = key.split('.');
  let value = target;

  for (const part of parts) {
    if (value[part]) {
      value = value[part];
    } else {
      return defaultValue;
    }
  }

  return value as T;
}
