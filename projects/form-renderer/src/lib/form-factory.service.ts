import { Injectable } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { Form } from './form';
import { RenderedForm } from './models/rendered-form.model';
import { FormConverterService } from './form-converter.service';

@Injectable({
  providedIn: 'root'
})
export class FormFactoryService {

  constructor(
    protected fb: UntypedFormBuilder,
    protected formConverter: FormConverterService
  ) { }

  make(form: RenderedForm): Form {
    const f = new Form(this.fb, this.formConverter);

    return f.init(form);
  }

}
