import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRendererComponent } from './form-renderer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormFactoryService } from './form-factory.service';
import * as testForms from '../test-forms';
import { ChangeDetectorRef, SimpleChange } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Form } from './form';
import { FormGroupsRendererComponent } from './form-renderer/form-groups-renderer/form-groups-renderer.component';
import { FormGroupRendererComponent } from './form-renderer/form-groups-renderer/form-group-renderer/form-group-renderer.component';
import { ControlGroupRendererComponent } from './form-renderer/control-group-renderer/control-group-renderer.component';
import { QuestionRendererComponent } from './form-renderer/question-renderer/question-renderer.component';
import { QuestionControlComponent } from './form-renderer/question-renderer/question-control/question-control.component';
import { FormComponentsModule } from '@automated-insurance-solutions/bail-form-components';

describe('FormRendererComponent', () => {
  let component: FormRendererComponent;
  let fixture: ComponentFixture<FormRendererComponent>;
  let formFactory: FormFactoryService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        FormRendererComponent,
        FormGroupsRendererComponent,
        FormGroupRendererComponent,
        ControlGroupRendererComponent,
        QuestionRendererComponent,
        QuestionControlComponent,
      ],
      imports: [
        ReactiveFormsModule,
        FormComponentsModule,
      ],
      providers: [
        ChangeDetectorRef,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    formFactory = TestBed.inject(FormFactoryService);
    fixture = TestBed.createComponent(FormRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should render a form', () => {
    const form = formFactory.make(testForms.basicForm);

    component.ngOnChanges({
      form: new SimpleChange(undefined, form, true)
    });

    fixture.detectChanges();

    expect(component.form).toBe(form);
    expect(component.renderedForm).toBe(testForms.basicForm);

    // Check the group renderer to be displaying
    const groupRenderer = fixture.debugElement.queryAll(By.css('bail-form-groups-renderer'));
    expect(groupRenderer.length).toBe(1);
  });

  it('should render a form from a RenderedForm', () => {
    component.ngOnChanges({
      form: new SimpleChange(undefined, testForms.basicForm, true)
    });

    fixture.detectChanges();

    expect(component.renderedForm).toBe(testForms.basicForm);
    expect(component.form).toBeInstanceOf(Form);

    // Check the group renderer to be displaying
    const groupRenderer = fixture.debugElement.queryAll(By.css('bail-form-groups-renderer'));
    expect(groupRenderer.length).toBe(1);
  });

  it('should render a form from a string', () => {
    component.ngOnChanges({
      form: new SimpleChange(undefined, btoa(JSON.stringify(testForms.basicForm)), true)
    });

    fixture.detectChanges();

    expect(component.renderedForm).toEqual(testForms.basicForm);
    expect(component.form).toBeInstanceOf(Form);

    // Check the group renderer to be displaying
    const groupRenderer = fixture.debugElement.queryAll(By.css('bail-form-groups-renderer'));
    expect(groupRenderer.length).toBe(1);
  });

  it('should check if the form is valid', () => {
    component.form = formFactory.make(testForms.formWithValidators);

    expect(component.valid()).toBeFalse();

    component.form.formGroup.get('groups.groupId.answers.questionId.answer').setValue('Foo');

    expect(component.valid()).toBeTrue();
  });

  it('should return false if the form is not set', () => {
    expect(component.valid()).toBeFalse();
  });

  it('should get the answers', () => {
    component.form = formFactory.make(testForms.basicForm);

    expect(component.answers()).toEqual([
      {
        group_id: 'groupId',
        child_id: null,
        answers: {
          questionId: {
            question_id: 'questionId',
            answer: null,
          }
        }
      }
    ]);
  });

  it('should return false if the form is not set', () => {
    expect(component.answers()).toBeUndefined();
  });
});
