import { TestBed } from '@angular/core/testing';
import { UntypedFormBuilder, UntypedFormGroup, ReactiveFormsModule } from '@angular/forms';
import { LocalStorageService } from '@automated-insurance-solutions/bail-core-components';
import { Form } from './form';
import * as testForms from '../test-forms';
import { ConditionValidator } from './condition-validator';
import { RenderedQuestion } from './models/rendered-question.model';
import { FormConverterService } from './form-converter.service';

describe('Form', () => {
  let localStorageSpy: { get: jasmine.Spy };
  let form: Form;

  beforeEach(() => {
    localStorageSpy = jasmine.createSpyObj('LocalStorageService', ['get']);

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
      ],
      providers: [
        {provide: LocalStorageService, useValue: localStorageSpy},
      ]
    });

    form = new Form(
      TestBed.inject(UntypedFormBuilder),
      TestBed.inject(FormConverterService),
    );
  });

  it('should initialise a form', () => {
    const f = form.init(testForms.basicForm);

    expect(f).toBe(form);
    expect(form.renderedForm).toEqual(testForms.basicForm);
    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            }
          }
        }
      }
    });
  });

  it('should initialise a form with conditions', () => {
    const f = form.init(testForms.formWithConditions);

    expect(f).toBe(form);
    expect(form.renderedForm).toEqual(testForms.formWithConditions);
    expect(form.formGroup.value).toEqual({
      groups: {
        G1: {
          group_id: 'G1',
          child_id: null,
          answers: {
            'G1-Q2': {
              question_id: 'G1-Q2',
              answer: null,
            }
          }
        }
      }
    });
  });

  it('should get the answers', () => {
    const answers = form.init(testForms.basicForm).answers();

    expect(answers).toEqual([
      {
        group_id: 'groupId',
        child_id: null,
        answers: {
          questionId: {
            question_id: 'questionId',
            answer: null,
          }
        }
      }
    ] as any);
  });

  it('should check if the form is valid', () => {
    form.init(testForms.formWithValidators);

    expect(form.valid()).toBeFalse();

    form.formGroup.get('groups').get('groupId').get('answers').get('questionId').patchValue({
      answer: 'Foo',
    });

    expect(form.valid()).toBeTrue();
  });

  it('should add a group', () => {
    form.init(testForms.basicForm);

    form.addGroup(form.formGroup.get('groups') as UntypedFormGroup, {
      id: 'newGroupId',
      label: 'New Group',
      name: 'new_group',
      multiple: false,
      maximum_groups: null,
      conditions: [],
      questions: [
        {
          id: 'newQuestionId',
          type: 'text',
          body: 'New Question',
          name: 'new_question',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
          conditions: [],
        }
      ]
    });

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            }
          }
        },
        newGroupId: {
          group_id: 'newGroupId',
          child_id: null,
          answers: {
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: null,
            }
          }
        },
      }
    });
  });

  it('should remove a group', () => {
    form.init(testForms.basicForm);

    form.removeGroup(form.formGroup.get('groups') as UntypedFormGroup, testForms.basicForm.groups[0]);

    expect(form.formGroup.value).toEqual({
      groups: {},
    });
  });

  it('should add a question', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'text',
      body: 'New Question',
      name: 'new_question',
      default: null,
      hint: null,
      validators: {
        required: false,
        maxLength: null,
        minLength: null
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: null,
            },
          }
        }
      }
    });
  });

  it('should add a location question', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'location',
      body: 'New Question',
      name: 'new_question',
      default: null,
      hint: null,
      validators: {
        required: false,
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: {
                value: null,
                geocode: null,
                street_views: []
              }
            },
          }
        }
      }
    });
  });

  it('should add an incidentType question', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'incidentType',
      body: 'New Question',
      name: 'new_question',
      default: null,
      hint: null,
      validators: {
        required: false,
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: {
                value: null,
                parent: null,
              }
            },
          }
        }
      }
    });
  });

  it('should add a vrnLookup question', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'vrnLookup',
      body: 'New Question',
      name: 'new_question',
      default: null,
      hint: null,
      validators: {
        required: false,
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: {
                value: null,
                vehicle: null,
              }
            },
          }
        }
      }
    });
  });

  it('should add a vehicleSelector question', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'vehicleSelector',
      body: 'New Question',
      name: 'new_question',
      default: null,
      hint: null,
      validators: {
        required: false,
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: {
                value: null,
                vehicle: null,
              }
            },
          }
        }
      }
    });
  });

  it('should add a vehicleType question', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'vehicleType',
      body: 'New Question',
      name: 'new_question',
      data: [
        {
          key: 'carId',
          value: 'Car'
        },
        {
          key: 'otherId',
          value: 'Other'
        },
      ],
      default: null,
      hint: null,
      validators: {
        required: false,
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: {
                value: null,
                other_vehicle_type: null
              },
            },
          }
        }
      }
    });
  });

  it('should add an upload question', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'upload',
      body: 'New Question',
      name: 'new_question',
      default: null,
      hint: null,
      validators: {
        required: false,
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: null,
            },
          }
        }
      }
    });
  });

  it('should add a multiple upload question', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'upload',
      body: 'New Question',
      name: 'new_question',
      default: null,
      hint: null,
      multiple: true,
      validators: {
        required: false,
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: [],
            },
          }
        }
      }
    });
  });

  it('should add a question with children', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'text',
      body: 'New Question',
      name: 'new_question',
      validators: {
        required: false,
        maxLength: null,
        minLength: null
      },
      conditions: [],
      children: [
        {
          id: 'childQuestionId',
          type: 'text',
          body: 'Child Question',
          name: 'child_question',
          default: null,
          hint: null,
          validators: {
            required: false,
            maxLength: null,
            minLength: null
          },
        }
      ]
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              children: {
                childQuestionId: {
                  answer: null,
                }
              },
            },
          }
        }
      }
    });
  });

  it('should add a question with a default value', () => {
    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'text',
      body: 'New Question',
      name: 'new_question',
      default: 'default value',
      hint: null,
      validators: {
        required: false,
        maxLength: null,
        minLength: null
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: 'default value',
            },
          }
        }
      }
    });
  });

  it('should load the reference from local storage', () => {
    localStorageSpy.get.and.returnValue('testReference');

    form.init(testForms.basicForm);

    const question: RenderedQuestion = {
      id: 'newQuestionId',
      type: 'text',
      body: 'Reference',
      name: 'reference',
      default: null,
      hint: null,
      validators: {
        required: false,
        maxLength: null,
        minLength: null
      },
      conditions: [],
    };

    form.addQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup, question);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              answer: null,
            },
            newQuestionId: {
              question_id: 'newQuestionId',
              answer: 'testReference',
            },
          }
        }
      }
    });
  });

  it('should remove a question', () => {
    form.init(testForms.basicForm);

    form.removeQuestion(form.formGroup.get('groups.groupId.answers') as UntypedFormGroup,
      testForms.basicForm.groups[0].questions[0]);

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {}
        }
      }
    });
  });

  it('should add a child', () => {
    form.init(testForms.formWithChildren);

    const child: RenderedQuestion = {
      id: 'newChildQuestionId',
      type: 'text',
      body: 'New Child Question',
      name: 'new_child_question',
      default: null,
      hint: null,
      validators: {
        required: false,
        maxLength: null,
        minLength: null
      },
    };

    form.addChild(
      form.formGroup.get('groups.groupId.answers.questionId.children') as UntypedFormGroup,
      testForms.formWithChildren.groups[0].questions[0],
      child
    );

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              children: {
                childQuestionId: {
                  answer: null,
                },
                newChildQuestionId: {
                  answer: null,
                },
              }
            }
          }
        }
      }
    });
  });

  it('should remove a child', () => {
    form.init(testForms.formWithChildren);

    form.removeChild(
      form.formGroup.get('groups.groupId.answers.questionId.children') as UntypedFormGroup,
      testForms.formWithChildren.groups[0].questions[0],
      testForms.formWithChildren.groups[0].questions[0].children[0]
    );

    expect(form.formGroup.value).toEqual({
      groups: {
        groupId: {
          group_id: 'groupId',
          child_id: null,
          answers: {
            questionId: {
              question_id: 'questionId',
              children: {}
            }
          }
        }
      }
    });
  });

  it('should set the form values', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'text',
            label: 'Test Question',
            name: 'test_question',
            value: 'Foo',
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: 'Foo'
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the vrnLookup value', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'vrnLookup',
            label: 'Test Question',
            name: 'test_question',
            value: 'Ford - Fiesta - Grey',
            vehicle: {
              make: 'Ford',
              model: 'Fiesta',
              color: 'Grey'
            }
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: {
                  value: 'Ford - Fiesta - Grey',
                  vehicle: {
                    make: 'Ford',
                    model: 'Fiesta',
                    color: 'Grey'
                  }
                }
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the vehicleSelector value', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'vehicleSelector',
            label: 'Test Question',
            name: 'test_question',
            value: 'Ford - Fiesta - Grey',
            vehicle: {
              make: 'Ford',
              model: 'Fiesta',
              color: 'Grey'
            }
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: {
                  value: 'Ford - Fiesta - Grey',
                  vehicle: {
                    make: 'Ford',
                    model: 'Fiesta',
                    color: 'Grey'
                  }
                }
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the vehicleType value', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'vehicleType',
            label: 'Test Question',
            name: 'test_question',
            data: [
              {
                key: 'carId',
                value: 'Car'
              },
              {
                key: 'otherId',
                value: 'Other'
              },
            ],
            key: 'otherId',
            value: 'Other',
            other_vehicle_type: 'Forklift'
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: {
                  value: 'otherId',
                  other_vehicle_type: 'Forklift'
                }
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the location value', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'location',
            label: 'Test Question',
            name: 'test_question',
            value: 'Norwich, UK',
            geocode: {
              lat: 1,
              lng: 1
            }
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: {
                  value: 'Norwich, UK',
                  geocode: {
                    lat: 1,
                    lng: 1
                  }
                }
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the location street view value', (done: DoneFn) => {
    form.init(testForms.formWithStreetViews);

    form.setValues([
      {
        id: 'G1',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'G1-Q1',
            type: 'location',
            label: 'Test Question',
            name: 'test_question',
            value: 'Norwich, UK',
            geocode: {
              lat: 1,
              lng: 1
            },
            street_views: [
              {
                'url': 'https:\/\/maps.googleapis.com\/maps\/api\/streetview?size=600x300&location=52.6308859,1.297355&heading=33.923716206116204\n            &fov=90&pitch=10.004122502442044&key=AIzaSyBKG7ftIqEbcacmJqvuS4u0k4WQv7LrkDo',
                'caption': 'Test Caption'
              }
            ]
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          G1: {
            group_id: 'G1',
            child_id: null,
            answers: {
              'G1-Q1': {
                question_id: 'G1-Q1',
                answer: {
                  value: 'Norwich, UK',
                  geocode: {
                    lat: 1,
                    lng: 1
                  },
                  street_views: [
                    {
                      'url': 'https:\/\/maps.googleapis.com\/maps\/api\/streetview?size=600x300&location=52.6308859,1.297355&heading=33.923716206116204\n            &fov=90&pitch=10.004122502442044&key=AIzaSyBKG7ftIqEbcacmJqvuS4u0k4WQv7LrkDo',
                      'caption': 'Test Caption'
                    }
                  ]
                }
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the incidentType value', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'incidentType',
            label: 'Test Question',
            name: 'test_question',
            key: 'swerveToAvoidId',
            value: 'Swerve to Avoid',
            parent: 'tJunctionId',
            parent_value: 'T-Junction',
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: {
                  value: 'swerveToAvoidId',
                  parent: 'tJunctionId'
                }
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the radio value', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'radio',
            label: 'Test Question',
            name: 'test_question',
            key: 'foo',
            value: 'Foo'
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: 'foo'
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the select value', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'select',
            label: 'Test Question',
            name: 'test_question',
            key: 'foo',
            value: 'Foo'
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: 'foo'
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the multiple select value', (done) => {
    form.init(testForms.basicForm);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        label: 'Test Group',
        name: 'test_group',
        multiple: false,
        values: [
          {
            id: 'questionId',
            type: 'select',
            label: 'Test Question',
            name: 'test_question',
            multiple: true,
            value: [
              'foo',
              'bar'
            ]
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              questionId: {
                question_id: 'questionId',
                answer: [
                  'foo',
                  'bar'
                ]
              }
            }
          }
        }
      });

      done();
    });
  });

  it('should set the multiple group values', (done: DoneFn) => {
    form.init(testForms.formWithMultipleGroup);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        name: 'test_group',
        label: 'Test Group',
        multiple: true,
        values: [
          [
            {
              id: 'groupId-question1',
              type: 'text',
              name: 'question_1',
              label: 'Question 1',
              value: 'Foo',
            },
            {
              id: 'groupId-question2',
              type: 'text',
              name: 'question_2',
              label: 'Question 2',
              value: 'Bar',
            },
          ],
          [
            {
              id: 'groupId-question1',
              type: 'text',
              name: 'question_1',
              label: 'Question 1',
              value: 'Baz',
            },
            {
              id: 'groupId-question2',
              type: 'text',
              name: 'question_2',
              label: 'Question 2',
              value: 'Qux',
            },
          ]
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: [
              {
                'groupId-question1': {
                  question_id: 'groupId-question1',
                  answer: 'Foo'
                },
                'groupId-question2': {
                  question_id: 'groupId-question2',
                  answer: 'Bar'
                },
              },
              {
                'groupId-question1': {
                  question_id: 'groupId-question1',
                  answer: 'Baz'
                },
                'groupId-question2': {
                  question_id: 'groupId-question2',
                  answer: 'Qux'
                },
              }
            ]
          }
        }
      });

      done();
    });
  });

  it('should set the nested form values', (done: DoneFn) => {
    form.init(testForms.formWithNestedGroup);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        name: 'parent_group',
        label: 'Parent Group',
        multiple: false,
        groups: [
          {
            id: 'groupId-nestedFormId-nestedGroupId',
            type: 'group',
            name: 'nested_group',
            label: 'Nested Group',
            multiple: false,
            values: [
              {
                id: 'groupId-nestedFormId-nestedGroupId-question1',
                type: 'text',
                name: 'question_1',
                label: 'Question 1',
                value: 'Foo',
              },
              {
                id: 'groupId-nestedFormId-nestedGroupId-question2',
                type: 'text',
                name: 'question_2',
                label: 'Question 2',
                value: 'Bar',
              },
            ]
          }
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: {
              "groupId-nestedFormId-nestedGroupId": {
                group_id: 'groupId-nestedFormId-nestedGroupId',
                child_id: null,
                answers: {
                  'groupId-nestedFormId-nestedGroupId-question1': {
                    question_id: 'groupId-nestedFormId-nestedGroupId-question1',
                    answer: 'Foo',
                  },
                  'groupId-nestedFormId-nestedGroupId-question2': {
                    question_id: 'groupId-nestedFormId-nestedGroupId-question2',
                    answer: 'Bar',
                  }
                }
              }
            }
          }
        }
      });

      done();
    }, done.fail);
  });

  /**
   * If the nested form is made up of multiple groups rather than just
   * values we have to set the values slightly differently. This test
   * asserts that the nested group values are set correctly.
   */
  it('should set the multiple nested form groups', (done: DoneFn) => {
    form.init(testForms.formWithMultipleNestedGroups);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        name: 'witnesses',
        label: 'Witnesses',
        multiple: true,
        groups: [
          [
            {
              id: 'groupId-nestedFormId-detailsId',
              type: 'group',
              name: 'details',
              label: 'Details',
              multiple: false,
              values: [
                {
                  id: 'groupId-nestedFormId-detailsId-nameId',
                  type: 'text',
                  name: 'name',
                  label: 'Name',
                  value: 'John Doe',
                },
              ]
            },
            {
              id: 'groupId-nestedFormId-contactDetailsId',
              type: 'group',
              name: 'contact_details',
              label: 'Contact Details',
              multiple: false,
              values: [
                {
                  id: 'groupId-nestedFormId-contactDetailsId-phoneId',
                  type: 'text',
                  name: 'phone',
                  label: 'Phone',
                  value: '07123 123123',
                },
              ]
            }
          ],
          [
            {
              id: 'groupId-nestedFormId-detailsId',
              type: 'group',
              name: 'details',
              label: 'Details',
              multiple: false,
              values: [
                {
                  id: 'groupId-nestedFormId-detailsId-nameId',
                  type: 'text',
                  name: 'name',
                  label: 'Name',
                  value: 'Jane Doe',
                },
              ]
            },
            {
              id: 'groupId-nestedFormId-contactDetailsId',
              type: 'group',
              name: 'contact_details',
              label: 'Contact Details',
              multiple: false,
              values: [
                {
                  id: 'groupId-nestedFormId-contactDetailsId-phoneId',
                  type: 'text',
                  name: 'phone',
                  label: 'Phone',
                  value: '07321 321321',
                },
              ]
            }
          ]
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: [
              {
                'groupId-nestedFormId-detailsId': {
                  group_id: 'groupId-nestedFormId-detailsId',
                  child_id: null,
                  answers: {
                    'groupId-nestedFormId-detailsId-nameId': {
                      question_id: 'groupId-nestedFormId-detailsId-nameId',
                      answer: 'John Doe',
                    },
                  }
                },
                'groupId-nestedFormId-contactDetailsId': {
                  group_id: 'groupId-nestedFormId-contactDetailsId',
                  child_id: null,
                  answers: {
                    'groupId-nestedFormId-contactDetailsId-phoneId': {
                      question_id: 'groupId-nestedFormId-contactDetailsId-phoneId',
                      answer: '07123 123123',
                    }
                  }
                },
              },
              {
                'groupId-nestedFormId-detailsId': {
                  group_id: 'groupId-nestedFormId-detailsId',
                  child_id: null,
                  answers: {
                    'groupId-nestedFormId-detailsId-nameId': {
                      question_id: 'groupId-nestedFormId-detailsId-nameId',
                      answer: 'Jane Doe',
                    },
                  }
                },
                'groupId-nestedFormId-contactDetailsId': {
                  group_id: 'groupId-nestedFormId-contactDetailsId',
                  child_id: null,
                  answers: {
                    'groupId-nestedFormId-contactDetailsId-phoneId': {
                      question_id: 'groupId-nestedFormId-contactDetailsId-phoneId',
                      answer: '07321 321321',
                    }
                  }
                },
              }
            ]
          }
        }
      });

      done();
    });
  });

  it('should set the multiple nested form groups with child id', (done: DoneFn) => {
    form.init(testForms.formWithMultipleNestedGroups);

    form.setValues([
      {
        id: 'groupId',
        type: 'group',
        name: 'witnesses',
        label: 'Witnesses',
        multiple: true,
        groups: [
          [
            {
              id: 'groupId-nestedFormId-detailsId',
              type: 'group',
              name: 'details',
              label: 'Details',
              child_id: 'groupId-nestedFormId-detailsId-childId',
              multiple: false,
              values: [
                {
                  id: 'groupId-nestedFormId-detailsId-nameId',
                  type: 'text',
                  name: 'name',
                  label: 'Name',
                  value: 'John Doe',
                },
              ]
            },
            {
              id: 'groupId-nestedFormId-contactDetailsId',
              type: 'group',
              name: 'contact_details',
              label: 'Contact Details',
              child_id: 'groupId-nestedFormId-contactDetailsId-childId',
              multiple: false,
              values: [
                {
                  id: 'groupId-nestedFormId-contactDetailsId-phoneId',
                  type: 'text',
                  name: 'phone',
                  label: 'Phone',
                  value: '07123 123123',
                },
              ]
            }
          ],
          [
            {
              id: 'groupId-nestedFormId-detailsId',
              type: 'group',
              name: 'details',
              label: 'Details',
              multiple: false,
              child_id: 'groupId-nestedFormId-detailsId-childId',
              values: [
                {
                  id: 'groupId-nestedFormId-detailsId-nameId',
                  type: 'text',
                  name: 'name',
                  label: 'Name',
                  value: 'Jane Doe',
                },
              ]
            },
            {
              id: 'groupId-nestedFormId-contactDetailsId',
              type: 'group',
              name: 'contact_details',
              label: 'Contact Details',
              multiple: false,
              child_id: 'groupId-nestedFormId-contactDetailsId-childId',
              values: [
                {
                  id: 'groupId-nestedFormId-contactDetailsId-phoneId',
                  type: 'text',
                  name: 'phone',
                  label: 'Phone',
                  value: '07321 321321',
                },
              ]
            }
          ]
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          groupId: {
            group_id: 'groupId',
            child_id: null,
            answers: [
              {
                'groupId-nestedFormId-detailsId': {
                  group_id: 'groupId-nestedFormId-detailsId',
                  child_id: 'groupId-nestedFormId-detailsId-childId',
                  answers: {
                    'groupId-nestedFormId-detailsId-nameId': {
                      question_id: 'groupId-nestedFormId-detailsId-nameId',
                      answer: 'John Doe',
                    },
                  }
                },
                'groupId-nestedFormId-contactDetailsId': {
                  group_id: 'groupId-nestedFormId-contactDetailsId',
                  child_id: 'groupId-nestedFormId-contactDetailsId-childId',
                  answers: {
                    'groupId-nestedFormId-contactDetailsId-phoneId': {
                      question_id: 'groupId-nestedFormId-contactDetailsId-phoneId',
                      answer: '07123 123123',
                    }
                  }
                },
              },
              {
                'groupId-nestedFormId-detailsId': {
                  group_id: 'groupId-nestedFormId-detailsId',
                  child_id: 'groupId-nestedFormId-detailsId-childId',
                  answers: {
                    'groupId-nestedFormId-detailsId-nameId': {
                      question_id: 'groupId-nestedFormId-detailsId-nameId',
                      answer: 'Jane Doe',
                    },
                  }
                },
                'groupId-nestedFormId-contactDetailsId': {
                  group_id: 'groupId-nestedFormId-contactDetailsId',
                  child_id: 'groupId-nestedFormId-contactDetailsId-childId',
                  answers: {
                    'groupId-nestedFormId-contactDetailsId-phoneId': {
                      question_id: 'groupId-nestedFormId-contactDetailsId-phoneId',
                      answer: '07321 321321',
                    }
                  }
                },
              }
            ]
          }
        }
      });

      done();
    });
  });

  it('should set the value of a control with conditions', (done: DoneFn) => {
    form.init(testForms.formWithConditions);

    const validator = new ConditionValidator(form.renderedForm.groups, form.formGroup.get('groups') as UntypedFormGroup);

    // The conditions are usually run in the components, because this is a unit
    // test those components aren't registered. Here, we check if the conditions
    // pass and if they do we add the relevant controls to the form.
    validator.shouldRenderQuestion(testForms.formWithConditions.groups[0].questions[0]).subscribe(() => {
      form.addQuestion(
        form.formGroup.get('groups.G1.answers') as UntypedFormGroup,
        testForms.formWithConditions.groups[0].questions[0]
      );
    });
    validator.shouldRenderGroup(testForms.formWithConditions.groups[1]).subscribe(() => {
      form.addGroup(form.formGroup.get('groups') as UntypedFormGroup, testForms.formWithConditions.groups[1]);
    });

    form.setValues([
      {
        id: 'G1',
        type: 'group',
        label: 'Group Without Conditions',
        name: 'groupWithoutConditions',
        multiple: false,
        values: [
          {
            id: 'G1-Q1',
            type: 'text',
            label: 'Question With Conditions',
            name: 'question_with_conditions',
            value: 'Bar',
          },
          {
            id: 'G1-Q2',
            type: 'text',
            label: 'Question Without Conditions',
            name: 'question_without_conditions',
            value: 'Foo',
          },
        ]
      },
      {
        id: 'G2',
        type: 'group',
        label: 'Group With Conditions',
        name: 'group_with_conditions',
        multiple: false,
        values: [
          {
            id: 'G2-Q1',
            type: 'text',
            label: 'Has Conditions',
            name: 'has_conditions',
            value: 'Baz',
          },
        ]
      }
    ]).then(f => {
      expect(f.formGroup.value).toEqual({
        groups: {
          G1: {
            group_id: 'G1',
            child_id: null,
            answers: {
              'G1-Q1': {
                question_id: 'G1-Q1',
                answer: 'Bar'
              },
              'G1-Q2': {
                question_id: 'G1-Q2',
                answer: 'Foo'
              },
            }
          },
          G2: {
            group_id: 'G2',
            child_id: null,
            answers: {
              'G2-Q1': {
                question_id: 'G2-Q1',
                answer: 'Baz'
              },
            }
          }
        }
      });

      done();
    });
  });
});
