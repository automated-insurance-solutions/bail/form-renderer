#!/usr/bin/env bash

echo "Setting dev version"

# NPM tags cannot contain a "/". We use "/" in our branch names (e.g. feature/AIS-1000), so here we replace any slashes
# with a dash in the branch name (e.g. feature-AIS-1000).
CLEAN_VERSION="${CI_COMMIT_REF_NAME/\//-}"

# Get all versions of the package
VERSIONS="$(curl -s https://gitlab.com/api/v4/projects/42631411/packages\?order_by\=version\&sort\=desc | grep -o '"version":*\"[^"]*"' | grep -o '"[^"]*"$')"
# Get any releases for the current release candidate (e.g. 3.0.0-rc-your-branch.0)
RELEASE_VERSIONS="$(echo "$VERSIONS" | grep ${CLEAN_VERSION})"

# Grab the correct release version
if [[ $(echo $RELEASE_VERSIONS | head -1) ]]; then
  VERSION=$(echo $RELEASE_VERSIONS | head -1 | cut -d'"' -f 2)
else
  VERSION=$(echo "$VERSIONS" | head -1 | cut -d'"' -f 2)
fi

# Change the tag from 0.1.0 to the current tag
npm version --allow-same-version $VERSION
# Bump the prerelease tag
npm version --preid=$CLEAN_VERSION prerelease
