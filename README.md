# Form Renderer

[![pipeline status](https://gitlab.gmlconsulting.co.uk/bail/form-renderer/badges/master/pipeline.svg)](https://gitlab.gmlconsulting.co.uk/bail/form-renderer/-/commits/master) [![coverage report](https://gitlab.gmlconsulting.co.uk/bail/form-renderer/badges/master/coverage.svg)](https://gitlab.gmlconsulting.co.uk/bail/form-renderer/-/commits/master)

Docs coming soon... 

## Unit Tests

To run the unit tests run `npm run test`.

## E2E Tests

To run the end to end tests you first have to build the js bundle by running: 

```npm run build:bundle```

If you are running into issues with the minified bundle you can run `npm run build:bundle:dev` to get the development bundle.

Next, you need to start the e2e server. 

The e2e server is used to host the test files that cypress will interact with. The server will need to run the entire time the tests are running so leave it running.

```npm run e2e:server```

Finally, you can run the tests by running:

```npm run e2e```

This will open the cypress interface. If you just want to run the tests in the terminal run `npm run e2e:run`
